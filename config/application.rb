require File.expand_path('../boot', __FILE__)

require 'rails/all'
require_relative '../lib/limiter/yazda_limiter'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module YazdaServer
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    config.api_only = true

    config.middleware.delete ::Rack::Sendfile
    config.middleware.delete ::Rack::Pjax
    config.middleware.delete ::ActionDispatch::Static
    # config.middleware.delete ::ActionDispatch::Cookies
    config.middleware.delete ::ActionDispatch::Flash
    # config.middleware.delete ::ActionDispatch::Session::CookieStore

    config.middleware.insert_before 0, 'Rack::Cors', logger: (-> { Rails.logger }) do
      # TODO allow per doorkeeper application
      allow do
        origins 'localhost', 'yazda_server.dev', 'localhost:5000',
                '*.yazdaapp.com', 'localhost:4200', '192.168.60.2:8100'

        routes = %w(/oauth/* /admin/* /sidekiq/* /v0.1/sign_in
                    /v0.1/reset_password/* /v0.1/register/* /v0.1/oauth/*
                    /sign_in/* /reset_password/* /register/* )

        routes.each do |path|
          resource path, headers: :any, methods: :any
        end
      end

      allow do
        origins '*'
        resource '*', headers: :any, methods: :any
      end
    end

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.active_record.schema_format                    = :sql

    if Rails.env.production?
      config.middleware.insert_after Rack::Cors,
                                     ::YazdaLimiter,
                                     max:        30,
                                     cache:      Redis.new,
                                     key_prefix: :throttle
    end

    config.generators do |g|
      g.test_framework :minitest, spec: true
    end

    config.to_prepare do
      DeviseMailer.layout 'mailer'
    end
  end
end
