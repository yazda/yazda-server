require 'sidekiq/web'
require 'sidekiq-scheduler/web'
require 'sidekiq-status/web'

Rails.application.routes.draw do
  Rails.application.routes.default_url_options[:host] = Rails.application.config.action_mailer.default_url_options[:host]

  get 'home/index'

  mount Blazer::Engine, at: "blazer"

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(username), ::Digest::SHA256.hexdigest('yazdaadmin')) &
    ActiveSupport::SecurityUtils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest('adzay'))
  end
  mount Sidekiq::Web => '/sidekiq'

  use_doorkeeper

  root to: 'home#index'

  api_version(module: 'V01', path: { value: 'v0.1' }, defaults: { format: :json }, default: true) do
    devise_for :users,
               path:        '/',
               controllers: {
                 passwords: 'v01/passwords'
               },
               path_names:  { registration: 'register',
                              password:     'reset_password' }

    # TODO JSON for all devise routes

    get 'account/settings', to: 'account#get_settings'
    get 'account/me', to: 'account#me'
    match 'account/settings', to: 'account#post_settings', via: [:put, :patch]
    post 'account/update_profile_image', to: 'account#update_profile_image'
    post 'account/update_banner_image', to: 'account#update_banner_image'

    # Invites
    get 'invites/pending', to: 'invites#pending'
    match 'invites/accept', to: 'invites#accept', via: [:put, :patch]
    match 'invites/reject', to: 'invites#reject', via: [:put, :patch]

    get 'oauth/facebook', to: 'providers#facebook'
    get 'oauth/strava', to: 'providers#strava'

    resource :message, only: [:show]
    resources :adventures, except: [:edit]
    resources :invites, only: [:create]
    resources :devices, only: [:create]

    resources :users, only: [:show, :index] do
      collection do
        get :search
        get :find_strava_users
        get :find_facebook_users
        post :find_friends_with_email
      end

      member do
        get :avatar
        get :banner
      end

      resources :friends, only: [:index]
      resources :followers, only: [:index]
    end

    resources :friendships, only: [:create] do
      collection do
        get :incoming
        get :outgoing
      end

      member do
        put :accept
        put :reject
        patch :accept
        patch :reject
      end
    end

    resources :clubs, only: [:show, :create, :update, :index] do
      member do
        get :members
        get :avatar
        get :banner

        post :join
        post :leave
        post :update_profile_image
        post :update_banner_image
        post :waiver

        patch :update_notification
        patch :update_member
      end
    end
  end

  api_version(module: 'V1', path: { value: 'v1' }, defaults: { format: :json }) do
    redirect 'password', to: 'v01/password'
    redirect 'register', to: 'v01/register'
    redirect 'reset_password', to: 'v01/reset_password'
    # devise_for :users,
    #            path:        '/',
    #            controllers: {
    #              passwords: 'v1/passwords'
    #            },
    #            path_names:  { registration: 'register',
    #                           password:     'reset_password' }

    # TODO JSON for all devise routes

    get 'account/settings', to: 'account#get_settings'
    get 'account/me', to: 'account#me'
    match 'account/settings', to: 'account#post_settings', via: [:put, :patch]
    post 'account/update_profile_image', to: 'account#update_profile_image'
    post 'account/update_banner_image', to: 'account#update_banner_image'

    # Invites
    get 'invites/pending', to: 'invites#pending'
    get 'invites/attending', to: 'invites#attending'
    get 'invites/rejected', to: 'invites#rejected'
    match 'invites/accept', to: 'invites#accept', via: [:put, :patch]
    match 'invites/reject', to: 'invites#reject', via: [:put, :patch]

    get 'oauth/facebook', to: 'providers#facebook'
    get 'oauth/strava', to: 'providers#strava'

    resources :adventures, except: [:edit]
    resources :invites, only: [:create]
    resources :locations, only: [:index, :create]
    resources :tags, only: [:index, :show]

    resources :users, only: [:show, :index] do
      collection do
        get :search
        get :find_strava_users
        get :find_facebook_users
        post :find_friends_with_email
      end

      member do
        get :avatar
        get :banner
      end

      resources :friends, only: [:index]
    end

    resources :friendships, only: [:create] do
      collection do
        get :incoming
        get :outgoing
      end

      member do
        put :accept
        put :reject
        patch :accept
        patch :reject
      end
    end

    resources :clubs, only: [:show, :create, :update, :index] do
      member do
        get :members
        get :avatar
        get :banner

        post :join
        post :leave
        post :update_profile_image
        post :update_banner_image
        post :waiver

        patch :update_notification
        patch :update_member
      end
    end

    namespace :internal do
      resources :volunteer_trackings, only: :none do
        collection do
          get :adventures
          get :users
        end
      end
    end
  end

  resource :widget, only: [:show]

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
