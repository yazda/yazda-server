CarrierWave.configure do |config|

  if ENV['AWS_ENABLED']
    config.fog_credentials = {
      provider:              'AWS', # required
      aws_access_key_id:     ENV['AWS_ACCESS_KEY_ID'], # required
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'] # required
    }
    config.fog_directory   = 'yazdauploads' # required
    config.fog_attributes  = { 'Cache-Control' => "max-age=#{365.day.to_i}" } # optional, defaults to {}
  end

  config.remove_previously_stored_files_after_update = false
  config.asset_host      = ActionController::Base.asset_host
end
