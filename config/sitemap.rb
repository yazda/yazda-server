require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = 'https://yazdaapp.com'
SitemapGenerator::Sitemap.compress     = false
SitemapGenerator::Sitemap.create do
  add 'sign_in', priority: 1, mobile: true
  add 'sign_in', priority: 1
  add 'register', priority: 1, mobile: true
  add 'register', priority: 1
  add 'reset_password', priority: 1, mobile: true
  add 'reset_password', priority: 1
  add 'bike-clubs', priority: 1, mobile: true
  add 'bike-clubs', priority: 1
  add 'bike-riders', priority: 1, mobile: true
  add 'bike-riders', priority: 1
  add 'bike-shops', priority: 1, mobile: true
  add 'bike-shops', priority: 1

  add '/adventures',
      lastmod:    Adventure.last.created_at,
      changefreq: :always,
      mobile:     true,
      priority: 0.7
  add '/adventures',
      lastmod:    Adventure.last.created_at,
      changefreq: :always,
      priority: 0.7

  add '/tags',
      changefreq: :always,
      mobile:     true
  add '/tags',
      changefreq: :always

  add '/clubs',
      lastmod:    Club.last.created_at,
      changefreq: :always,
      mobile:     true,
      priority: 0.7
  add '/clubs',
      lastmod:    Club.last.created_at,
      changefreq: :always,
      priority: 0.7

  Adventure.public_adventure.find_each do |adventure|
    add "/adventures/#{adventure.id}",
        lastmod:  adventure.updated_at,
        mobile:   true

    add "/adventures/#{adventure.id}",
        lastmod:  adventure.updated_at
  end

  User.all.find_each do |user|
    images = []
    images << { loc: user.avatar.url } if user.avatar.url
    images << { loc: user.banner.url } if user.banner.url

    add "/users/#{user.id}",
        lastmod:  user.updated_at,
        priority: 0.5,
        mobile:   true,
        images:   images

    add "/users/#{user.id}",
        lastmod:  user.updated_at,
        priority: 0.5,
        images:   images
  end

  Club.all.find_each do |club|
    images = []
    images << { loc: club.avatar.url } if club.avatar.url
    images << { loc: club.banner.url } if club.banner.url

    add "/clubs/#{club.id}",
        lastmod:  club.updated_at,
        priority: 0.9,
        mobile:   true,
        images:   images

    add "/clubs/#{club.id}",
        lastmod:  club.updated_at,
        priority: 0.9,
        images:   images
  end

  ActsAsTaggableOn::Tag.all.find_each do |tag|
    add "/tags/#{tag.name}", mobile: true
    add "/tags/#{tag.name}"

    add "/adventures?tag=#{tag.name}",
        changefreq: :always,
        mobile:     true
    add "/adventures?tag=#{tag.name}",
        changefreq: :always

    add "/clubs?tag=#{tag.name}",
        changefreq: :always,
        mobile:     true
    add "/clubs?tag=#{tag.name}",
        changefreq: :always

    # TODO users by tag
  end
end
