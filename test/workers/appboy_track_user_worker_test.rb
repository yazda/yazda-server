require 'test_helper'

describe AppboyTrackUserWorker do
  it 'should set user hash' do
    api = MiniTest::Mock.new

    api.expect :track_attribute, nil, [Hash]

    # TODO why isn't this test?
    Rails.env.stub :development?, false do
      Appboy::API.stub :new, api do
        User.stub :find, User.first do
          worker = AppboyTrackUserWorker.new
          worker.perform(1)
        end
      end
    end

    api.verify
  end
end
