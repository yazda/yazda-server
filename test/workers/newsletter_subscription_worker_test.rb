require 'test_helper'

describe NewsletterSubscriptionWorker do
  describe '#perform' do
    let(:user) do
      MiniTest::Mock.new
    end

    before do
      user.expect(:email, 'test@test.com')
      user.expect(:name, 'Test User')
    end

    describe 'subscribe' do
      subscribe = MiniTest::Mock.new

      subscribe.expect(:subscribe,
                       nil,
                       [{
                          id:           'newsletter',
                          email:        'test@test.com',
                          merge_vars:   { FNAME: 'Test', LNAME: 'User' },
                          double_optin: false,
                          send_welcome: true
                        }])
      subscribe.expect(:subscribe,
                       nil,
                       [{
                          id:           'newsletter',
                          email:        'test@test.com',
                          merge_vars:   { FNAME: 'Test', LNAME: 'User' },
                          double_optin: false,
                          send_welcome: true
                        }])

        # worker = NewsletterSubscriptionWorker.new

    end
  end
end
