require 'test_helper'

describe UpdateAdventureWorker do
  let(:adventure) do
    Adventure.first
  end

  describe '#perform' do
    describe 'adventure_edit' do
      describe 'both' do
        before do
          NotificationPreference.update_all(adventure_edit: 'both')
        end

        it 'should send push and email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = UpdateAdventureWorker.new
            worker.perform(1)
          end

          assert_equal adventure.invites.count, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end
    end

    describe 'email' do
      before do
        NotificationPreference.update_all(adventure_edit: 'email')
      end

      it 'should not send push but email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = UpdateAdventureWorker.new
          worker.perform(1)
        end

        assert_equal adventure.invites.count, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size
      end
    end

    describe 'push' do
      before do
        NotificationPreference.update_all(adventure_edit: 'push')
      end

      it 'should send push and not email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = UpdateAdventureWorker.new
          worker.perform(1)
        end

        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 1, SendPushNotificationWorker.jobs.size
      end
    end
  end
end
