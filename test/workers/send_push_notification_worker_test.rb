require 'test_helper'
require "#{Rails.root}/lib/notification_message_builder"

describe SendPushNotificationWorker do
  it 'should call publish' do
    api     = MiniTest::Mock.new
    message = NotificationMessageBuilder.new.adventure_invite(Adventure.first)

    api.expect(:send_messages,
               nil,
               [{ messages: message, external_user_ids: [1] }])

    Appboy::API.stub :new, api do
      worker = SendPushNotificationWorker.new
      worker.perform([1], message)
    end
  end
end
