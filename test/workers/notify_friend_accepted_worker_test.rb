require 'test_helper'

describe NotifyFriendAcceptedWorker do
  let(:friendship) do
    Friendship.first
  end

  describe '#perform' do
    describe 'friend' do
      describe 'both' do
        before do
          NotificationPreference.update_all(friend: 'both')
        end

        it 'should send push and email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Friendship.stub :find, friendship do
            worker = NotifyFriendAcceptedWorker.new
            worker.perform(1)
          end

          assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'email' do
        before do
          NotificationPreference.update_all(friend: 'email')
        end

        it 'should not send push but send email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Friendship.stub :find, friendship do
            worker = NotifyFriendAcceptedWorker.new
            worker.perform(1)
          end

          assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'push' do
        before do
          NotificationPreference.update_all(friend: 'push')
        end

        it 'should send push but not email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Friendship.stub :find, friendship do
            worker = NotifyFriendAcceptedWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'none' do
        before do
          NotificationPreference.update_all(friend: 'none')
        end

        it 'should not send push or email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Friendship.stub :find, friendship do
            worker = NotifyFriendAcceptedWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
        end
      end
    end
  end
end
