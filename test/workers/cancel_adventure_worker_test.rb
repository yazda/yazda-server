require 'test_helper'

describe CancelAdventureWorker do
  let(:adventure) do
    Adventure.create! name:           'Testing 123',
                      address:        'Golden CO',
                      start_time:     1.week.from_now,
                      end_time:       2.weeks.from_now,
                      user_ids:       User.limit(3).pluck(:id),
                      adventure_type: 'hiking',
                      owner:          User.last
  end

  describe '#perform' do
    describe 'adventure_cancel' do
      describe 'both' do
        before do
          NotificationPreference.update_all(adventure_cancel: 'both')
        end

        it 'should send push and email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = CancelAdventureWorker.new
            worker.perform(1)
          end

          assert_equal 3, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'email' do
        before do
          NotificationPreference.update_all(adventure_cancel: 'email')
        end

        it 'should not send push but send email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = CancelAdventureWorker.new
            worker.perform(1)
          end

          assert_equal 3, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'push' do
        before do
          NotificationPreference.update_all(adventure_cancel: 'push')
        end

        it 'should send push but not email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = CancelAdventureWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end
    end
  end
end
