require 'test_helper'

describe NotifyAdventureInviteesWorker do
  let(:adventure) do
    Adventure.first
  end

  describe '#perform' do
    describe 'adventure_invite' do
      describe 'both' do
        before do
          NotificationPreference.update_all(adventure_invite: 'both')
        end

        it 'should send push and email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
          assert_equal 0, InviteWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = NotifyAdventureInviteesWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
          assert_equal adventure.invites.count, InviteWorker.jobs.size
        end
      end

      describe 'email' do
        before do
          NotificationPreference.update_all(adventure_invite: 'email')
        end

        it 'should not send push but send email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
          assert_equal 0, InviteWorker.jobs.size

          Adventure.stub :find, adventure do
            worker = NotifyAdventureInviteesWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
          assert_equal adventure.invites.count, InviteWorker.jobs.size
        end
      end
    end

    describe 'push' do
      before do
        NotificationPreference.update_all(adventure_invite: 'push')
      end

      it 'should send push and not email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size
        assert_equal 0, InviteWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = NotifyAdventureInviteesWorker.new
          worker.perform(1)
        end

        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size
        assert_equal adventure.invites.count, InviteWorker.jobs.size
      end
    end
  end

  # TODO Club && link checks
end
