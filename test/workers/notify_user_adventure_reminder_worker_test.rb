require "test_helper"

describe NotifyUserAdventureReminderWorker do
  let(:adventure) { Adventure.find_by_name 'Future' }

  describe 'adventure_reminder' do
    describe 'both' do
      before do
        NotificationPreference.update_all(adventure_reminder: 'both')
      end

      it 'should send push and email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = NotifyUserAdventureReminderWorker.new
          worker.perform(1)
        end

        assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 1, SendPushNotificationWorker.jobs.size
      end
    end

    describe 'none' do
      before do
        NotificationPreference.update_all(adventure_reminder: 'none')
      end

      it 'should not send push or email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = NotifyUserAdventureReminderWorker.new
          worker.perform(1)
        end

        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size
      end
    end

    describe 'email' do
      before do
        NotificationPreference.update_all(adventure_reminder: 'email')
      end

      it 'should not send push but send email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = NotifyUserAdventureReminderWorker.new
          worker.perform(1)
        end

        assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size
      end
    end

    describe 'push' do
      before do
        NotificationPreference.update_all(adventure_reminder: 'push')
      end

      it 'should send push but not email' do
        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 0, SendPushNotificationWorker.jobs.size

        Adventure.stub :find, adventure do
          worker = NotifyUserAdventureReminderWorker.new
          worker.perform(1)
        end

        assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
        assert_equal 1, SendPushNotificationWorker.jobs.size
      end
    end
  end
end
