require 'test_helper'

# TODO this
describe InviteUpdatedWorker do
  let(:invite) do
    Invite.find_or_create_by!(user:      User.last,
                              adventure: Adventure.last)
  end

  describe '#perform' do
    describe 'adventure_joined' do
      describe 'both' do
        before :each do
          NotificationPreference.update_all(adventure_joined: 'both')
        end

        it 'should send push' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size


          Invite.stub :find, invite do
            worker = InviteUpdatedWorker.new
            worker.perform(1)
          end

          assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'email' do
        before :each do
          NotificationPreference.update_all(adventure_joined: 'email')
        end

        it 'should not send push but send email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size


          Invite.stub :find, invite do
            worker = InviteUpdatedWorker.new
            worker.perform(1)
          end

          assert_equal 1, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size
        end
      end

      describe 'push' do
        before :each do
          NotificationPreference.update_all(adventure_joined: 'push')
        end

        it 'should send push but not email' do
          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 0, SendPushNotificationWorker.jobs.size

          Invite.stub :find, invite do
            worker = InviteUpdatedWorker.new
            worker.perform(1)
          end

          assert_equal 0, Sidekiq::Extensions::DelayedMailer.jobs.size
          assert_equal 1, SendPushNotificationWorker.jobs.size
        end
      end
    end
  end
end
