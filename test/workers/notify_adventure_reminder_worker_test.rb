require "test_helper"

describe NotifyAdventureReminderWorker do
  let(:adventure) { Adventure.first }

  it 'starts a worker for each adventure' do
    assert_equal 0, NotifyUserAdventureReminderWorker.jobs.size
    adventures = Adventure.where(canceled: false)

    Adventure.stubs(:not_notified).returns(adventures)
    worker = NotifyAdventureReminderWorker.new
    worker.perform

    assert_equal adventures.count, NotifyUserAdventureReminderWorker.jobs.size
  end
end
