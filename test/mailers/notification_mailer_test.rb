require "test_helper"

describe NotificationMailer do
  let(:adventure) do
    adventure = Adventure
                  .new(owner:          users(:user_1),
                       adventure_type: Adventure.adventure_types[:mountain_biking],
                       name:           'Test adventure',
                       start_time:     1.week.from_now,
                       end_time:       8.days.from_now,
                       lat:            0,
                       lon:            0)

    invite = Invite.new(user: users(:user_2), adventure: adventure)
    adventure.invites << invite
    adventure.save
    adventure
  end

  describe '#new_adventure' do
    let(:mail) do
      NotificationMailer.new_adventure(adventure.invites.first.id)
    end

    it 'renders the subject' do
      mail.subject.must_equal('Adventure Invite - Test adventure')
    end

    it 'renders the receiver email' do
      user = adventure.invites.first.user
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end

  describe '#update_adventure' do
    let(:mail) { NotificationMailer.update_adventure(adventure.invites.first.id) }

    it 'renders the subject' do
      mail.subject.must_equal('Adventure Update - Test adventure')
    end

    it 'renders the receiver email' do
      user = adventure.invites.first.user
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end

  describe '#canceled_adventure' do
    let(:mail) do
      NotificationMailer.canceled_adventure(adventure.id,
                                            adventure.invites.first.user.id)
    end

    it 'renders the subject' do
      mail.subject.must_equal('Adventure Canceled - Test adventure')
    end

    it 'renders the receiver email' do
      user = adventure.invites.first.user
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end

  describe '#new_friend_request' do
    let(:friendship) { friendships(:friendship_1) }
    let(:mail) { NotificationMailer.new_friend_request(friendship.id) }

    it 'renders the subject' do
      mail.subject.must_equal('Yazda Friend Request')
    end

    it 'renders the receiver email' do
      user = friendship.friend
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end

  describe '#adventure_reminder' do
    let(:user) { users(:user_1) }
    let(:adventure) { adventures(:future) }
    let(:mail) { NotificationMailer.adventure_reminder(user.id, adventure.id) }

    it 'renders the subject' do
      mail.subject.must_equal("Adventure Reminder - #{adventure.name}")
    end

    it 'renders the receiver email' do
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end

  describe '#new_chat' do
    let(:user) { users(:user_1) }
    let(:sent_by) { users(:user_2) }
    let(:message_txt) { 'Here is a test message' }
    let(:mail) { NotificationMailer.new_chat(user.id, sent_by.id, message_txt) }

    it 'renders the subject' do
      mail.subject.must_equal("#{sent_by.name} sent you a message!")
    end

    it 'renders the receiver email' do
      mail.to.must_equal([user.email])
    end

    it 'renders the sender email' do
      mail.from.must_equal(['no-reply@yazdaapp.com'])
    end
  end
end
