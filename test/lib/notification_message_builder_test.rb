require "#{Rails.root}/lib/notification_message_builder"

describe NotificationMessageBuilder do
  subject { NotificationMessageBuilder.new }

  describe '#adventure_invite' do
    let(:adventure) { Adventure.find_by_name 'Future' }

    it 'builds with owner name' do
      expected = "#{adventure.owner.name} has invited you to a new adventure."
      message = subject.adventure_invite(adventure)

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds with invited_by name' do
      expected = 'A guy has invited you to a new adventure.'
      message = subject.adventure_invite(adventure, invited_by: 'A guy')

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.adventure_invite(adventure)

      message[:apple_push][:alert][:title].must_equal adventure.name
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'adventure'

      message[:android_push][:title].must_equal adventure.name
      message[:android_push][:collapse_key].must_equal 'adventure'
    end
  end

  describe '#update_adventure' do
    let(:adventure) { Adventure.find_by_name 'Future' }

    it 'builds the correct message' do
      expected = "#{adventure.name} details have been updated."
      message = subject.update_adventure(adventure)

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.update_adventure(adventure)

      message[:apple_push][:alert][:title].must_equal adventure.name
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'adventure_update'

      message[:android_push][:title].must_equal adventure.name
      message[:android_push][:collapse_key].must_equal 'adventure_update'
    end
  end

  describe '#cancel_adventure' do
    let(:adventure) { Adventure.find_by_name 'Future' }

    it 'builds the correct message' do
      expected = "#{adventure.name} has been canceled."
      message = subject.cancel_adventure(adventure)

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.cancel_adventure(adventure)

      message[:apple_push][:alert][:title].must_equal adventure.name
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'adventure_update'

      message[:android_push][:title].must_equal adventure.name
      message[:android_push][:collapse_key].must_equal 'adventure_update'
    end
  end

  describe '#friend_request' do
    let(:friendship) { Friendship.first }

    it 'builds the correct message' do
      expected = "#{friendship.user.name} sent you a friend request."
      message = subject.friend_request(friendship)

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.friend_request(friendship)

      message[:apple_push][:alert][:title].must_equal 'Yazda'
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'friend_request'

      message[:android_push][:title].must_equal 'Yazda'
      message[:android_push][:collapse_key].must_equal 'friend_request'
    end
  end

  describe '#invite_responded_to' do
    let(:invite) { Invite.first }

    describe 'invite is ya' do
      it 'builds the correct message' do
        invite.attending = :ya
        expected = "#{invite.user.name} said YA to #{invite.adventure.name}."
        message = subject.invite_responded_to(invite)

        message[:apple_push][:alert][:body].must_equal expected
        message[:android_push][:alert].must_equal expected
      end
    end

    describe 'invite is na' do
      it 'builds the correct message' do
        invite.attending = :na
        expected = "#{invite.user.name} said NA to #{invite.adventure.name}."
        message = subject.invite_responded_to(invite)

        message[:apple_push][:alert][:body].must_equal expected
        message[:android_push][:alert].must_equal expected
      end
    end

    describe 'invite is pending' do
      it 'builds the correct message' do
        invite.attending = :pending
        expected = "#{invite.user.name} has not responded to #{invite.adventure.name}."
        message = subject.invite_responded_to(invite)

        message[:apple_push][:alert][:body].must_equal expected
        message[:android_push][:alert].must_equal expected
      end
    end

    it 'builds the notification' do
      message = subject.invite_responded_to(invite)

      message[:apple_push][:alert][:title].must_equal 'Yazda'
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'invite'

      message[:android_push][:title].must_equal 'Yazda'
      message[:android_push][:collapse_key].must_equal 'invite'
    end
  end

  describe '#adventure_reminder' do
    let(:adventure) { Adventure.find_by_name 'Future' }

    it 'builds the correct message' do
      expected = "Time to start packing your bags. #{adventure.name} is happening soon."
      message = subject.adventure_reminder(adventure)

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.adventure_reminder(adventure)

      message[:apple_push][:alert][:title].must_equal adventure.name
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'adventure_update'

      message[:android_push][:title].must_equal adventure.name
      message[:android_push][:collapse_key].must_equal 'adventure_update'
    end
  end

  describe '#chat' do
    it 'builds the correct message' do
      expected = "Here is my message"
      message = subject.chat(expected, 'title')

      message[:apple_push][:alert][:body].must_equal expected
      message[:android_push][:alert].must_equal expected
    end

    it 'builds the notification' do
      message = subject.chat('message', 'title')

      message[:apple_push][:alert][:title].must_equal 'title'
      message[:apple_push][:sound].must_equal 'yazda_alert.caf'
      message[:apple_push][:extra].must_equal notification_type: 'chat'

      message[:android_push][:title].must_equal 'title'
      message[:android_push][:collapse_key].must_equal 'chat'
    end
  end
end
