require "test_helper"
require "#{Rails.root}/lib/omniauth/strava"

describe Omniauth::Strava do
  describe 'self' do
    describe '#authenticate' do
      before do
        Omniauth::Strava.any_instance.stubs(:get_user_profile).returns {}
      end

      # TODO handle auth_code
      it 'returns user_info' do
        Omniauth::Strava.authenticate(access_token: 'adsf')[0].must_equal {}
      end

      it 'returns access_token' do
        Omniauth::Strava.authenticate(access_token: 'adsf')[1].must_equal 'adsf'
      end
    end

    describe '#deauthorize' do
      it 'returns the response' do
        response = MiniTest::Mock.new

        response.expect(:success?, true)
        response.expect(:parsed_response, [{ test: 'value' }])

        Omniauth::Strava.stub :post, response do
          Omniauth::Strava.deauthorize('test').must_equal [{ test: 'value' }]
        end

        assert response.verify
      end

      # TODO fix this
      # it 'returns throws Error' do
      #   response = MiniTest::Mock.new
      #
      #   response.expect(:success?, false)
      #
      #   Omniauth::Strava.stub :post, response do
      #     ->() { Omniauth::Strava.deauthorize('test') }.must_raise Omniauth::ResponseError
      #   end
      #
      #   assert response.verify
      # end
    end
  end

  # TODO this
  # describe '#get_user_profile' do
  #   it 'returns the response' do
  #     obj      = Omniauth::Strava.new
  #     response = MiniTest::Mock.new
  #
  #     response.expect(:success?, true)
  #     response.expect(:parsed_response, [{ test: 'value' }])
  #
  #     Omniauth::Strava.stub :get, response do
  #       obj.get_user_profile('test')
  #     end
  #
  #     assert response.verify
  #   end
  #
  #   it 'returns throws Error' do
  #     obj      = Omniauth::Strava.new
  #     response = MiniTest::Mock.new
  #
  #     response.expect(:success?, false)
  #
  #     Omniauth::Strava.stub :get, response do
  #       ->() { obj.get_user_profile('test') }.must_raise Omniauth::ResponseError
  #     end
  #
  #     assert response.verify
  #   end
  # end

  describe '#get_user_friends' do
    let(:response) { MiniTest::Mock.new }
    let(:provider) { MiniTest::Mock.new }

    it 'returns the response' do
      obj = Omniauth::Strava.new

      response.expect(:parsed_response, ['hi'])
      provider.expect(:access_token, 'test')
      provider.expect(:uid, 'test')

      Omniauth::Strava.stub :get, response do
        obj.get_user_friends(provider).must_equal ['hi']
      end

      assert response.verify
    end

    it 'returns empty array' do
      obj = Omniauth::Strava.new

      provider.expect(:access_token, 'test')

      Omniauth::Strava.stub :get, -> { raise ArgmentError.new } do
        obj.get_user_friends(provider).must_equal []
      end

      assert response.verify
    end
  end
end
