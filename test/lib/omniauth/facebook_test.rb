require "test_helper"
require "#{Rails.root}/lib/omniauth/facebook"

describe Omniauth::Facebook do
  # describe 'self' do
  #   describe '#authenticate' do
  #     before do
  #       Omniauth::Facebook.any_instance.stubs(:get_user_profile).returns({})
  #       Omniauth::Facebook.any_instance.stubs(:get_access_token).returns('adsf')
  #     end
  #
  #     # TODO auth_code
  #     it 'returns user_info' do
  #       Omniauth::Facebook.authenticate(access_token: 'adsf')[0].must_equal Hash.new
  #     end
  #
  #     it 'returns access_token' do
  #       Omniauth::Facebook.authenticate(access_token: 'adsf')[1].must_equal 'adsf'
  #     end
  #   end
  #
  #   describe '#deauthorize' do
  #     it 'returns the response' do
  #       response = MiniTest::Mock.new
  #
  #       response.expect(:success?, true)
  #       response.expect(:parsed_response, {})
  #
  #       Omniauth::Facebook.stub :delete, response do
  #         Omniauth::Facebook.deauthorize('test').must_equal Hash.new
  #       end
  #
  #       assert response.verify
  #     end
  #
  #     it 'returns throws Error' do
  #       response = MiniTest::Mock.new
  #
  #       response.expect(:success?, false)
  #
  #       Omniauth::Facebook.stub :delete, response do
  #         ->() { Omniauth::Facebook.deauthorize('test') }.must_raise Omniauth::ResponseError
  #       end
  #
  #       assert response.verify
  #     end
  #   end
  # end
  #
  # # TODO Fix this
  # # describe '#get_access_token' do
  # #   let(:response) { MiniTest::Mock.new }
  # #
  # #   it 'returns the response' do
  # #     obj      = Omniauth::Facebook.new
  # #
  # #     response.expect(:success?, true)
  # #     response.expect(:parsed_response, { 'access_token' => 'test'})
  # #
  # #     Omniauth::Facebook.stub :get, response do
  # #       obj.get_access_token('test').must_equal 'test'
  # #     end
  # #
  # #     assert response.verify
  # #   end
  # #
  # #   it 'returns throws Error' do
  # #     obj      = Omniauth::Facebook.new
  # #
  # #     response.expect(:success?, false)
  # #
  # #     Omniauth::Facebook.stub :get, response do
  # #       ->() { obj.get_access_token('test') }.must_raise Omniauth::ResponseError
  # #     end
  # #
  # #     assert response.verify
  # #   end
  # # end
  # #
  # # describe '#get_user_profile' do
  # #   let(:response) { MiniTest::Mock.new }
  # #
  # #   it 'returns the response' do
  # #     obj      = Omniauth::Facebook.new
  # #
  # #     response.expect(:success?, true)
  # #     response.expect(:parsed_response, [{ test: 'value' }])
  # #
  # #     Omniauth::Facebook.stub :get, response do
  # #       obj.get_user_profile('test').must_equal [{ test: 'value' }]
  # #     end
  # #
  # #     assert response.verify
  # #   end
  # #
  # #   it 'returns throws Error' do
  # #     obj      = Omniauth::Facebook.new
  # #
  # #     response.expect(:success?, false)
  # #
  # #     Omniauth::Facebook.stub :get, response do
  # #       ->() { obj.get_user_profile('test') }.must_raise Omniauth::ResponseError
  # #     end
  # #
  # #     assert response.verify
  # #   end
  # # end
  #
  # describe '#get_user_friends' do
  #   let(:response) { MiniTest::Mock.new }
  #   let(:provider) { MiniTest::Mock.new }
  #
  #   it 'returns the response' do
  #     obj      = Omniauth::Facebook.new
  #
  #     response.expect(:parsed_response, {'data' => ['hi']})
  #     provider.expect(:access_token, 'test')
  #
  #     Omniauth::Facebook.stub :get, response do
  #       obj.get_user_friends(provider).must_equal ['hi']
  #     end
  #
  #     assert response.verify
  #   end
  #
  #   it 'returns empty array' do
  #     obj      = Omniauth::Facebook.new
  #
  #     provider.expect(:access_token, 'test')
  #
  #     Omniauth::Facebook.stub :get, -> { raise ArgmentError.new } do
  #       obj.get_user_friends(provider).must_equal []
  #     end
  #
  #     assert response.verify
  #   end
  # end
end
