ENV["RAILS_ENV"] = "test"
require 'simplecov'
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"
require 'minitest/spec'
require 'mocha/mini_test'
require 'sidekiq/testing'
require "minitest/rails"
require 'shoulda'

Rails.logger.level = 4

class ActiveRecord::Base
  mattr_accessor :shared_connection
  @@shared_connection = nil

  def self.connection
    @@shared_connection || retrieve_connection
  end
end

# Forces all threads to share the same connection. This works on
# Capybara because it starts the web server in a thread.
ActiveRecord::Base.shared_connection = ActiveRecord::Base.connection

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
# require "minitest/rails/capybara"

# Uncomment for awesome colorful output
# require "minitest/pride"

# Minitest::Reporters.use!(Minitest::Reporters::SpecReporter.new(color: true),
#                          ENV,
#                          Minitest.backtrace_filter)

module SidekiqMinitestSupport
  def after_teardown
    Sidekiq::Worker.clear_all
  end
end

class MiniTest::Spec
  include SidekiqMinitestSupport
end

class ActiveSupport::TestCase
  ActiveRecord::Migration.check_pending!

  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...

  self.use_transactional_fixtures = true

  def login(user)
    @controller.stubs(:valid_doorkeeper_token?).returns(true)
    @controller.stubs(:current_resource_owner).returns(user)
  end
end
