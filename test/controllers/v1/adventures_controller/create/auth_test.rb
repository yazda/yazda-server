describe V1::AdventuresController do
  before do
    login users(:user_1)
  end

  describe 'without users' do
    it 'calls "AppboyService.track_event"' do
      AppboyService.any_instance.expects(:track_event)

      post :create,
           format:            :json,
           adventure_type:    'mountain_biking',
           name:              'Adventure Name',
           description:       'here is a quick description',
           start_time:        3.hours.from_now,
           end_time:          5.hours.from_now,
           reservation_limit: 3, lat: 0, lon: 0,
           skill_level:       3
    end

    describe 'http call' do
      before do
        post :create,
             format:            :json,
             adventure_type:    'mountain_biking',
             name:              'Adventure Name',
             description:       'here is a quick description',
             start_time:        3.hours.from_now,
             end_time:          5.hours.from_now,
             reservation_limit: 3, lat: 0, lon: 0,
             skill_level:       3
      end

      it 'creates the adventure' do
        response.status.must_equal 201
      end

      it 'returns the correct adventures' do
        body = JSON.parse response.body

        body['adventure']['name'].must_equal 'Adventure Name'
      end
    end
  end

  describe 'with club' do
    describe 'with incorrect club' do
      it 'returns 404' do
        post :create,
             format:            :json,
             adventure_type:    'mountain_biking',
             name:              'Adventure Name',
             description:       'here is a quick description',
             start_time:        3.hours.from_now,
             end_time:          5.hours.from_now,
             reservation_limit: 3, lat: 0, lon: 0, club_id: 1

        response.status.must_equal 404
      end
    end

    describe 'with club role club' do
      it 'returns 404' do
        post :create,
             format:            :json,
             adventure_type:    'mountain_biking',
             name:              'Adventure Name',
             description:       'here is a quick description',
             start_time:        3.hours.from_now,
             end_time:          5.hours.from_now,
             reservation_limit: 3, lat: 0, lon: 0,
             club_id:           clubs(:club_1).id

        response.status.must_equal 201
      end
    end
  end

  describe 'with location' do
    describe 'with correct location' do
      before do
        post :create,
             format:            :json,
             adventure_type:    'mountain_biking',
             name:              'Adventure Name',
             description:       'here is a quick description',
             start_time:        3.hours.from_now,
             end_time:          5.hours.from_now,
             reservation_limit: 3, location_id: locations(:one).id,
             skill_level:       3
      end

      it 'creates the adventure' do
        response.status.must_equal 201
      end
    end

    describe 'with incorrect location' do
      it 'returns 404' do
        post :create,
             format:            :json,
             adventure_type:    'mountain_biking',
             name:              'Adventure Name',
             description:       'here is a quick description',
             start_time:        3.hours.from_now,
             end_time:          5.hours.from_now,
             reservation_limit: 3, location_id: 1

        response.status.must_equal 404
      end
    end
  end
end
