describe V1::AdventuresController do
  before :each do
    post :create, format: :json
  end

  it 'is is unauthorized' do
    response.status.must_equal 401
  end
end
