describe V1::AdventuresController do
  before do
    login users(:user_1)
  end

  # TODO
  # describe 'with club_id' do
  #   let(:returned_adventures) do
  #     [:club_public]
  #   end
  #
  #   let(:unreturned_adventures) do
  #     [:public_owned_user_1, :link_owned_user_1, :private_owned_user_1,
  #      :link_outside_range, :private_outside_range, :invited_link_outside_range,
  #      :invited_private_outside_range, :rejected_link_outside_range,
  #      :rejected_private_outside_range, :club_link, :club_private,
  #      :public_outside_range, :invited_public_outside_range,
  #      :rejected_public_outside_range, :past]
  #   end
  #
  #   before do
  #     get :index, format: :json, club_id: clubs(:club_1).id
  #   end
  #
  #   it 'returns success' do
  #     response.status.must_equal 200
  #   end
  #
  #   it 'returns the correct adventures' do
  #     body = JSON.parse response.body
  #
  #     returned_adventures.each do |sym|
  #       name = adventures(sym).name
  #       body['adventures'].map { |a| a['name'] }.must_include name
  #     end
  #
  #     unreturned_adventures.each do |sym|
  #       name = adventures(sym).name
  #       body['adventures'].map { |a| a['name'] }.wont_include name
  #     end
  #   end
  # end
  #
  # describe 'status' do
  #   describe 'past' do
  #     let(:returned_adventures) do
  #       [:past]
  #     end
  #
  #     let(:unreturned_adventures) do
  #       [:public_owned_user_1, :link_owned_user_1, :private_owned_user_1,
  #        :link_outside_range, :private_outside_range, :invited_link_outside_range,
  #        :invited_private_outside_range, :rejected_link_outside_range,
  #        :rejected_private_outside_range, :club_link, :club_private,
  #        :public_outside_range, :invited_public_outside_range,
  #        :rejected_public_outside_range, :club_public]
  #     end
  #
  #     it 'returns past adventures' do
  #       get :index, format: :json, status: 'past'
  #       body = JSON.parse response.body
  #
  #       returned_adventures.each do |sym|
  #         name = adventures(sym).name
  #         body['adventures'].map { |a| a['name'] }.must_include name
  #       end
  #
  #       unreturned_adventures.each do |sym|
  #         name = adventures(sym).name
  #         body['adventures'].map { |a| a['name'] }.wont_include name
  #       end
  #     end
  #   end
  # end
end
