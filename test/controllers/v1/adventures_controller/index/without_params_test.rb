describe V1::AdventuresController do
  before do
    login users(:user_1)
  end

  describe 'without params' do
    let(:returned_adventures) do
      [:public_owned_user_1, :invited_public_outside_range,
       :link_owned_user_1, :private_owned_user_1, :invited_link_outside_range,
       :invited_private_outside_range, :club_public, :club_link, :club_private]
    end

    let(:unreturned_adventures) do
      [:link_outside_range, :private_outside_range,
       :rejected_link_outside_range, :rejected_public_outside_range,
       :rejected_private_outside_range, :past,
       :public_outside_range]
    end

    it 'returns success' do
      get :index, format: :json
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      get :index, format: :json
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end
  # TODO adventure_type
end
