describe V1::AdventuresController do
  before do
    login users(:user_1)
  end

  describe 'with user_id' do
    let(:returned_adventures) do
      [:public_owned_user_1]
    end

    let(:unreturned_adventures) do
      [:link_owned_user_1, :private_owned_user_1, :link_outside_range,
       :private_outside_range, :invited_link_outside_range,
       :invited_private_outside_range, :rejected_link_outside_range,
       :rejected_private_outside_range, :club_link, :club_private,
       :public_outside_range, :invited_public_outside_range,
       :rejected_public_outside_range, :club_public, :past]
    end

    before do
      get :index, format: :json, user_id: users(:user_1).id
    end

    it 'returns success' do
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end
end
