describe V1::AdventuresController do
  before do
    login users(:user_1)
  end

  describe 'with lat/lon/distance' do
    let(:returned_adventures) do
      [:public_owned_user_1, :link_owned_user_1, :private_owned_user_1,
       :club_link, :club_private, :club_public, :invited_link_outside_range,
       :invited_private_outside_range, :invited_public_outside_range]
    end

    let(:unreturned_adventures) do
      [:link_outside_range, :private_outside_range, :rejected_link_outside_range,
       :rejected_private_outside_range, :public_outside_range,
       :rejected_public_outside_range, :past]
    end

    it 'returns success' do
      get :index, format: :json, lat: 0, lon: 0, distance: 20
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      get :index, format: :json, lat: 0, lon: 0, distance: 20
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end
end
