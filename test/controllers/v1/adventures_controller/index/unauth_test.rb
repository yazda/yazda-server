describe V1::AdventuresController do
  include Rails.application.routes.url_helpers

  describe 'without params' do
    let(:returned_adventures) do
      [:public_owned_user_1, :public_outside_range,
       :invited_public_outside_range, :rejected_public_outside_range,
       :club_public]
    end

    let(:unreturned_adventures) do
      [:link_owned_user_1, :private_owned_user_1, :link_outside_range,
       :private_outside_range, :invited_link_outside_range,
       :invited_private_outside_range, :rejected_link_outside_range,
       :rejected_private_outside_range, :club_link, :club_private, :past]
    end

    it 'returns success' do
      get :index, format: :json
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      get :index, format: :json
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end

  # TODO change this to actually return stuff around the area
  # describe 'with lat/lon/distance' do
  #   let(:returned_adventures) do
  #     [:public_owned_user_1]
  #   end
  #
  #   let(:unreturned_adventures) do
  #     [:link_owned_user_1, :private_owned_user_1, :link_outside_range,
  #      :private_outside_range, :invited_link_outside_range,
  #      :invited_private_outside_range, :rejected_link_outside_range,
  #      :rejected_private_outside_range, :club_link, :club_private,
  #      :public_outside_range, :invited_public_outside_range,
  #      :rejected_public_outside_range, :club_public, :past]
  #   end
  #
  #   it 'returns success' do
  #     get :index, format: :json, lat: 0, lon: 0, distance: 100
  #     response.status.must_equal 200
  #   end
  #
  #   it 'returns the correct adventures' do
  #     get :index, format: :json, lat: 0, lon: 0, distance: 100
  #     body = JSON.parse response.body
  #
  #     returned_adventures.each do |sym|
  #       name = adventures(sym).name
  #       body['adventures'].map { |a| a['name'] }.must_include name
  #     end
  #
  #     unreturned_adventures.each do |sym|
  #       name = adventures(sym).name
  #       body['adventures'].map { |a| a['name'] }.wont_include name
  #     end
  #   end
  # end

  describe 'with user_id' do
    let(:returned_adventures) do
      [:public_owned_user_1]
    end

    let(:unreturned_adventures) do
      [:link_owned_user_1, :private_owned_user_1, :link_outside_range,
       :private_outside_range, :invited_link_outside_range,
       :invited_private_outside_range, :rejected_link_outside_range,
       :rejected_private_outside_range, :club_link, :club_private,
       :public_outside_range, :invited_public_outside_range,
       :rejected_public_outside_range, :club_public, :past]
    end

    before do
      get :index, format: :json, user_id: users(:user_1).id
    end

    it 'returns success' do
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end

  describe 'with club_id' do
    let(:returned_adventures) do
      [:club_public]
    end

    let(:unreturned_adventures) do
      [:public_owned_user_1, :link_owned_user_1, :private_owned_user_1,
       :link_outside_range, :private_outside_range, :invited_link_outside_range,
       :invited_private_outside_range, :rejected_link_outside_range,
       :rejected_private_outside_range, :club_link, :club_private,
       :public_outside_range, :invited_public_outside_range,
       :rejected_public_outside_range, :past]
    end

    before do
      get :index, format: :json, club_id: clubs(:club_1).id
    end

    it 'returns success' do
      response.status.must_equal 200
    end

    it 'returns the correct adventures' do
      body = JSON.parse response.body

      returned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.must_include name
      end

      unreturned_adventures.each do |sym|
        name = adventures(sym).name
        body['adventures'].map { |a| a['name'] }.wont_include name
      end
    end
  end

  describe 'status' do
    describe 'past' do
      let(:returned_adventures) do
        [:past]
      end

      let(:unreturned_adventures) do
        [:public_owned_user_1, :link_owned_user_1, :private_owned_user_1,
         :link_outside_range, :private_outside_range, :invited_link_outside_range,
         :invited_private_outside_range, :rejected_link_outside_range,
         :rejected_private_outside_range, :club_link, :club_private,
         :public_outside_range, :invited_public_outside_range,
         :rejected_public_outside_range, :club_public]
      end

      it 'returns past adventures' do
        get :index, format: :json, status: 'past'
        body = JSON.parse response.body

        returned_adventures.each do |sym|
          name = adventures(sym).name
          body['adventures'].map { |a| a['name'] }.must_include name
        end

        unreturned_adventures.each do |sym|
          name = adventures(sym).name
          body['adventures'].map { |a| a['name'] }.wont_include name
        end
      end
    end
  end
end
