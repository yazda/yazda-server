describe V1::AdventuresController do
  before :each do
    login users(:user_1)
  end

  describe 'owned adventure' do
    before do
      AppboyService.any_instance.expects(:track_event).with('delete-adventure', adventures(:public_owned_user_1))
      CancelAdventureWorker.expects(:perform_async).with id

      Adventure.any_instance.expects(:cancel!)

      delete :destroy, id: id, format: :json
    end

    let(:id) { adventures(:public_owned_user_1).id }

    it 'is successful' do
      response.status.must_equal 204
    end

    it 'should return no content' do
      response.body.must_be_empty
    end
  end

  describe 'unowned adventure' do
    let(:id) { adventures(:invited_public_outside_range).id }

    it 'is not found' do
      delete :destroy, id: id, format: :json

      response.status.must_equal 403
    end
  end
end
