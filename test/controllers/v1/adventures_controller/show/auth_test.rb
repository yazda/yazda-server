describe V1::AdventuresController do
  before :each do
    login users(:user_1)
    get :show, id: id, format: :json
  end

  describe 'public adventure' do
    let(:id) { adventures(:public_owned_user_1).id }

    it 'is successful' do
      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      body = JSON.parse response.body

      body['adventure']['id'].must_equal id
    end
  end

  describe 'link adventure' do
    let(:id) { adventures(:link_owned_user_1).id }

    it 'is successful' do
      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      body = JSON.parse response.body

      body['adventure']['id'].must_equal id
    end
  end

  describe 'invited private adventure' do
    let(:id) { adventures(:invited_private_outside_range).id }

    it 'should be success' do
      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      get :show, id: id, format: :json
      body = JSON.parse response.body

      body['adventure']['id'].must_equal id
    end
  end

  describe 'uninvited private adventure' do
    let(:id) { adventures(:private_uninvited_user_1).id }

    it 'should be unauthorized' do
      response.status.must_equal 403
    end
  end
end
