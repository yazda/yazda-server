describe V1::AdventuresController do
  before :each do
    get :show, id: id, format: :json
  end

  describe 'public adventure' do
    let(:id) { adventures(:public_owned_user_1).id }

    it 'is successful' do
      get :show, id: id, format: :json
      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      get :show, id: id, format: :json
      body = JSON.parse response.body

      body['adventure']['id'].must_equal id
    end
  end

  describe 'link adventure' do
    let(:id) { adventures(:link_owned_user_1).id }

    it 'is successful' do
      get :show, id: id, format: :json
      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      get :show, id: id, format: :json
      body = JSON.parse response.body

      body['adventure']['id'].must_equal id
    end
  end

  describe 'private adventure' do
    let(:id) { adventures(:private_owned_user_1).id }

    it 'should be unauthorized' do
      get :show, id: id, format: :json
      response.status.must_equal 403
    end
  end
end
