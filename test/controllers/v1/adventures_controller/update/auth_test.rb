describe V1::AdventuresController do
  before :each do
    login users(:user_1)
  end

  describe 'owned adventure' do
    let(:id) { adventures(:public_owned_user_1).id }

    it 'is successful' do
      Adventure.any_instance.expects(:save!)
      patch :update, id: id, name: 'new name', format: :json

      response.status.must_equal 200
    end

    it 'should return the correct adventure' do
      Adventure.any_instance.expects(:save!)
      patch :update, id: id, name: 'new name', format: :json

      body = JSON.parse response.body
      body['adventure']['id'].must_equal id
    end

    it 'should perform worker if changes detected' do
      Adventure.any_instance.expects(:save!)
      AppboyService.any_instance.expects(:track_event)
        .with('update-adventure', adventures(:public_owned_user_1))

      patch :update, id: id, name: 'new name', format: :json
    end

    # TODO this isn't work for some weird reason
    # it 'should not perform worker if changes detected' do
    #   Adventure.any_instance.expects(:save!)
    #   AppboyService.any_instance.expects(:track_event).never
    #
    #   patch :update, id: id, format: :json
    # end
  end


  describe 'unowned adventure' do
    let(:id) { adventures(:invited_public_outside_range).id }

    it 'is not found' do
      patch :update, id: id, format: :json, name: 'new name'

      response.status.must_equal 403
    end
  end
end
