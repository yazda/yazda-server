describe V1::AdventuresController do
  before :each do
    patch :update, id: 1, format: :json
  end

  it 'is is unauthorized' do
    response.status.must_equal 401
  end
end
