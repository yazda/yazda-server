describe V1::FriendsController do
  include Rails.application.routes.url_helpers

  describe 'success' do
    before do
      get :index, format: :json, user_id: users(:user_1).id
    end

    it 'is is successful' do
      response.status.must_equal 200
    end

    it 'returns a list of friends' do
      json = JSON.parse response.body

      json['users'].count.must_equal 1
      json['users'][0]['name'].must_equal 'user2'
    end

    # TODO: adventure_type params
    # TODO: q params
  end

  describe 'failures' do
    before do
      get :index, format: :json, user_id: -1
    end

    it 'is is 404' do
      response.status.must_equal 404
    end
  end
end
