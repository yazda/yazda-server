require 'test_helper'

describe NotificationPreference do
  let(:notification_preference) do
    NotificationPreference.new(user: users(:user_1))
  end

  it 'must be valid' do
    notification_preference.must_be :valid?
  end

  validate_presence_of :adventure_invite
  validate_presence_of :adventure_edit
  validate_presence_of :adventure_cancel
  validate_presence_of :adventure_reminder
  validate_presence_of :adventure_joined
  validate_presence_of :chat
  validate_presence_of :friend
  validate_presence_of :user

  validate_inclusion_of(:adventure_invite).in_array ['both', 'email', 'push']
  validate_inclusion_of(:adventure_edit).in_array ['both', 'email', 'push']
  validate_inclusion_of(:adventure_cancel).in_array ['both', 'email', 'push']
  validate_inclusion_of(:adventure_reminder).in_array ['both', 'email',
                                                       'push', 'none']
  validate_inclusion_of(:adventure_joined).in_array ['both', 'email', 'push', 'none']
  validate_inclusion_of(:chat).in_array ['both', 'email', 'push', 'none']
  validate_inclusion_of(:friend).in_array ['both', 'email', 'push', 'none']

  should belong_to :user

  describe 'depreciations' do
    describe '#invite' do
      it 'should set correct values when true' do
        notification_preference.invite = false
        notification_preference.save!
        notification_preference.invite = true
        notification_preference.save!

        notification_preference.adventure_invite.must_equal 'both'
        notification_preference.adventure_edit.must_equal 'both'
        notification_preference.adventure_cancel.must_equal 'both'
        notification_preference.adventure_reminder.must_equal 'both'
        notification_preference.adventure_joined.must_equal 'both'
      end

      it 'should set correct values when false' do
        notification_preference.invite = false
        notification_preference.save!

        notification_preference.adventure_invite.must_equal 'email'
        notification_preference.adventure_edit.must_equal 'email'
        notification_preference.adventure_cancel.must_equal 'email'
        notification_preference.adventure_reminder.must_equal 'none'
        notification_preference.adventure_joined.must_equal 'none'
      end
    end

    describe '#friend_request' do
      it 'should set correct values when true' do
        notification_preference.friend_request = false
        notification_preference.save!
        notification_preference.friend_request = true
        notification_preference.save!

        notification_preference.friend.must_equal 'both'
      end

      it 'should set correct values when false' do
        notification_preference.friend_request = false
        notification_preference.save!

        notification_preference.friend.must_equal 'none'
      end
    end
  end
end
