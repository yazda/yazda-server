require "test_helper"

describe User do
  let(:user) do
    User.new(
      email:                   'test@test.com',
      name:                    'Test',
      password:                'password',
      password_confirmation:   'password',
      notification_preference: notification_preferences(:notification_preference_1)
    )
  end

  it 'must be valid' do
    user.must_be :valid?
  end

  # name
  should validate_presence_of :name
  should validate_length_of(:name).is_at_least(2).is_at_most(255)

  # friendships
  should have_many(:friendships).dependent(:destroy)
  should have_many(:pending_friendships).class_name('Friendship')
  should have_many(:requested_friendships).class_name('Friendship')
  should have_many(:rejected_friendships).class_name('Friendship')

  # friends
  should have_many(:friends).through(:approved_friendships)

  # friends
  should have_many(:followers).through(:inverse_friendships).class_name('User').source :user

  # inverse_friendships
  should have_many(:inverse_friendships)
           .class_name('Friendship')
           .with_foreign_key('friend_id')
           .dependent(:destroy)
  should have_many(:incoming_friendships)
           .class_name('Friendship')
           .with_foreign_key('friend_id')

  # events
  should have_many(:adventures).dependent(:destroy)

  # invites
  should have_many(:invites).dependent(:destroy)

  # clubs
  should have_many(:club_roles).dependent(:destroy)
  should have_many(:clubs).through(:club_roles)

  # methods
  describe '#facebook?' do
    it 'should return true if provider has facebook' do
      user.save!
      user.facebook?.wont_equal true

      user.providers << Provider.new(provider: 'facebook', uid: 'test')

      user.facebook?.must_equal true
    end
  end

  describe '#facebook_image' do

  end

  describe '#view_preferences' do

  end

  describe '#adventure_leader?' do
    it 'returns true' do
      emails = %w(james.stoup@gmail.com lauren@theinvisiblechecklist.com)

      emails.each do |email|
        user.email = email
        user.adventure_leader?.must_equal true
      end
    end

    it 'returns false' do
      emails = %w(test@gmail.com bill@theinvisiblechecklist.com ogm@yazdapp.com)

      emails.each do |email|
        user.email = email
        user.adventure_leader?.must_equal false
      end
    end
  end

  describe '#views_mountain_biking?' do
    it 'returns true if preferences views_mountain_biking' do
      user.adventure_type_preferences = ['mountain_biking']
      user.views_mountain_biking?.must_equal true
    end

    it 'returns false if preferences views_mountain_biking' do
      user.adventure_type_preferences = []
      user.views_mountain_biking?.wont_equal true
    end
  end

  describe '#views_biking?' do
    it 'returns true if preferences biking' do
      user.adventure_type_preferences = ['biking']
      user.views_biking?.must_equal true
    end

    it 'returns false if preferences biking' do
      user.adventure_type_preferences = []
      user.views_biking?.wont_equal true
    end
  end

  describe '#views_running?' do
    it 'returns true if preferences views_running' do
      user.adventure_type_preferences = ['running']
      user.views_running?.must_equal true
    end

    it 'returns false if preferences views_running' do
      user.adventure_type_preferences = []
      user.views_running?.wont_equal true
    end
  end

  describe '#views_trail_running?' do
    it 'returns true if preferences views_trail_running' do
      user.adventure_type_preferences = ['trail_running']
      user.views_trail_running?.must_equal true
    end

    it 'returns false if preferences views_trail_running' do
      user.adventure_type_preferences = []
      user.views_trail_running?.wont_equal true
    end
  end

  describe '#views_hiking?' do
    it 'returns true if preferences views_hiking' do
      user.adventure_type_preferences = ['hiking']
      user.views_hiking?.must_equal true
    end

    it 'returns false if preferences views_hiking' do
      user.adventure_type_preferences = []
      user.views_hiking?.wont_equal true
    end
  end

  describe '#notifies_invite=' do
    it 'sets to true' do
      user.notifies_invite = true
      user.notification_preference.invite.must_equal true
    end

    it 'sets to false' do
      user.notifies_invite = false
      user.notification_preference.invite.must_equal false
    end
  end

  describe '#notifies_invite' do
    it 'returns true' do
      user.notification_preference.invite = true
      user.notifies_invite?.must_equal true
    end

    it 'returns false' do
      user.notification_preference.invite = false
      user.notifies_invite?.must_equal false
    end
  end

  describe '#notifies_friend_request=' do
    it 'sets to true' do
      user.notifies_friend_request = true
      user.notification_preference.friend_request.must_equal true
    end

    it 'sets to false' do
      user.notifies_friend_request = false
      user.notification_preference.friend_request.must_equal false
    end
  end

  describe '#notifies_friend_request' do
    it 'returns true' do
      user.notification_preference.friend_request = true
      user.notifies_friend_request?.must_equal true
    end

    it 'returns false' do
      user.notification_preference.friend_request = false
      user.notifies_friend_request?.must_equal false
    end
  end
end
