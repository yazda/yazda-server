require "test_helper"

describe Adventure do
  let(:adventure) do
    adventure = Adventure.new(owner:          users(:user_1),
                              adventure_type: Adventure.adventure_types[:mountain_biking],
                              name:           'Test adventure',
                              start_time:     1.week.from_now,
                              end_time:       8.days.from_now,
                              privacy:        0,
                              lat:            0,
                              lon:            0)

    invite = Invite.new(user: users(:user_2))
    adventure.invites << invite
    adventure
  end

  it 'must be valid' do
    adventure.must_be :valid?
  end

  should belong_to(:club)
  should belong_to(:location)
  should belong_to(:owner).class_name('User').with_foreign_key('user_id').counter_cache(true)
  should have_many(:invites).dependent(:destroy)
  should have_many(:users).through(:invites)

  # owner
  should validate_presence_of(:owner)

  # type
  should validate_presence_of(:adventure_type)
  should define_enum_for(:adventure_type).with([:mountain_biking, :hiking,
                                                :biking, :running,
                                                :trail_running])

  should define_enum_for(:privacy).with([:public_adventure,
                                         :private_adventure, :link])

  # name
  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_least(5).is_at_most(255)

  # start_time
  # TODO validate after today
  # should validate_presence_of(:start_time)

  # lat
  should validate_presence_of(:lat)
  should validate_numericality_of(:lat)

  # lon
  should validate_presence_of(:lon)
  should validate_numericality_of(:lon)

  # duration
  should validate_numericality_of(:duration).only_integer.is_greater_than 0

  # skill_level
  should validate_numericality_of(:skill_level)
           .is_greater_than_or_equal_to(1)
           .is_less_than_or_equal_to(5)
           .only_integer

  # description
  should validate_length_of(:description).is_at_most(50_000)

  # reservation_limit
  should validate_numericality_of(:reservation_limit)
           .only_integer
           .is_greater_than_or_equal_to(0)

  # skill_level
  should validate_numericality_of(:skill_level)
           .only_integer
           .is_greater_than_or_equal_to(1)
           .is_less_than_or_equal_to(5)

  describe 'validations' do
    describe 'invites' do
      # TODO: this
    end

    describe 'club' do
      before do
        @club_role     = ClubRole.create user: adventure.owner, club: clubs(:club_2)
        adventure.club = @club_role.club
      end

      it 'is valid when owner' do
        @club_role.owner!
        adventure.must_be :valid?
      end

      it 'is valid when admin' do
        @club_role.admin!
        adventure.must_be :valid?
      end

      it 'is valid when leader' do
        @club_role.leader!
        adventure.must_be :valid?
      end

      it 'is invalid when member' do
        @club_role.member!
        adventure.wont_be :valid?

        adventure.errors.messages[:club].must_include 'You need leader or above permissions for this club'
      end
    end
  end

  describe '#is_sponsored?' do
    it 'returns false if no club' do
      adventure.club = nil
      adventure.is_sponsored?.must_equal false
    end

    it 'returns false if is_yazda? is false' do
      Club.any_instance.expects(:is_yazda?).returns false
      adventure.club = clubs(:club_1)

      adventure.is_sponsored?.must_equal false
    end

    it 'returns true if is_yazda? is true' do
      Club.any_instance.expects(:is_yazda?).returns true
      adventure.club = clubs(:club_1)

      adventure.is_sponsored?.must_equal true
    end
  end

  # invited?
  describe '#invited?' do
    it 'should return true for invited users' do
      adventure.save!
      adventure.invited?(adventure.invites.first.user).must_equal true
    end

    it 'should return true for owners' do
      adventure.invited?(users :user_1).must_equal true
    end

    it 'should return false for uninvited users' do
      adventure.invited?(users :user_3).wont_equal true
    end
  end

  # attending?
  describe '#attending?' do
    it 'should return true for users who are attending' do
      adventure.invites.first.accept!
      adventure.attending?(adventure.invites.first.user).must_equal true
    end

    it 'should return true for owners' do
      adventure.attending?(adventure.owner).must_equal true
    end

    it 'should return false for pending' do
      adventure.attending?(adventure.invites.first.user).wont_equal true
    end

    it 'should return false for na' do
      adventure.invites.first.reject!
      adventure.attending?(adventure.invites.first.user).wont_equal true
    end

    it 'should return false for uninvited users' do
      adventure.attending?(users :user_3).wont_equal true
    end
  end

  # responded?
  describe '#responded?' do
    it 'should return true for users who are attending' do
      adventure.invites.first.accept!
      adventure.responded?(adventure.invites.first.user).must_equal true
    end

    it 'should return true for owners' do
      adventure.responded?(adventure.owner).must_equal true
    end

    it 'should return false for pending' do
      adventure.responded?(adventure.invites.first.user).wont_equal true
    end

    it 'should return true for na' do
      adventure.invites.first.reject!
      adventure.responded?(adventure.invites.first.user).must_equal true
    end

    it 'should return false for uninvited users' do
      adventure.responded?(users :user_3).wont_equal true
    end
  end

  # cancel!
  describe '#cancel!' do
    it 'should set canceled' do
      adventure.save

      adventure.canceled.must_equal false
      adventure.canceled_date.must_be_nil
      adventure.cancel!

      adventure.canceled.must_equal true
      adventure.canceled_date.must_be_kind_of ActiveSupport::TimeWithZone
    end
  end

  # before_validation
  describe 'before_validate' do
    # TODO validation calls
  end

  # scopes
  describe 'scopes' do
    describe 'club' do

    end

    describe 'canceled' do

    end

    describe 'user' do

    end

    describe 'by_status_events_and_location' do

    end

    describe 'by_status' do

    end

    describe 'by_adventure_type' do
      it 'should return by type if type passed' do
        Adventure.by_adventure_type(1, nil)
          .must_equal Adventure.where(adventure_type: 1)
      end

      it 'should return by view_preferences if no type' do
        Adventure.by_adventure_type(nil, users(:user_1))
          .must_equal Adventure.where(adventure_type: users(:user_1)
                                                        .view_preferences)
      end
    end

    describe 'not_notified' do
      it 'should return adventures that have alert_sent_at === nil' do
        Adventure.not_notified.each do |adventure|
          adventure.alert_sent_at.must_equal nil
        end
      end
    end
  end
end
