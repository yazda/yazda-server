require "test_helper"

describe Invite do
  let(:invite) {
    invite = Invite.new(user: users(:user_4), adventure: adventures(:public_owned_user_1))
  }

  it 'must be valid' do
    invite.must_be :valid?
  end

  should define_enum_for(:attending).with([:ya, :pending, :na])

  # Validations
  should validate_presence_of(:user)
  should validate_presence_of(:invited_by)
  should validate_presence_of(:adventure)
  should validate_uniqueness_of(:adventure).scoped_to(:user_id)

  it 'should validate adventure not canceled' do
    invite.adventure.canceled = true

    invite.wont_be :valid?
  end

  # TODO: get these working
  describe 'reservation_limit changes' do
    before do
      invite.adventure.reservation_limit = 0
    end

    it 'should validate if adventure is full & attending is "ya"' do
      invite.attending = :ya
      invite.wont_be :valid?
    end

    it 'should not validate if adventure is full & attending is "na"' do
      invite.attending = :na
      invite.must_be :valid?
    end

    it 'should not validate if adventure is full & attending is "pending"' do
      invite.attending = :pending
      invite.must_be :valid?
    end
  end

  # associations
  # TODO get these working again
  # should belong_to(:user)
  # should belong_to(:adventure)
  # should belong_to(:invited_by).class_name('User')

  # class methods
  describe 'sef.status_invite_arel' do

  end

  # methods
  describe '#accept!' do
    it 'should set values' do
      invite.accepted_at.must_be_nil
      invite.rejected_at.must_be_nil
      invite.ya?.must_equal false

      invite.accept!

      invite.accepted_at.must_be_kind_of ActiveSupport::TimeWithZone
      invite.rejected_at.must_be_nil
      invite.ya?.must_equal true
    end

    it 'should return nil if ya?' do
      invite.ya!

      invite.accept!.must_equal nil
    end
  end

  describe '#reject!' do
    it 'should set values' do
      invite.accepted_at.must_be_nil
      invite.rejected_at.must_be_nil
      invite.na?.must_equal false

      invite.reject!

      invite.rejected_at.must_be_kind_of ActiveSupport::TimeWithZone
      invite.accepted_at.must_be_nil
      invite.na?.must_equal true
    end

    it 'should return nil if na?' do
      invite.na!

      invite.reject!.must_equal nil
    end
  end

  describe 'scopes' do
    it 'should contain future invites' do
      Invite.future.must_include invites(:user_2_pending_future_invite)
      Invite.future.must_include invites(:user_3_ya_future_invite)
      Invite.future.must_include invites(:user_4_na_future_invite)

      Invite.future.wont_include invites(:user_2_pending_past_invite)
      Invite.future.wont_include invites(:user_3_ya_past_invite)
      Invite.future.wont_include invites(:user_4_na_past_invite)
    end
  end

  # TODO test to_csv
end
