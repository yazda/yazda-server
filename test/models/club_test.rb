require "test_helper"

describe Club do
  let(:club) do
    Club.new(
      name:          'Testing',
      description:   'here is a description of the club',
      website:       'http://yazdaapp.com',
      location:      'Denver, CO',
      contact_email: 'info@yazdaapp.com'
    )
  end

  it 'must be valid' do
    club.must_be :valid?
  end

  # validations
  should validate_length_of(:description).is_at_most(50_000)
  should allow_value('', nil).for(:description)

  should validate_length_of(:name).is_at_least(4).is_at_most(255)

  # should validate_length_of(:website).is_at_most(255)

  should validate_presence_of(:location)
  should validate_length_of(:location).is_at_most(255)

  should validate_presence_of(:lat)
  should validate_numericality_of(:lat)

  should validate_presence_of(:lon)
  should validate_numericality_of(:lon)

  should validate_length_of(:contact_email).is_at_most(255)
  should allow_value('', nil).for(:contact_email)

  # associations
  should have_many(:adventures).dependent(:destroy)
  should have_many(:club_roles).dependent(:destroy)
  should have_many(:users).through(:club_roles)

  describe '#is_yazda?' do
    it 'returns false' do
      club.is_yazda?.must_equal false
    end

    it 'returns true' do
      club.id = 1
      club.is_yazda?.must_equal true
    end
  end
end
