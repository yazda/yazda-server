require "test_helper"

describe Friendship do
  let(:user_1) { users(:user_1) }
  let(:user_3) { users(:user_3) }
  let(:friendship) { Friendship.new(user: user_1, friend: user_3) }

  it 'must be valid' do
    friendship.must_be :valid?
  end

  # enum
  should define_enum_for(:status).with([:pending,
                                        :approved,
                                        :rejected,
                                        :requested])

  # validations
  should validate_presence_of(:user)
  should validate_presence_of(:friend)
  should validate_uniqueness_of(:friend).scoped_to(:user_id)

  # associations
  should belong_to(:user)
  should belong_to(:friend).class_name('User')

  describe '#accept!' do
    it 'must set accepted_at' do
      date = DateTime.now
      mock = MiniTest::Mock.new

      mock.expect :accepted_at=, nil, [date]
      mock.expect :rejected_at=, nil, [nil]
      mock.expect :approved!, true
      mock.expect :pending?, true

      mock.expect :accepted_at=, nil, [date]
      mock.expect :rejected_at=, nil, [nil]
      mock.expect :approved!, true
      mock.expect :pending?, true

      DateTime.stub(:now, date) do
        Friendship.stub(:find_by, mock) do
          friendship.accept!
        end
      end

      mock.verify
    end

    it 'does not execute unless not pending or not requested' do
      date = DateTime.now
      mock = MiniTest::Mock.new
      mock.expect :accepted_at=, nil, [date]
      mock.expect :rejected_at=, nil, [nil]
      mock.expect :pending?, false
      mock.expect :requested?, false

      mock.expect :accepted_at=, nil, [date]
      mock.expect :rejected_at=, nil, [nil]
      mock.expect :pending?, false
      mock.expect :requested?, false

      DateTime.stub(:now, date) do
        Friendship.stub(:find_by, mock) do
          friendship.accept!
        end
      end

      mock.verify
    end
  end

  describe '#reject!' do
    it 'must set rejected_at' do
      date = DateTime.now
      mock = MiniTest::Mock.new
      mock.expect :rejected_at=, nil, [date]
      mock.expect :accepted_at=, nil, [nil]
      mock.expect :pending?, true
      mock.expect :rejected!, true

      mock.expect :rejected_at=, nil, [date]
      mock.expect :accepted_at=, nil, [nil]
      mock.expect :pending?, true
      mock.expect :rejected!, true

      DateTime.stub(:now, date) do
        Friendship.stub(:find_by, mock) do
          friendship.reject!

          mock.verify
        end
      end
    end

    it 'does not execute unless not pending or not requested' do
      date = DateTime.now
      mock = MiniTest::Mock.new
      mock.expect :rejected_at=, nil, [date]
      mock.expect :accepted_at=, nil, [nil]
      mock.expect :pending?, false
      mock.expect :requested?, false

      mock.expect :rejected_at=, nil, [date]
      mock.expect :accepted_at=, nil, [nil]
      mock.expect :pending?, false
      mock.expect :requested?, false

      DateTime.stub(:now, date) do
        Friendship.stub(:find_by, mock) do
          friendship.reject!

          mock.verify
        end
      end
    end
  end

  describe '#require_friend' do
    let(:user) { users(:user_1)}
    let(:friend) { users(:user_5)}

    it 'creates a new friendship' do
      Friendship.expects(:create!).with(user: user, friend: friend, status: :pending)
      Friendship.expects(:create!).with(user: friend, friend: user, status: :requested)

      Friendship.request_friend(user, friend)
    end
  end
end
