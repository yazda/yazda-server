require "test_helper"

describe ClubRole do
  let(:club_role) do
    ClubRole.new(
      club:             clubs(:club_4),
      user:             users(:user_2),
      adventure_invite: 'none',
      chat:             'none',
      role:             0
    )
  end

  it 'must be valid' do
    club_role.must_be :valid?
  end

  should validate_presence_of :role
  should validate_presence_of :club
  should validate_presence_of :user
  should validate_uniqueness_of(:user).scoped_to :club_id

  should validate_presence_of :adventure_invite
  should validate_inclusion_of(:adventure_invite).in_array(%w(both email push none))

  should validate_presence_of :chat
  should validate_inclusion_of(:chat).in_array(%w(both email push none))
end
