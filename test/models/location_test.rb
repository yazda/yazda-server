require "test_helper"

describe Location do
  subject { Location.new(lat: 0, lon: 0, name: 'test name') }

  it "must be valid" do
    value(subject).must_be :valid?
  end

  should have_many(:adventures)

  should validate_presence_of :lat
  should validate_numericality_of :lat

  should validate_presence_of :lon
  should validate_numericality_of :lon

  should validate_presence_of(:name)
  should validate_length_of(:name).is_at_least(3).is_at_most(255)
end
