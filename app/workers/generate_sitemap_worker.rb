class GenerateSitemapWorker
  include Sidekiq::Worker

  def perform(*args)
    SitemapGenerator::Interpreter.run(config_file: ENV["CONFIG_FILE"])
    SitemapGenerator::Sitemap.ping_search_engines 'https://yazdaapp.com/sitemap.xml' if Rails.env.production?
  end
end
