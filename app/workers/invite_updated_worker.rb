require_relative '../../lib/notification_message_builder'

class InviteUpdatedWorker
  include Sidekiq::Worker

  def perform(invite_id)
    invite = Invite.find(invite_id)
    url    = "yazda://adventures/#{invite.adventure.id}"

    user       = invite.adventure.owner
    preference = user.notification_preference

    if ['both', 'email'].include? preference.adventure_joined
      NotificationMailer.delay.invite_responded_to(invite_id)
    end

    if ['both', 'push'].include? preference.adventure_joined
      message = NotificationMessageBuilder.new.invite_responded_to(invite, custom_uri: url)

      SendPushNotificationWorker.perform_async [user.id], message
    end
  end
end
