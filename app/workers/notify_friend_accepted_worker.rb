require_relative '../../lib/notification_message_builder'

class NotifyFriendAcceptedWorker
  include Sidekiq::Worker

  def perform(friendship_id)
    friendship = Friendship.find(friendship_id)

    preference = friendship.user.notification_preference

    if ['both', 'email'].include? preference.friend
      NotificationMailer.delay.accept_friend_request(friendship_id)
    end

    if ['both', 'push'].include? preference.friend
      message = NotificationMessageBuilder.new.friend_accepted(friendship)

      SendPushNotificationWorker.perform_async [friendship.user_id], message
    end
  end
end
