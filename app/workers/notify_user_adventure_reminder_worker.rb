require_relative '../../lib/notification_message_builder'

class NotifyUserAdventureReminderWorker
  include Sidekiq::Worker

  def perform(adventure_id)
    adventure = Adventure.find(adventure_id)
    user_ids =[]
    url        = "yazda://adventures/#{adventure.id}"
    message  = NotificationMessageBuilder.new.adventure_reminder(adventure, custom_uri: url)

    adventure.invites.ya.find_each do |invite|
      user_id    = invite.user_id
      user       = invite.user
      preference = user.notification_preference

      if ['both', 'email'].include? preference.adventure_reminder
        NotificationMailer.delay.adventure_reminder(user_id, adventure.id)
      end

      if ['both', 'push'].include? preference.adventure_reminder
        user_ids << user_id
      end
    end

    SendPushNotificationWorker.perform_async user_ids, message if user_ids.present?

    adventure.update(alert_sent_at: DateTime.now)
  end
end
