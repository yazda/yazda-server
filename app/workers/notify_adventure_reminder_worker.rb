class NotifyAdventureReminderWorker
  include Sidekiq::Worker

  def perform(*args)
    Adventure.not_notified.find_each do |adventure|
      NotifyUserAdventureReminderWorker.perform_async(adventure.id)
    end
  end
end
