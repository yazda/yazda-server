class MeetupRunnerWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(*args)
    client = MeetupApi.new

    Club.where(external_sync: true).where.not(meetup_id: nil).find_each do |club|
      events = client.events group_urlname: club.meetup_id,
                             status:        'upcoming',
                             page:          5,
                             text_format:   'plain'

      user = club.club_roles.where(role: [2, 3]).first.user

      events['results'].each do |event|
        next if event['rsvp_limit'] ||
          event['fee'] ||
          event['visibility'] != 'public' ||
          !(event['venue'] && event['venue']['address_1']) ||
          Adventure.exists?(meetup_id: event['id'])

        start_time = Time.at event['time']/1000
        end_time   = if event['duration']
                       start_time + event['duration']/1000
                     else
                       start_time + 3.hours
                     end

        Adventure.create!(
          name:           event['name'],
          description:    event['description'],
          start_time:     start_time,
          end_time:       end_time,
          adventure_type: club.meetup_type,
          lat:            event['venue']['lat'],
          lon:            event['venue']['lon'],
          meetup_id:      event['id'],
          owner:          user,
          club:           club
        ) if start_time > DateTime.now
      end
    end
  end
end
