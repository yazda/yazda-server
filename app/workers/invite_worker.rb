require_relative '../../lib/notification_message_builder'

class InviteWorker
  include Sidekiq::Worker

  def perform(invite_id)
    invite     = Invite.find(invite_id)
    adventure  = invite.adventure
    user       = invite.user
    preference = user.notification_preference
    url        = "yazda://adventures/#{adventure.id}"

    if ['both', 'email'].include? preference.adventure_invite
      NotificationMailer.delay.new_adventure(invite.id)
    end

    if ['both', 'push'].include? preference.adventure_invite
      club    = adventure.club
      banner  = club.present? ? club.banner.url : nil
      message = NotificationMessageBuilder
                  .new
                  .adventure_invite(adventure,
                                    invited_by:       invite.invited_by.name,
                                    custom_uri:       url,
                                    appboy_image_url: banner)

      SendPushNotificationWorker.perform_async [user.id], message
    end
  end
end
