require_relative '../../lib/notification_message_builder'

class CancelAdventureWorker
  include Sidekiq::Worker

  def perform(adventure_id)
    adventure = Adventure.find(adventure_id)
    url       = "yazda://adventures/#{adventure.id}"

    message = NotificationMessageBuilder.new.cancel_adventure(adventure,
                                                              custom_uri: url)

    user_ids = []

    adventure.invites.ya.find_each do |invite|
      preference = invite.user.notification_preference

      if ['both', 'email'].include? preference.adventure_cancel
        NotificationMailer.delay.canceled_adventure(adventure_id, invite.user.id)
      end

      if ['both', 'push'].include? preference.adventure_cancel
        user_ids << invite.user_id
      end
    end

    SendPushNotificationWorker.perform_async user_ids, message if user_ids.present?
  end
end
