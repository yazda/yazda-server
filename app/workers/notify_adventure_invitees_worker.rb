require_relative '../../lib/notification_message_builder'

class NotifyAdventureInviteesWorker
  include Sidekiq::Worker

  # TODO make this perform more async
  def perform(adventure_id)
    adventure = Adventure.find(adventure_id)
    url       = "yazda://adventures/#{adventure.id}"
    club      = adventure.club
    banner    = club.present? ? club.banner.url : nil
    message   = NotificationMessageBuilder.new.adventure_invite(adventure,
                                                                custom_uri:       url,
                                                                appboy_image_url: banner)

    return if adventure.canceled?

    invite_ids = adventure.invites.pluck(:id).to_set

    invite_ids.each do |invite_id|
      InviteWorker.perform_async(invite_id)
    end

    if adventure.club && !adventure.link?
      ids   = []
      roles = adventure.club.club_roles

      roles.find_each do |role|
        next if role.user.adventure_type_preferences.exclude?(adventure.adventure_type) ||
          role.user_id == adventure.user_id

        invite = Invite.find_or_create_by(user:       role.user,
                                          adventure:  adventure,
                                          invited_by: adventure.owner)

        if ['both', 'email'].include? role.adventure_invite
          NotificationMailer.delay.new_adventure(invite.id)
        end

        if ['both', 'push'].include? role.adventure_invite
          ids << role.user_id
        end
      end

      SendPushNotificationWorker.perform_async ids, message if ids.present?
    end
  end
end
