require_relative '../../lib/notification_message_builder'

class NotifyFriendRequestWorker
  include Sidekiq::Worker

  def perform(friendship_id)
    friendship = Friendship.find(friendship_id)
    preference = friendship.friend.notification_preference

    if ['both', 'email'].include? preference.friend
      NotificationMailer.delay.new_friend_request(friendship_id)
    end

    if ['both', 'push'].include? preference.friend
      message = NotificationMessageBuilder.new.friend_request(friendship)

      SendPushNotificationWorker.perform_async [friendship.friend_id], message
    end
  end
end
