require_relative '../../lib/notification_message_builder'

class UpdateAdventureWorker
  include Sidekiq::Worker

  sidekiq_options unique: :until_timeout, unique_expiration: 60 * 120

  def perform(adventure_id)
    adventure = Adventure.find(adventure_id)
    url       = "yazda://adventures/#{adventure.id}"
    user_ids  = []
    message   = NotificationMessageBuilder.new.update_adventure(adventure,
                                                                custom_uri: url)

    adventure.invites.ya.find_each do |invite|
      preference = invite.user.notification_preference

      if ['both', 'email'].include? preference.adventure_edit
        NotificationMailer.delay.update_adventure(invite.id)
      end

      if ['both', 'push'].include? preference.adventure_edit
        user_ids << invite.user_id
      end
    end

    SendPushNotificationWorker.perform_async user_ids, message if user_ids.present?
  end
end
