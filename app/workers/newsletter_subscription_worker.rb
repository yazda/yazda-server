class NewsletterSubscriptionWorker
  include Sidekiq::Worker

  def perform(subscribe, user_id)
    user = User.find(user_id)
    name = user.name.split(/\s/)

    if subscribe == :subscribe || subscribe == 'subscribe'
      Gibbon::API.lists.subscribe(
        id:           yazda_list['id'],
        email:        { email: user.email },
        merge_vars:   { FNAME: name.shift, LNAME: name.shift },
        double_optin: false,
        send_welcome: true
      ) unless Rails.env.development?
    elsif subscribe == :unsubscribe || subscribe == 'unsubscribe'
      Gibbon::API.lists.unsubscribe(
        id:    yazda_list['id'],
        email: { email: user.email }
      ) unless Rails.env.development?
    else
      raise ArgumentError
    end
  end

  def yazda_list
    @@yazda_list ||= Gibbon::API
                       .lists
                       .list(filters: {
                               list_name: 'Yazda Newsletter'
                             })['data'][0]
  end
end
