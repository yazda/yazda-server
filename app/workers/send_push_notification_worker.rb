class SendPushNotificationWorker
  include Sidekiq::Worker

  def perform(user_ids, message)
    unless Rails.env.development?
      api = Appboy::API.new(ENV['APPBOY_GROUP_ID'])

      user_ids.in_groups_of(50, false) do |group|
        api.send_messages(messages: message, external_user_ids: group)
      end
    end
  end
end
