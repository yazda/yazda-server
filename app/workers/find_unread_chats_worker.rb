require_relative '../../lib/notification_message_builder'

class FindUnreadChatsWorker
  include Sidekiq::Worker

  sidekiq_options retry: false

  def perform(*args)
    client = Mongo::Client.new(ENV['MONGO_URL'])
    chats  = client[:chats]

    # order by oldest first so that we can send notifications in order
    chats
      .find(notified_at: { '$exists': false })
      .sort(sent_at: 1)
      .each do |msg|

      chat_message = msg['msg'][0..140]
      user         = User.find(msg['user']['id'].to_i)
      name         = nil
      user_ids     = nil
      message      = nil
      ids          = []

      if msg['adventureId']
        adventure = Adventure.find(msg['adventureId'])
        name      = adventure.name
        message   = NotificationMessageBuilder
                      .new
                      .chat(chat_message,
                            name,
                            {
                              custom_uri: "yazda://adventures/#{adventure.id}"
                            })

        user_ids = adventure.invites.ya.pluck :user_id
        user_ids << adventure.user_id

        user_ids = user_ids.reject do |id|
          id == user.id || msg['read'].include?(id)
        end
      else
        name    = user.name
        message = NotificationMessageBuilder.new.chat(chat_message, name)

        sent_to  = msg['sentTo'].reject { |a| a['read'] || a['id'].to_i == user.id }
        user_ids = sent_to.map { |u| u['id'].to_i }
      end

      NotificationPreference.where(user_id: user_ids).find_each do |pref|
        send_email pref, user.id, message, msg['adventureId']

        if ['both', 'push'].include? pref.chat
          ids << pref.user_id
        end
      end

      SendPushNotificationWorker.perform_async ids, message
      chats.update_one({ :_id => msg['_id'] },
                       { :'$set' => { notified_at: DateTime.now } })
    end

    client.close
  end

  private

  def send_email preference, sent_by, message, adventure_id
    if ['both', 'email'].include? preference.chat
      NotificationMailer.delay.new_chat(preference.user_id,
                                        sent_by,
                                        message[:default],
                                        adventure_id)
    end
  end
end
