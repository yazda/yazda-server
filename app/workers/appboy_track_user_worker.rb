class AppboyTrackUserWorker
  include Sidekiq::Worker

  def perform(id)
    unless Rails.env.development?
      user = User.find(id)
      api  = Appboy::API.new(ENV['APPBOY_GROUP_ID'])

      hash = user.as_json

      hash['first_name']    = hash['name'].split(' ').shift
      hash['external_id']   = hash.delete 'id'
      hash['home_city']     = hash.delete 'location'
      hash['bio']           = hash.delete 'description'
      hash['has_facebook']  = user.facebook?
      hash['has_strava']    = user.strava?
      hash['friends_count'] = user.friends.count
      hash['birthday']      = user.birthday
      hash['gender']        = user.gender
      hash['image_url']     = hash['avatar']['url']
      hash['facebook']      = { id: user.providers
                                      .where(provider: 'facebook')
                                      .first
                                      .uid } if user.facebook?

      hash.delete 'avatar'
      hash.delete 'banner'

      api.track_attribute hash
    end
  end
end
