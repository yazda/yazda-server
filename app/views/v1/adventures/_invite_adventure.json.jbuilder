json.id adventure.id
json.adventure_type adventure.adventure_type
json.reservation_limit adventure.reservation_limit
json.name adventure.name
json.start_time_epoch adventure.start_time.to_i
json.start_time adventure.start_time
json.end_time_epoch adventure.end_time.to_i
json.end_time adventure.end_time
json.duration_in_minutes adventure.duration
json.location_id adventure.location_id
json.trail_id adventure.location.trail_id if adventure.location.present?
json.location_name adventure.location ? adventure.location.name : nil
json.address adventure.address
json.lat adventure.lat
json.lon adventure.lon
json.created_at adventure.created_at
json.created_at_epoch adventure.created_at.to_i
json.updated_at adventure.updated_at
json.updated_at_epoch adventure.updated_at.to_i
json.skill_level adventure.skill_level
json.sponsored adventure.is_sponsored?
json.attendings_count adventure.invites.ya.count
json.pendings_count adventure.invites.pending.count
json.rejections_count adventure.invites.na.count
json.club do
  json.partial! 'v1/clubs/club_small', club: adventure.club if adventure.club
end
json.invite do
  invite = adventure.invites.find_by(user: @current_resource_owner.id) if @current_resource_owner
  if invite.present?
    json.partial!('v1/invites/invite', invite: invite)
  else
    json.null!
  end
end
json.owner do
  json.partial! 'v1/users/user_invite', user: adventure.owner
end
