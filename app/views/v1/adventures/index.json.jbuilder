json.partial! 'v1/shared/paging', resource: @adventures
json.adventures do
  json.partial! partial: 'v1/adventures/index_adventure',
                collection: @adventures, as: :adventure
end
