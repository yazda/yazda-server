role = @current_resource_owner && adventure.club.present? ? adventure.club.club_roles.find_by(user: @current_resource_owner) : nil

json.partial! 'v1/adventures/invite_adventure', adventure: adventure
json.description adventure.description
json.html_description adventure.html_description
json.tags adventure.tag_list
json.privacy adventure.privacy
json.can_edit adventure.start_time > DateTime.current &&
                !adventure.canceled && @current_resource_owner &&
                ((adventure.owner.id == @current_resource_owner.id) ||
                  (role.present? && [:admin, :owner, 'admin', 'owner'].include?(role.role)))
json.is_owner @current_resource_owner &&
                (adventure.owner.id == @current_resource_owner.id)
json.is_invited adventure.invited?(@current_resource_owner)
json.is_attending adventure.attending?(@current_resource_owner)
json.has_responded adventure.responded?(@current_resource_owner)
json.canceled adventure.canceled
json.user_ids adventure.user_ids
json.attendings do
  json.partial! partial:    'v1/invites/invite_tiny',
                collection: adventure.invites.ya.includes(:user).limit(10),
                as:         :invite
end
json.pendings do
  json.partial! partial:    'v1/invites/invite_tiny',
                collection: adventure.invites.pending.includes(:user).first(10),
                as:         :invite
end
json.rejections do
  json.partial! partial:    'v1/invites/invite_tiny',
                collection: adventure.invites.na.includes(:user).first(10),
                as:         :invite
end
