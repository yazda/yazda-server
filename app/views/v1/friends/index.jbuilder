json.partial! 'v1/shared/paging', resource: @friends
json.users do
  json.partial! collection: @friends,
                partial:    'v1/users/user_invite',
                as:         :user
end
