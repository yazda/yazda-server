json.partial! 'v1/shared/paging', resource: @users
json.users do
  json.partial! collection: @users, partial: 'v1/users/user_member', as: :user
end
