json.partial! 'v1/shared/paging', resource: @clubs
json.clubs do
  json.partial! partial: 'v1/clubs/club_small', collection: @clubs, as: :club
end
