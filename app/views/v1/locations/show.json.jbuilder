json.location do
  json.id @location.id
  json.name @location.name
  json.lat @location.lat
  json.lon @location.lon
  json.address @location.address
end
