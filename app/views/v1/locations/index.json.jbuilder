json.partial! 'v1/shared/paging', resource: @locations
json.locations @locations do |location|
  json.id location.id
  json.name location.name
  json.lat location.lat
  json.lon location.lon
  json.address location.address
end
