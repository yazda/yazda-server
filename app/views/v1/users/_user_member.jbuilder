club_role = @club.club_roles.find_by(user: user)

json.partial! 'v1/users/user_invite', user: user
json.role club_role.role
