json.id user.id
json.name user.name
json.profile_image_url "#{@hostname}/v1/users/#{user.id}/avatar"
json.profile_image_thumb_url "#{@hostname}/v1/users/#{user.id}/avatar?size=thumb"
json.banner_image_url "#{@hostname}/v1/users/#{user.id}/banner"
json.banner_image_thumb_url "#{@hostname}/v1/users/#{user.id}/banner?size=thumb"
json.is_adventure_leader user.adventure_leader?
