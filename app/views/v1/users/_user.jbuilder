json.id user.id
json.name user.name
json.location user.location
json.profile_image_url "#{@hostname}/v1/users/#{user.id}/avatar"
json.profile_image_thumb_url "#{@hostname}/v1/users/#{user.id}/avatar?size=thumb"
json.banner_image_url "#{@hostname}/v1/users/#{user.id}/banner"
json.banner_image_thumb_url "#{@hostname}/v1/users/#{user.id}/banner?size=thumb"
json.description user.description
json.html_description user.html_description
json.tags user.tag_list
json.sign_in_count user.sign_in_count
json.adventures_count user.adventures.count
json.friends_count user.friends.count
json.created_at user.created_at
json.created_at_epoch user.created_at.to_i
json.updated_at user.updated_at
json.updated_at_epoch user.updated_at.to_i
# TODO friendship status
json.following @current_resource_owner &&
                 (Friendship.approved
                    .exists?(user_id:   @current_resource_owner.id,
                             friend_id: user.id) ||
                   @current_resource_owner.id == user.id)
json.request_sent @current_resource_owner &&
                    (Friendship
                       .where
                       .not(status: Friendship.statuses[:rejected])
                       .exists?(user_id: @current_resource_owner.id, friend_id: user.id) ||
                      @current_resource_owner.id == user.id)
json.is_adventure_leader user.adventure_leader?
json.clubs do
  json.partial! partial:    'v1/clubs/club_small',
                collection: user.clubs,
                as:         :club
end
