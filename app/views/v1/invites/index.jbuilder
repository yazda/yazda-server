json.partial! 'v1/shared/paging', resource: @invites
json.invites do
  json.partial! 'v1/invites/invite_with_event', collection: @invites, as: :invite
end
