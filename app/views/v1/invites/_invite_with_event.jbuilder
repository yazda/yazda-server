json.partial! 'v1/invites/invite', invite: invite
json.adventure do
  json.partial! 'v1/adventures/invite_adventure', adventure: invite.adventure
end
json.invited_by do
  json.partial! 'v1/users/user_tiny', user: invite.invited_by
end
