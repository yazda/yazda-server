json.invite do
  json.partial! 'v1/invites/invite_with_event', invite: @invite
end
