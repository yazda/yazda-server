json.partial! 'v1/shared/paging', resource: @followers
json.users do
  json.partial! collection: @followers,
                partial:    'v1/users/user_invite',
                as:         :user
end
