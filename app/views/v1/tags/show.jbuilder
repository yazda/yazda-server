json.tag do
  json.name @tag.name
  json.count @tag.taggings_count
  json.adventures @adventures do |adventure|
    json.partial! adventure
  end

  json.clubs @clubs do |club|
    json.partial! 'v1/clubs/club', club: club
  end

  json.users @users do |user|
    json.partial! 'v1/users/user', user: user
  end
end
