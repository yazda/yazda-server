json.partial! 'v1/shared/paging', resource: @users
json.users @users do |user|
  json.id user.id
  json.name user.name
  json.email user.email
  json.profile_image_url "#{@hostname}/v1/users/#{user.id}/avatar"
  json.profile_image_thumb_url "#{@hostname}/v1/users/#{user.id}/avatar?size=thumb"
  json.banner_image_url "#{@hostname}/v1/users/#{user.id}/banner"
  json.banner_image_thumb_url "#{@hostname}/v1/users/#{user.id}/banner?size=thumb"
end
