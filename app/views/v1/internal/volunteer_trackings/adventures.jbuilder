json.partial! 'v1/shared/paging', resource: @adventures
json.adventures @adventures do |adventure|
  json.id adventure.id
  json.name adventure.name
  json.start_time adventure.start_time
  json.end_time adventure.end_time
  json.address adventure.address
  json.adventure_type adventure.adventure_type
  json.description adventure.description
  json.privacy adventure.privacy
  json.owner do
    json.id adventure.owner.id
    json.name adventure.owner.name
    json.email adventure.owner.email
    json.profile_image_url "#{@hostname}/v1/users/#{adventure.owner.id}/avatar"
    json.banner_image_url "#{@hostname}/v1/users/#{adventure.owner.id}/banner"
  end
  json.attendings adventure.invites.ya.pluck(:user_id)
  json.club do
    json.id adventure.club.id
    json.banner_image_url "#{@hostname}/v1/clubs/#{adventure.club.id}/banner"
    json.profile_image_url "#{@hostname}/v1/clubs/#{adventure.club.id}/avatar"
  end
end
