json.friendship do
  json.partial! 'v1/friendships/friendship', friendship: @friendship
end
