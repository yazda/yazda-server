json.partial! 'v1/shared/paging', resource: @friendships
json.friendships do
  json.partial! partial: 'v1/friendships/friendship', collection: @friendships, as: :friendship
end
