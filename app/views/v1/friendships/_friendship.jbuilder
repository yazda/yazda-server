json.id friendship.id
json.status friendship.status
json.user do
  json.partial! 'v1/users/user_invite', user: friendship.user
end
json.friend do
  json.partial! 'v1/users/user_invite', user: friendship.friend
end
json.created_at friendship.created_at
json.created_at_epoch friendship.created_at.to_i
json.updated_at friendship.updated_at
json.updated_at_epoch friendship.updated_at.to_i
json.accepted_at friendship.accepted_at
json.accepted_at_epoch friendship.accepted_at.to_i
json.rejected_at friendship.rejected_at
json.rejected_at_epoch friendship.rejected_at.to_i
