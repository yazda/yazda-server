json.user do
  json.partial! 'v1/users/user', user: @user
  json.partial! 'v1/account/account', user: @user
end
