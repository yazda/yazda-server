json.page params[:page].blank? ? 1 : params[:page].to_i
json.total (resource.try(:total_count) || resource.count)
json.page_count (((resource.try(:total_count) || resource.count) /
  (if params[:page_size].blank? || params[:page_size].to_i == 0
        25
   else
     params[:page_size].to_i
   end)) + 1)
