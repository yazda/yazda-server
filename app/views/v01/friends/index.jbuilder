json.partial! 'v01/shared/paging', resource: @friends
json.users do
  json.partial! collection: @friends,
                partial:    'v01/users/user_invite',
                as:         :user
end
