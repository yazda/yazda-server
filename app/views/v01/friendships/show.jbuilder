json.friendship do
  json.partial! 'v01/friendships/friendship', friendship: @friendship
end
