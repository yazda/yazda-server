json.partial! 'v01/shared/paging', resource: @friendships
json.friendships do
  json.partial! partial: 'v01/friendships/friendship', collection: @friendships, as: :friendship
end
