json.partial! 'v01/shared/paging', resource: @followers
json.users do
  json.partial! collection: @followers,
                partial:    'v01/users/user_invite',
                as:         :user
end
