json.partial! 'v01/shared/paging', resource: @users
json.users do
  json.partial! collection: @users, partial: 'v01/users/user_member', as: :user
end
