club_role = @club_role || (@current_resource_owner ? club
                                                       .club_roles
                                                       .find_by_user_id(@current_resource_owner.id) : nil)

json.id club.id
json.name club.name
json.description club.description
json.profile_image_url "#{@hostname}/v0.1/clubs/#{club.id}/avatar"
json.profile_image_thumb_url "#{@hostname}/v0.1/clubs/#{club.id}/avatar?size=thumb"
json.banner_image_url "#{@hostname}/v0.1/clubs/#{club.id}/banner"
json.banner_image_thumb_url "#{@hostname}/v0.1/clubs/#{club.id}/banner?size=thumb"
json.website club.website
json.contact_email club.contact_email
json.waiver_url club.waiver.url
json.location club.location
json.lat club.lat
json.lon club.lon
json.created_at club.created_at
json.updated_at club.updated_at
json.member_count club.club_roles.count
json.adventures_count club.adventures_count(@current_resource_owner)
json.is_member !!club_role
json.role club_role ? club_role.role : ''
json.adventure_invite club_role ? club_role.adventure_invite : ''
json.chat club_role ? club_role.chat : ''
