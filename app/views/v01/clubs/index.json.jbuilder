json.partial! 'v01/shared/paging', resource: @clubs
json.clubs do
  json.partial! partial: 'v01/clubs/club_small', collection: @clubs, as: :club
end
