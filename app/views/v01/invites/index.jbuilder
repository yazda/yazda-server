json.partial! 'v01/shared/paging', resource: @invites
json.invites do
  json.partial! 'v01/invites/invite_with_event', collection: @invites, as: :invite
end
