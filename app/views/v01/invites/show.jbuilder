json.invite do
  json.partial! 'v01/invites/invite_with_event', invite: @invite
end
