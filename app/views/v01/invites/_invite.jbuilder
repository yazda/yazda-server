json.id invite.id
json.attending invite.attending
json.accepted_at invite.accepted_at
json.accepted_at_epoch invite.accepted_at.to_i
json.rejected_at invite.rejected_at
json.rejected_at_epoch invite.rejected_at.to_i
json.created_at invite.created_at
json.created_at_epoch invite.created_at.to_i
json.user do
  json.partial! 'v01/users/user_invite', user: invite.user
end
