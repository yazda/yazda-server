json.partial! 'v01/invites/invite', invite: invite
json.adventure do
  json.partial! 'v01/adventures/invite_adventure', adventure: invite.adventure
end
