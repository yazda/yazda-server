json.partial! 'v01/shared/paging', resource: @adventures
json.adventures do
  json.partial! partial: 'v01/adventures/adventure', collection: @adventures, as: :adventure
end
