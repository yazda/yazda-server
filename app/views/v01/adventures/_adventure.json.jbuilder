json.partial! 'v01/adventures/invite_adventure', adventure: adventure
json.club do
  json.partial! 'v01/clubs/club_small', club: adventure.club if adventure.club
end
json.attendings do
  json.partial! partial: 'v01/invites/invite', collection: adventure.invites.ya, as: :invite
end
json.pendings do
  json.partial! partial: 'v01/invites/invite', collection: adventure.invites.pending, as: :invite
end
json.rejections do
  json.partial! partial: 'v01/invites/invite', collection: adventure.invites.na, as: :invite
end
