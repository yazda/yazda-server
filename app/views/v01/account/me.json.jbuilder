json.user do
  json.partial! 'v01/users/user', user: @user
  json.partial! 'v01/account/account', user: @user
end
