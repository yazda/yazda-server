json.id user.id
json.name user.name
json.location user.location
json.adventures_count user.adventures.count
json.following_count user.friends.count
json.profile_image_url "#{@hostname}/v0.1/users/#{user.id}/avatar"
json.profile_image_thumb_url "#{@hostname}/v0.1/users/#{user.id}/avatar?size=thumb"
json.banner_image_url "#{@hostname}/v0.1/users/#{user.id}/banner"
json.banner_image_thumb_url "#{@hostname}/v0.1/users/#{user.id}/banner?size=thumb"
json.is_adventure_leader user.adventure_leader?
json.following @current_resource_owner && (Friendship
                                             .approved
                                             .exists?(user_id:   @current_resource_owner.id,
                                                      friend_id: user.id) ||
  @current_resource_owner.id == user.id)
json.request_sent @current_resource_owner &&
                    (Friendship
                       .where
                       .not(status: Friendship.statuses[:rejected])
                       .exists?(user_id: @current_resource_owner.id, friend_id: user.id) ||
                      @current_resource_owner.id == user.id)
