club_role = @club.club_roles.find_by(user: user)

json.partial! 'v01/users/user_invite', user: user
json.role club_role.role
