class NotificationMailer < ApplicationMailer
  layout 'mailer', except: :follow_up

  def follow_up(user_id)
    @user = User.find(user_id)

    mail(to:      "#{@user.name} <#{@user.email}>",
         from:    'James Stoup <james.stoup@yazdaapp.com>',
         subject: 'Thanks for signing up to Yazda')
  end

  def new_adventure(invite_id)
    @invite    = Invite.find(invite_id)
    @user      = @invite.user
    @adventure = @invite.adventure
    @club      = @invite.adventure.club
    set_timezone(@adventure)

    mail(to:      "#{@user.name} <#{@user.email}>",
         subject: "Adventure Invite - #{@adventure.name}")
  end

  def invite_responded_to(invite_id)
    @invite    = Invite.find(invite_id)
    @adventure = @invite.adventure

    mail(to:      "#{@adventure.owner.name} <#{@adventure.owner.email}>",
         subject: "Invite Responded - #{@invite.user.name}")
  end

  def update_adventure(invite_id)
    @invite    = Invite.find(invite_id)
    @adventure = @invite.adventure
    set_timezone(@adventure)

    mail(to:      "#{@invite.user.name} <#{@invite.user.email}>",
         subject: "Adventure Update - #{@adventure.name}")
  end

  def canceled_adventure(adventure_id, user_id)
    @adventure = Adventure.find(adventure_id)
    @user      = User.find(user_id)
    set_timezone(@adventure)

    mail(to:      "#{@user.name} <#{@user.email}>",
         subject: "Adventure Canceled - #{@adventure.name}")
  end

  def new_friend_request(friendship_id)
    @friendship = Friendship.find(friendship_id)

    mail(to:      "#{@friendship.friend.name} <#{@friendship.friend.email}>",
         subject: 'Yazda Friend Request')
  end

  def accept_friend_request(friendship_id)
    @friendship = Friendship.find(friendship_id)

    mail(to:      "#{@friendship.user.name} <#{@friendship.user.email}>",
         subject: 'Yazda Friend Request Accepted')
  end

  def adventure_reminder(user_id, adventure_id)
    @user      = User.find(user_id)
    @adventure = Adventure.find(adventure_id)
    set_timezone(@adventure)

    mail(to:      "#{@user.name} <#{@user.email}>",
         subject: "Adventure Reminder - #{@adventure.name}")
  end

  def new_chat(user_id, sent_by, message, adventure_id = nil)
    @user      = User.find(user_id)
    @sent_by   = User.find(sent_by)
    @message   = message
    @adventure = Adventure.find adventure_id if adventure_id.present?

    mail(to:      @user.email,
         subject: "#{@sent_by.name} sent you a message!")
  end

  def first_accepted_invite(adventure_id, user_id)
    @adventure = Adventure.first adventure_id
    @user      = User.find user_id

    mail(to: @user.email, subject: 'You have an eye for adventure!')
  end

  private

  def set_timezone(adventure)
    begin
      @timezone = Timezone.lookup(adventure.lat, adventure.lon)
    rescue Timezone::Error::InvalidZone
      @timezone = Timezone.fetch('America/Denver')
    end
  end
end
