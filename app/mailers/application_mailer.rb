class ApplicationMailer < ActionMailer::Base
  default from: "Yazda <no-reply@yazdaapp.com>"
  layout 'mailer'
end
