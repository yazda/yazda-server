class DeviseMailer < Devise::Mailer
  helper :application
  include Devise::Controllers::UrlHelpers
  layout 'mailer'
  default from: "Yazda <no-reply@yazdaapp.com>"

end
