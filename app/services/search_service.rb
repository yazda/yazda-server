class SearchService
  attr_reader :user_id, :user

  def initialize(user)
    if user.is_a? User
      @user    = user
      @user_id = user.id
    elsif user.is_a? Numeric
      @user_id = user
      @user    = User.find(user)
    else
      raise ArgumentError.new('Must be Numeric or User')
    end
  end

  def find_user(search_term = '',
                pref = Adventure.adventure_types.keys.join(','),
                page_size = Kaminari.config.default_per_page,
                page = 0)

    page_size = Kaminari.config.default_per_page unless page_size.is_a? Numeric

    query = <<-SQL
      (select users.*, sum(weight) from (
        #{find_friends_not_following search_term}

        union all

        #{find_friends_from_friends search_term}

        union all

        #{find_friends_from_adventures search_term}

        union all

        #{find_by_search(search_term, pref)}
      ) as t
      join users on users.id = t.id
      group by users.id
    SQL

    if search_term.blank?
      query << 'having SUM(weight) >= 45 order by SUM(weight) desc'
    else
      query << "order by similarity(name,
                #{ActiveRecord::Base.connection.quote(query)}) desc,
                SUM(weight) desc"
    end
# TODO return total count

    offset = page_size * ((page = page.to_i - 1) < 0 ? 0 : page)
    query << " LIMIT #{page_size} OFFSET #{offset})"

# TODO sanitize
    User.find_by_sql(query)
  end

  private

  def find_friends_not_following search_term
    query = <<-SQL
      select friendships.user_id as id, 4 as weight
        from friendships
        join users on friendships.user_id = users.id
        where friendships.friend_id = #{user_id}
          and status = 1
          and user_id not in (
            select friend_id
              from friendships
              where user_id = #{user_id}
                and status != 2
          )
    SQL

    unless search_term.blank?
      query << "and users.name <-> #{ActiveRecord::Base.connection.quote search_term} < 0.85"
    end

    query
  end

  def find_friends_from_friends search_term
    query = <<-SQL
      select friendships.friend_id as id, 2 as weight
        from friendships
        join users on friendships.friend_id = users.id
        where friendships.user_id IN (
          select friend_id
          from friendships
          where user_id = #{user_id} and status = 1)
        and friendships.friend_id NOT IN (
          select friend_id
          from friendships
          where user_id = #{user_id} and status != 2)
        and not friend_id = #{user_id}
    SQL

    unless search_term.blank?
      query << "and users.name <-> #{ActiveRecord::Base.connection.quote search_term} < 0.85"
    end

    query
  end

  def find_friends_from_adventures search_term
    query = <<-SQL
      select in2.user_id as id, 5 as weight
        from invites as in1
        join invites as in2 on in1.adventure_id = in2.adventure_id
        join users on in2.user_id = users.id
        where in1.attending = 0
        and in2.attending = 0
        and in2.user_id != #{user_id}
        and in2.user_id NOT IN (
          select friend_id
          from friendships
          where user_id = #{user_id} and status != 2)
    SQL

    unless search_term.blank?
      query << "and users.name <-> #{ActiveRecord::Base.connection.quote search_term} < 0.85"
    end

    query
  end

  def find_by_search(query, pref)
    sql = ''

    if query.blank?
      sql = User.select('id, 1 as weight')
    else
      sql = User.search(query)
    end

    if pref.present?
      query = "adventure_type_preferences && ? OR adventure_type_preferences IS NULL OR adventure_type_preferences = '{}'"
      sql   = sql.where(query, "{#{pref}}")
    end

    sql.to_sql
  end
end
