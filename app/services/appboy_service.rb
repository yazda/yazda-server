class AppboyService
  attr_reader :api, :user

  def initialize(user)
    @api = Appboy::API.new(ENV['APPBOY_GROUP_ID'])

    if user.is_a? User
      @user    = user
      @user_id = user.id
    elsif user.is_a? Numeric
      @user_id = user
      @user    = User.find(user)
    else
      raise ArgumentError.new('Must be Numeric or User')
    end
  end

  def track_event(event, model=[])
    return unless Rails.env.production?

    properties = model.present? ? model.attributes : []

    api.track_event(external_id: user.id,
                    time:        Time.now,
                    name:        event,
                    properties:  properties)
  end
end
