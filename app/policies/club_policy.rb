class ClubPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def update_profile_image?
    club_admin_or_owner?
  end

  alias update? update_profile_image?
  alias update_banner_image? update_profile_image?
  alias waiver? update_profile_image?
  alias update_member? update_profile_image?

  private

  def club_admin_or_owner?
    role = record.club_roles.find_by(user_id: user.id)

    role.present? && (role.admin? || role.owner?)
  end
end
