class AdventurePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    !record.private_adventure? ||
      (user.present? && (owned_adventure || club_admin || invited_adventure))
  end

  def update?
    record.start_time > DateTime.current && user.present? &&
      (owned_adventure || club_admin)
  end

  alias destroy? update?
  alias pending? show?
  alias attending? show?
  alias rejected? show?

  private

  def owned_adventure
    record.owner == user
  end

  def invited_adventure
    record.invites.exists?(user_id: user.id)
  end

  def club_admin
    return false unless record.club.present?

    role = record.club.club_roles.find_by(user_id: user.id)

    role.present? && (role.admin? || role.owner?)
  end
end
