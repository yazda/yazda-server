class WidgetsController < ApplicationController
  layout 'widget'

  before_action :authenticate_app_id

  def show
    club_id     = params[:club_id]
    lat         = params[:lat].to_f
    lon         = params[:lon].to_f
    distance    = params[:distance].to_i
    @theme      = %w(light dark).include?(params[:theme]) ? params[:theme] : "dark"
    @background = params[:background] if params[:background] =~ /\A#?(?:[A-Fa-f0-9]{3}){1,2}\z/i

    if club_id.present?
      @club       = Club.find(params[:club_id])
      @adventures = Adventure.public_adventure.where(club_id: club_id)
    elsif lat.present? && lon.present? && distance.present?
      @adventures = Adventure.public_adventure.near([lat, lon], distance)
    else
      # Implement error system
    end

    @adventures = @adventures
                    .canceled
                    .by_status
                    .order(start_time: :asc)

    @adventures = @adventures.page(params[:page]).per(page_size)

    # Allow iframes to work for this action
    response.headers.except! 'X-Frame-Options'
  end

  private

  def authenticate_app_id
    Doorkeeper::Application.find_by! uid: params[:app_id]
  end

  def page_size
    params[:page_size] || 12
  end
end
