class ApplicationController < ActionController::Base
  # Since we're an API null session it up
  protect_from_forgery with: :exception, unless: -> c { c.request.format == 'application/json' }
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  force_ssl if: :production?

  before_action :hostname

  def self.add_auth_params(api)
    api.param :header, 'Authentication-Token', :string, :required, 'Authentication token'
  end

  protected

  def production?
    Rails.env.production? && !(action_name == 'health_check' || controller_name == 'main' ||
      controller_name == 'doc')
  end

  def hostname
    @hostname = request.base_url
  end
end
