class V1::Internal::VolunteerTrackingsController < V1::BaseController
  before_action :require_current_resource_owner
  before_action :reject_if_not_official

  def adventures
    # TODO better authorization
    head :unauthorized unless current_resource_owner
                                .clubs
                                .exists?(id: params[:club_id])

    @adventures = Adventure
                    .includes(:owner, :club)
                    .canceled
                    .where(club_id: params[:club_id])
                    .by_status(:past)
                    .order(end_time: :desc)
    .page(params[:page])
  end

  def users
    @users = User.where(id: user_id_params[:ids].split(','))
  end

  private

  def reject_if_not_official
    head :unauthorized unless doorkeeper_token.application.official?
  end

  def user_id_params
    params.permit(:ids)
  end
end
