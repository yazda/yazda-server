class V1::LocationsController < V1::BaseController
  before_action :require_current_resource_owner

  def index
    q = params[:q]

    @locations = Location.where('name <-> ? < 0.85', q)
  end

  def create
    @location = Location.new(resource_params)

    @location.save!

    AppboyService.new(current_resource_owner)
      .track_event('create-location', @location)

    render :show, status: :created
  end

  private

  def location_params
    params.permit(:name, :lat, :lon, :address, :trail_id)
  end
end
