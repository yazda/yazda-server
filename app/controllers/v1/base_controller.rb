class V1::BaseController < ActionController::Metal
  include AbstractController::Rendering
  include ActionView::Rendering
  # include ActionController::HideActions

  # include ActionController::UrlFor
  include ActionController::Redirecting
  include ActionController::Rendering
  include ActionController::Renderers::All
  include ActionController::Head
  # include ActionController::ConditionalGet
  include ActionController::RackDelegation

  include ActionController::ForceSSL
  # include ActionController::DataStreaming

  # Before callbacks should also be executed the earliest as possible, so
  # also include them at the bottom.
  include AbstractController::Callbacks
  include Devise::Controllers::Helpers
  include Doorkeeper::Rails::Helpers

  include ActionController::Caching
  include ActionController::MimeResponds
  include ActionController::RequestForgeryProtection
  include ActionController::ImplicitRender
  include ActionController::StrongParameters
  include ActionController::RespondWith

  # Append rescue at the bottom to wrap as much as possible.
  include ActionController::Rescue

  # Params wrapper should come before instrumentation so they are
  # properly showed in logs
  include ActionController::ParamsWrapper

  # Add instrumentations hooks at the bottom, to ensure they instrument
  # all the methods properly.
  include ActionController::Instrumentation
  include Bugsnag::Rails::ControllerMethods
  include Pundit
  include Rails.application.routes.url_helpers


  append_view_path "#{Rails.root}/app/views"

  # Since we're an API null session it up
  protect_from_forgery with: :exception, unless: -> c { c.request.format == 'application/json' }
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  force_ssl if: :production?

  before_action :hostname
  before_action :doorkeeper_authorize!
  before_action :current_resource_owner
  before_bugsnag_notify :add_user_info_to_bugsnag
  before_action :set_resource, only: [:destroy, :show, :update]
  # respond_to :json

  rescue_from ActiveRecord::RecordInvalid do
    render json:     {
      full_errors: get_resource.errors.full_messages,
      errors:      get_resource.errors.inject([]) do |ret, hash|
        ret << { source: { pointer: "data/attributes/#{hash[0]}" },
                 detail: hash[1] }
      end }, status: :unprocessable_entity
  end

  # TODO clean up all of the views. There is a lot of data we're passing that
  # we propbably don't need to
  rescue_from ActiveRecord::RecordNotFound do
    head :not_found
  end

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def current_resource_owner
    @current_resource_owner ||= if doorkeeper_token && doorkeeper_token.resource_owner_id
                                  User.find_by_id(doorkeeper_token.resource_owner_id)
                                end
  end

  protected

  def require_current_resource_owner
    head :unauthorized unless current_resource_owner
  end

  def self.add_auth_params(api)
    api.param :header, 'Authentication-Token', :string, :required, 'Authentication token'
  end

  protected

  def add_user_info_to_bugsnag(notification)
    notification.user = { id: current_resource_owner.id } if current_resource_owner
  end

  def production?
    Rails.env.production? && !(action_name == 'health_check' || controller_name == 'main' ||
      controller_name == 'doc')
  end

  def hostname
    @hostname = request.base_url
  end

  def pundit_user
    current_resource_owner
  end

  private

  # Returns the resource from the created instance variable
  # @return [Object]
  def get_resource
    instance_variable_get("@#{resource_name}")
  end

  # Returns the allowed parameters for searching
  # Override this method in each API controller
  # to permit additional parameters to search on
  # @return [Hash]
  def query_params
    {}
  end

  # Returns the allowed parameters for pagination
  # @return [Hash]
  def page_params
    params.permit(:page, :page_size)
  end

  # The resource class based on the controller
  # @return [Class]
  def resource_class
    @resource_class ||= resource_name.classify.constantize
  end

  # The singular name for the resource class based on the controller
  # @return [String]
  def resource_name
    @resource_name ||= self.controller_name.singularize
  end

  # Only allow a trusted parameter "white list" through.
  # If a single resource is loaded for #create or #update,
  # then the controller for the resource must implement
  # the method "#{resource_name}_params" to limit permitted
  # parameters for the individual model.
  def resource_params
    @resource_params ||= self.send("#{resource_name}_params")
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_resource(resource = nil)
    if resource || params[:id]
      resource ||= resource_class.find(params[:id])
      instance_variable_set("@#{resource_name}", resource)
    end
  end

  def page_size
    page_params[:page_size]
  end

  def reject_if_current_resource_owner
    head :forbidden if current_resource_owner
  end

  def user_not_authorized
    head :forbidden
  end
end
