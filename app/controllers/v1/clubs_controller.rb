class V1::ClubsController < V1::BaseController
  skip_before_action :doorkeeper_authorize!,
                     only: [:index, :show, :avatar, :banner, :members]
  before_action :require_current_resource_owner,
                except: [:show, :index, :avatar, :banner, :members]

  def index
    user_id = search_params[:user_id]
    @clubs  = Club.search(search_params[:q])
    @clubs  = @clubs
                .joins(:club_roles)
                .where(club_roles: { user_id: user_id }) if user_id.present?

    if search_params[:lat] && search_params[:lon]
      @clubs = @clubs
                 .near([search_params[:lat].to_f, search_params[:lon].to_f],
                       search_params[:distance] || 100)
    end

    @clubs = @clubs.tagged_with(params[:tag]) if params[:tag].present?
    @clubs = @clubs
               .order(:adventures_count)
               .page(page_params[:page])
               .per(page_size)
  end

  def members
    q = params[:q]

    @club  = Club.find(params[:id])
    @users = @club.users.includes(:club_roles)

    @users = @users.where.not(id: current_resource_owner.id) if current_resource_owner
    @users = @users.where('name <-> ? < 0.75', q) if q.present?
    @users = @users
               .ordered
               .page(page_params[:page])
               .per(page_size)
  end

  def show
    @club_role = ClubRole.where(user_id: current_resource_owner.id,
                                club_id: get_resource.id).first if current_resource_owner
  end

  def join
    set_resource
    @club_role = current_resource_owner
                   .club_roles
                   .find_or_create_by!(club: get_resource)

    AppboyService.new(current_resource_owner).track_event('join-club',
                                                          @club_role.club)

    render :show
  end

  def leave
    set_resource
    club_role = current_resource_owner.club_roles
                  .find_by!(club_id: get_resource.id)

    if club_role.admin? || club_role.owner?
      head :unprocessable_entity
    else
      club_role.destroy

      AppboyService.new(current_resource_owner).track_event('leave-club', club_role.club)

      render :show
    end
  end

  def create
    club_role = current_resource_owner.club_roles.build(role: :owner)
    @club     = club_role.build_club(club_params)

    club_role.save!

    AppboyService.new(current_resource_owner).track_event('create-club', @club)

    render :show, status: :created
  end

  def update_profile_image
    authorize set_resource

    get_resource.process_avatar_upload = true
    get_resource.update!(update_profile_image_params)

    AppboyService.new(current_resource_owner)
      .track_event('update-profile-image-club', get_resource)

    render :show
  end

  def update_banner_image
    authorize set_resource

    get_resource.process_banner_upload = true
    get_resource.update!(update_banner_image_params)


    AppboyService.new(current_resource_owner)
      .track_event('update-banner-image-club', get_resource)

    render :show
  end

  def update_notification
    set_resource
    @club_role = get_resource.club_roles.find_by! user: current_resource_owner
    @club_role.update!(update_notification_params)

    render :show
  end

  def waiver
    authorize set_resource

    get_resource.process_waiver_upload = true
    get_resource.update!(waiver_params)


    AppboyService.new(current_resource_owner)
      .track_event('upload-waiver-club', get_resource)

    render :show
  end

  def update
    authorize set_resource

    get_resource.update!(update_club_params)

    AppboyService.new(current_resource_owner).track_event('update-club', get_resource)

    render :show
  end

  def update_member
    authorize set_resource

    user      = User.find(params[:user_id])
    club_role = ClubRole.find_by!(user: user, club: get_resource)

    club_role.update! role: params[:role] unless club_role.owner?

    head :ok
  end

  def avatar
    @club = Club.find(params[:id])
    size  = ['thumb'].delete params[:size]

    redirect_to URI.parse(@club.avatar.url(size)).to_s
  end

  def banner
    @club = Club.find(params[:id])
    size  = ['thumb'].delete params[:size]

    if @club.banner.present?
      redirect_to URI.parse(@club.banner.url(size)).to_s
    else
      head :ok
    end
  end

  # def destroy
  #   set_resource(current_resource_owner.adventures.find(params[:id]))
  #   get_resource.cancel!
  #
  #   CancelAdventureWorker.perform_async get_resource.id
  #
  #   head :no_content
  # end

  private

  def update_banner_image_params
    params.permit(:banner)
  end

  def update_notification_params
    params.permit(:adventure_invite, :chat)
  end

  def update_profile_image_params
    params.permit(:avatar)
  end

  def waiver_params
    params.permit(:waiver, :remote_waiver_url)
  end

  def club_params
    params.permit(:name, :description, :website, :contact_email, :location,
                  :waiver, :remote_waiver_url)
  end

  def update_club_params
    params.permit(:name, :description, :website, :contact_email, :location,
                  :waiver, :remote_waiver_url)
  end

  def search_params
    params.permit(:lat, :lon, :distance, :q, :user_id)
  end
end
