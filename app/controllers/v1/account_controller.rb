class V1::AccountController < V1::BaseController
  before_action :require_current_resource_owner

  def get_settings
    set_resource(current_resource_owner)

    render :me
  end

  def post_settings
    set_resource(current_resource_owner)
    get_resource.update!(settings_params)

    render :me
  end

  def update_profile_image
    set_resource(current_resource_owner)

    case update_profile_image_params[:avatar]
    when 'facebook'
      provider = current_resource_owner.providers
                   .where(provider: 'facebook').first
      image    = "https://graph.facebook.com/#{provider.uid}/picture?type=large"

      get_resource.process_avatar_upload = true
      get_resource.remote_avatar_url = image
      get_resource.save!
    when 'strava'
      provider = current_resource_owner.providers
                   .where(provider: 'strava').first
      client   = Strava::Api::V3::Client.new(access_token: provider.access_token)
      athlete  = client.retrieve_current_athlete

      get_resource.process_avatar_upload = true
      get_resource.remote_avatar_url = athlete["profile"]
      get_resource.save!
    else
      get_resource.process_avatar_upload = true
      get_resource.update!(update_profile_image_params)
    end

    @user = current_resource_owner.reload

    render :me
  end

  def update_banner_image
    set_resource(current_resource_owner)

    get_resource.process_banner_upload = true
    get_resource.update!(update_banner_image_params)

    render :me
  end

  def me
    set_resource(current_resource_owner)

    render :me
  end

  def resource_name
    'user'
  end

  private

  def update_banner_image_params
    params.permit(:banner)
  end

  def update_profile_image_params
    params.permit(:avatar)
  end

  def settings_params
    params.permit(:phone, :newsletter, :views_mountain_biking, :views_hiking,
                  :views_biking, :views_running, :views_trail_running,
                  :notifies_invite, :notifies_friend_request, :name,
                  :location, :description, :adventure_invite,
                  :adventure_edit, :adventure_cancel, :adventure_reminder,
                  :adventure_joined, :chat, :friend, :gender, :birthday)
  end
end
