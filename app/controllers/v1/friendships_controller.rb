class V1::FriendshipsController < V1::BaseController
  before_action :require_current_resource_owner

  skip_before_action :set_resource

  def incoming
    @friendships = current_resource_owner
                     .incoming_friendships
                     .page(page_params[:page])
                     .per(page_size)
                     .includes(:friend, :user)

    render :index
  end

  def outgoing
    @friendships = current_resource_owner
                     .requested_friendships
                     .page(page_params[:page])
                     .per(page_size)
                     .includes(:friend, :user)

    render :index
  end

  def create
    @friendship = Friendship.request_friend current_resource_owner,
                                            User.find(params[:user_id])

    AppboyService.new(current_resource_owner)
      .track_event('add-friend', @friendship)

    render :show, status: :created
  end

  def accept
    @friendship = current_resource_owner
                    .incoming_friendships
                    .find(params[:id])

    @friendship.accept!
    @friendship.reload

    AppboyService.new(current_resource_owner)
      .track_event('accept-friend', @friendship)

    render :show
  end

  def reject
    @friendship = current_resource_owner
                    .incoming_friendships
                    .find(params[:id])

    @friendship.reject!
    @friendship.reload

    AppboyService.new(current_resource_owner)
      .track_event('reject-friend', @friendship)


    render :show
  end
end
