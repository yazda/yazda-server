class V1::TagsController < V1::BaseController
  skip_before_action :doorkeeper_authorize!
  skip_before_action :set_resource

  # TODO order by trending
  def index
    @tags = ActsAsTaggableOn::Tag.named_like(params[:q] || '').most_used
  end

  # TODO order by trending
  def show
    @tag = ActsAsTaggableOn::Tag.named(params[:id]).first

    @adventures = adventures
    @clubs = clubs
    @users = User.includes(:clubs, :taggings).tagged_with(params[:id]).page(1).per(5)
  end

  private

  def show_params
    params.permit(:status, :lat, :lon, :attending, :adventure_type,
                  :distance)
  end

  def adventures
    if current_resource_owner
      Adventure
        .by_status_events_and_location(current_resource_owner,
                                       Invite.attendings[show_params[:attending]],
                                       show_params[:lat],
                                       show_params[:lon],
                                       show_params[:distance])
        .by_adventure_type(Adventure.adventure_types[show_params[:adventure_type]],
                           current_resource_owner)
    else
      Adventure.public_adventure
    end.canceled
      .by_status(Adventure::STATUS[params[:status]])
      .distinct
      .order(start_time: :asc)
      .tagged_with(params[:id])
      .page(1)
      .per(5)
      .includes(:owner, :club, :location, :taggings)
  end

  def clubs
    if show_params[:lat] && show_params[:lon]
      Club.near([show_params[:lat].to_f, show_params[:lon].to_f],
                show_params[:distance] || 100)
    else
      Club.all
    end.includes(:taggings).tagged_with(params[:id]).page(1).per(5)
  end
end
