class V1::InvitesController < V1::BaseController
  include ActionController::DataStreaming

  skip_before_action :doorkeeper_authorize!,
                     only: [:pending, :attending, :rejected]
  before_action :require_current_resource_owner,
                except: [:pending, :attending, :rejected]

  def create
    adventure = Adventure.find params[:adventure_id]

    if current_resource_owner == adventure.owner || !adventure.private_adventure?
      set_resource adventure.invites.build(user_id:    params[:user_id],
                                           invited_by: current_resource_owner)

      get_resource.save!
      InviteWorker.perform_async(get_resource.id)
      render :show, status: :created
    else
      head :forbidden
    end
  end

  def pending
    if params[:adventure_id]
      adventure = Adventure.find(params[:adventure_id])
      authorize adventure

      @invites = adventure.invites.includes(:user).pending
    else
      doorkeeper_authorize!
      require_current_resource_owner
      @invites = current_resource_owner
                   .pending_invites
                   .future
                   .canceled
    end

    respond_to do |format|
      format.csv do
        send_data @invites.to_csv, filename: "#{adventure.name} - Pending.csv"
      end

      format.json do
        @invites = @invites.page(page_params[:page]).per(page_size)
        render :index
      end
    end
  end

  def attending
    if params[:adventure_id]
      adventure = Adventure.find(params[:adventure_id])
      authorize adventure

      @invites = adventure.invites.includes(:user).ya
    else
      doorkeeper_authorize!
      require_current_resource_owner
      @invites = current_resource_owner
                   .attending_invites
                   .canceled
    end

    respond_to do |format|
      format.csv do
        send_data @invites.to_csv, filename: "#{adventure.name} - Attending.csv"
      end

      format.json do
        @invites = @invites.page(page_params[:page]).per(page_size)
        render :index
      end
    end
  end

  def rejected
    if params[:adventure_id]
      adventure = Adventure.find(params[:adventure_id])
      authorize adventure

      @invites = adventure.invites.includes(:user).na
    else
      doorkeeper_authorize!
      require_current_resource_owner
      @invites = current_resource_owner
                   .rejected_invites
                   .future
                   .canceled
    end

    respond_to do |format|
      format.csv do
        send_data @invites.to_csv, filename: "#{adventure.name} - Rejected.csv"
      end

      format.json do
        @invites = @invites.page(page_params[:page]).per(page_size)
        render :index
      end
    end
  end

  def accept
    adventure = Adventure.find(params[:adventure_id])

    if adventure.private_adventure?
      set_resource(adventure
                     .invites
                     .future
                     .find_by_user_id!(current_resource_owner.id))
      get_resource.accept!
    else
      set_resource(Invite
                     .future
                     .find_or_initialize_by(adventure: adventure,
                                            user:      current_resource_owner))
      get_resource.accept!

      get_resource.save!
    end

    render :show, status: :ok
  end

  def reject
    adventure = Adventure.find(params[:adventure_id])

    if adventure.private_adventure?
      set_resource(adventure
                     .invites
                     .future
                     .find_by_user_id!(current_resource_owner.id))
      get_resource.reject!
    else
      set_resource(Invite
                     .future
                     .find_or_initialize_by(adventure: adventure,
                                            user:      current_resource_owner))
      get_resource.reject!

      get_resource.save!
    end

    render :show, status: :ok
  end
end
