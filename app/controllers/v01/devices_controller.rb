class V01::DevicesController < V01::BaseController
  skip_before_action :set_resource
  before_action :require_current_resource_owner

  def create
    head :ok
  end

  private

  def device_params
    params.permit(:name, :identifier, :device_type)
  end
end
