class V01::SessionsController < V01::BaseController
  skip_before_action :doorkeeper_authorize!
  before_action :deny_session
  respond_to :json

  private

  def deny_session
    head :method_not_allowed
  end
end
