class V01::FriendsController < V01::BaseController
  skip_before_action :doorkeeper_authorize!, only: :index

  def index
    user = User.find(params[:user_id])

    type = params[:adventure_type] || Adventure.adventure_types.keys.join(',')

    @friends = user
                 .friends
                 .where("adventure_type_preferences && ? OR adventure_type_preferences IS NULL OR adventure_type_preferences = '{}'",
                        "{#{type}}")
                 .ordered

    @friends = @friends.where('similarity(name,?) > 0.15', params[:q]) if params[:q].present?

    @friends = @friends
                 .page(page_params[:page])
                 .per(page_size)
  end

  private

  def resource_name
    'user'
  end
end
