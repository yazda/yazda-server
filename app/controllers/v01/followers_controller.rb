class V01::FollowersController < V01::BaseController
  before_action :require_current_resource_owner

  def index
    user = User.find(params[:user_id])

    @followers = user.friends.ordered.page(page_params[:page]).per(page_size)
  end

  private

  def resource_name
    'user'
  end
end
