class V01::PasswordsController < V01::BaseController
  skip_before_action :doorkeeper_authorize!
  before_action :reject_if_current_resource_owner
  respond_to :json

  # POST /resource/password
  def create
    set_resource(resource_class.send_reset_password_instructions(resource_params))

    yield get_resource if block_given?

    head :ok
  end

  # PUT /resource/password
  def update
    set_resource(resource_class.reset_password_by_token(reset_password_params))

    yield get_resource if block_given?

    if get_resource.errors.empty?
      head :ok
    else
      raise ActiveRecord::RecordInvalid, get_resource
    end
  end

  private

  def reset_password_params
    params.permit(:reset_password_token, :password, :password_confirmation)
  end

  def user_params
    params.permit(:email)
  end

  def resource_name
    'user'
  end
end
