require "#{Rails.root}/lib/omniauth/facebook"
require "#{Rails.root}/lib/omniauth/strava"

class V01::ProvidersController < V01::BaseController
  skip_before_action :set_resource
  skip_before_action :doorkeeper_authorize!

  def facebook
    user_info, access_token = Omniauth::Facebook.authenticate(provider_params, request.referer)
    if user_info['email'].blank?
      Omniauth::Facebook.deauthorize(access_token)

      render json: { errors: ['Email is required'] }, status: :unprocessable_entity
    else
      generate_token user_info, access_token, 'facebook', current_resource_owner

      AppboyService.new(current_resource_owner)
        .track_event('connect-facebook') if current_resource_owner

      render json: { accessToken: @access_token,
                     grantType:   'credentials',
                     tokenType:   'bearer',
                     new_user:    @user.was_a_new_record }
    end
  end

  def strava
    user_info, access_token = Omniauth::Strava.authenticate(provider_params)
    if user_info['email'].blank?
      Omniauth::Strava.deauthorize(access_token)

      render json: { errors: ['Email is required'] }, status: :unprocessable_entity
    else
      generate_token user_info, access_token, 'strava', current_resource_owner

      AppboyService.new(current_resource_owner)
        .track_event('connect-strava') if current_resource_owner

      render json: { accessToken: @access_token,
                     grantType:   'credentials',
                     tokenType:   'bearer',
                     new_user:    @user.was_a_new_record }
    end
  end

  private

  def provider_params
    params.permit(:access_token, :auth_code)
  end

  def generate_token user_info, access_token, provider, owner
    @user = User.find_or_create_from_omniauth(user_info, access_token,
                                              provider, owner)

    application = if request.headers['HTTP_AUTHORIZATION']
                    token = request.headers['HTTP_AUTHORIZATION'].gsub(/.*\s/, '')
                    Doorkeeper::AccessToken.find_by_token(token).application
                  else
                    Doorkeeper::Application.find_by(name: 'Yazda Website')
                  end

    @access_token = Doorkeeper::AccessToken.create!(application:       application,
                                                    resource_owner_id: @user.id).token
  end
end
