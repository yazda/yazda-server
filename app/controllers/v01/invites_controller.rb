class V01::InvitesController < V01::BaseController
  before_action :require_current_resource_owner

  def create
    adventure = Adventure.find params[:adventure_id]

    if current_resource_owner == adventure.owner || adventure.public_adventure?
      set_resource adventure.invites.build(user_id: params[:user_id],
                                           invited_by: current_resource_owner)

      get_resource.save!
      InviteWorker.perform_async(get_resource.id)
      render :show, status: :created
    else
      head :forbidden
    end
  end

  def pending
    @invites = current_resource_owner
                 .pending_invites
                 .future
                 .canceled
                 .page(page_params[:page]).per(page_size)

    render :index
  end

  def accept
    adventure = Adventure.find(params[:adventure_id])

    if adventure.private_adventure?
      set_resource(adventure
                     .invites
                     .future
                     .find_by_user_id!(current_resource_owner.id))
      get_resource.accept!
    else
      set_resource(Invite
                     .future
                     .find_or_initialize_by(adventure: adventure,
                                            user:      current_resource_owner))
      get_resource.accept!

      get_resource.save!
    end

    render :show, status: :ok
  end

  def reject
    adventure = Adventure.find(params[:adventure_id])

    if adventure.private_adventure?
      set_resource(adventure
                     .invites
                     .future
                     .find_by_user_id!(current_resource_owner.id))
      get_resource.reject!
    else
      set_resource(Invite
                     .future
                     .find_or_initialize_by(adventure: adventure,
                                            user:      current_resource_owner))
      get_resource.reject!

      get_resource.save!
    end

    render :show, status: :ok
  end
end
