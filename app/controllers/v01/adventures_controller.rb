class V01::AdventuresController < V01::BaseController
  skip_before_action :doorkeeper_authorize!, only: [:index, :show]
  before_action :require_current_resource_owner, except: [:index, :show]

  def index
    @adventures = if params[:user_id].present?
                    Adventure.public_adventure.user(params[:user_id])
                  elsif params[:club_id].present?
                    Adventure.club(current_resource_owner, params[:club_id])
                  elsif current_resource_owner
                    Adventure
                      .by_status_events_and_location(current_resource_owner,
                                                     Invite.attendings[adventure_index_params[:attending]],
                                                     adventure_index_params[:lat],
                                                     adventure_index_params[:lon],
                                                     100)
                      .by_adventure_type(Adventure.adventure_types[adventure_index_params[:adventure_type]],
                                         current_resource_owner)
                  else
                    Adventure.public_adventure
                  end

    @adventures = @adventures
                    .canceled
                    .by_status(Adventure::STATUS[params[:status]])
                    .distinct
                    .order(start_time: :asc)
                    .page(page_params[:page])
                    .per(page_size)
  end

  def show
    authorize set_resource
  end

  def create
    @adventure      = current_resource_owner.adventures.build(resource_params)
    @adventure.club = current_resource_owner.clubs.find(params[:club_id]) if params[:club_id]

    @adventure.save!

    AppboyService.new(current_resource_owner)
      .track_event('create-adventure', @adventure)

    render :show, status: :created
  end

  def update
    authorize set_resource

    # TODO do not allow public to go private
    get_resource.attributes = update_adventure_params

    get_resource.save!(resource_params)

    if (get_resource.changed & notifies_of_changed).present?
      UpdateAdventureWorker.perform_in(15.minutes, get_resource.id)
    end

    AppboyService.new(current_resource_owner)
      .track_event('update-adventure', get_resource)

    render :show
  end

  def destroy
    authorize set_resource

    get_resource.cancel!

    AppboyService.new(current_resource_owner)
      .track_event('delete-adventure', get_resource)

    CancelAdventureWorker.perform_async get_resource.id

    head :no_content
  end

  private

  def notifies_of_changed
    %w(name start_time end_time lat lon address adventure_type skill_level)
  end

  def adventure_index_params
    params.permit(:status, :lat, :lon, :attending, :adventure_type, :distance)
  end

  def adventure_params
    params.permit(:adventure_type, :name, :description, :start_time, :end_time,
                  :reservation_limit, :address, :lat, :lon, :private,
                  :skill_level, user_ids: [])
  end

  def update_adventure_params
    params.permit(:name, :description, :start_time, :end_time,
                  :reservation_limit, :address, :lat, :lon, :skill_level)
  end
end
