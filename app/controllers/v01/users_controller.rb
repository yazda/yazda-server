require_relative '../../../lib/omniauth/strava'
require_relative '../../../lib/omniauth/facebook'

class V01::UsersController < V01::BaseController
  skip_before_action :doorkeeper_authorize!, only: [:avatar, :banner, :show]
  before_action :require_current_resource_owner, except: [:avatar, :banner,
                                                          :show]

  def search
    pref   = current_resource_owner.adventure_type_preferences.join(',')
    @users = SearchService
               .new(current_resource_owner.id)
               .find_user(params[:q], pref, page_size, page_params[:page])
# TODO find by user location
    render :index
  end

  def find_strava_users
    @users = []

    if current_resource_owner.strava?
      # TODO enum
      strava     = current_resource_owner.providers.where(provider: 'strava').first
      friend_ids = current_resource_owner.friendships.pluck(:friend_id)

      provider = Omniauth::Strava.new
      users    = provider.get_user_friends strava

      ids = users.map { |user| user['id'] }

      @users = User
                 .includes(:providers).where(providers: { uid: ids })
                 .where.not(id: friend_ids)
    end

    render :index
  end

  def find_facebook_users
    @users = []

    if current_resource_owner.facebook?
      # TODO enum
      facebook   = current_resource_owner.providers.where(provider: 'facebook').first
      friend_ids = current_resource_owner.friendships.pluck(:friend_id)


      provider = Omniauth::Facebook.new
      users    = provider.get_user_friends facebook

      ids = users.map { |user| user['id'] }

      @users = User
                 .includes(:providers).where(providers: { uid: ids })
                 .where.not(id: friend_ids)
    end

    render :index
  end

  def find_friends_with_email
    # TODO only allow Yazda apps to call this function
    query = { email: email_params }
    t     = User.arel_table
    limit = email_params ? email_params.length : 0

    @users = User
               .where(query)
               .where.not(id: current_resource_owner.friends.pluck(:id))
               .where.not(id: current_resource_owner.id)
               .order_by_emails(email_params)
               .page(page_params[:page])
               .per(limit) #TODO does this work?
  end

  def show
    @user = User.find(params[:id])
  end

  def index
    @users = User.where(id: user_id_params[:ids].split(','))
  end

  def avatar
    @user = User.find(params[:id])
    size  = ['thumb'].delete params[:size]

    redirect_to URI.parse(@user.avatar.url(size)).to_s
  end

  def banner
    @user = User.find(params[:id])
    size  = ['thumb'].delete params[:size]

    if @user.banner.present?
      redirect_to URI.parse(@user.banner.url(size)).to_s
    else
      head :ok
    end
  end

  private
  def resource_name
    'user'
  end

  def email_params
    param_list = params[:emails]
    emails     = []

    if param_list.is_a? String
      emails = param_list.split(',')
    elsif param_list.is_a? Array
      emails = param_list
    end

    emails
  end

  def user_id_params
    params.permit(:ids)
  end
end
