class V01::RegistrationsController < V01::BaseController
  skip_before_action :doorkeeper_authorize!
  before_action :reject_if_current_resource_owner
  respond_to :json

  def create
    @user = User.new(user_params)
    @user.build_notification_preference
    @user.club_roles.build(club:             Club.first,
                           role:             User.count == 0 ? :owner : :member,
                           adventure_invite: 'none',
                           chat:             'none')
    @user.save!

    render 'v1/account/me', status: :created
  end

  private

  def user_params
    params.permit(:name, :email, :password, :password_confirmation, :newsletter)
  end

  def resource_name
    'user'
  end
end
