class V01::AccountController < V01::BaseController
  before_action :require_current_resource_owner

  def get_settings
    set_resource(current_resource_owner)

    render :me
  end

  def post_settings
    set_resource(current_resource_owner)
    get_resource.update!(settings_params)

    render :me
  end

  def update_profile_image
    set_resource current_resource_owner
    get_resource.process_avatar_upload = true
    get_resource.update!(update_profile_image_params)

    render :me
  end

  def update_banner_image
    set_resource current_resource_owner
    get_resource.process_banner_upload = true
    get_resource.update!(update_banner_image_params)

    render :me
  end

  def me
    set_resource(current_resource_owner)

    render :me
  end

  private

  def update_banner_image_params
    params.permit(:banner)
  end

  def update_profile_image_params
    params.permit(:avatar)
  end

  def settings_params
    params.permit(:phone, :newsletter, :views_mountain_biking, :views_hiking,
                  :views_biking, :views_running, :views_trail_running,
                  :notifies_invite, :notifies_friend_request, :name,
                  :location, :description, :adventure_invite,
                  :adventure_edit, :adventure_cancel, :adventure_reminder,
                  :adventure_joined, :chat, :friend)
  end

  def resource_name
    'user'
  end
end
