class AppInvite < ActiveRecord::Base
  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false },
            format: { with: Devise.email_regexp }
end
