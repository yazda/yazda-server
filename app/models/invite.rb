class Invite < ActiveRecord::Base
  enum attending: [:ya, :pending, :na]

  scope :canceled, ->() { joins(:adventure).where(Adventure.arel_table[:canceled].eq(false)) }
  scope :future, ->() do
    joins(:adventure)
      .where(Adventure.arel_table[:start_time].gt(DateTime.now))
      .order('adventures.start_time ASC')
  end

  belongs_to :user, touch: true
  belongs_to :invited_by, class_name: 'User'
  belongs_to :adventure

  validates :user, presence: true
  validates :invited_by, presence: true
  validates :adventure, presence: true, uniqueness: { scope: :user_id }
  validate :adventure_not_canceled
  validate :adventure_full
  validates_associated :adventure

  before_validation :set_invited_by

  def self.to_csv
    attributes = %w(name email showed_up)

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.find_each do |invite|
        csv << [invite.user.name, nil, nil]
      end
    end
  end

  def self.status_invite_arel(user_id, status = nil)
    query = Invite.arel_table[:user_id].eq(user_id)

    if status
      query = query.and(Invite.arel_table[:attending].eq(status))
    else
      query = query.and(Invite
                          .arel_table[:attending]
                          .in([Invite.attendings[:ya],
                               Invite.attendings[:pending]]))
    end

    Invite.arel_table.grouping(query)
  end

  def set_invited_by
    self.invited_by ||= self.adventure.present? ? self.adventure.owner : nil
  end

  def accept!
    return if ya?

    self.accepted_at = DateTime.now
    self.rejected_at = nil

    ya!

    AppboyService.new(user)
      .track_event('accept-invite', self)

    InviteUpdatedWorker.perform_async(self.id)
  end

  def reject!
    return if na?

    self.rejected_at = DateTime.now
    self.accepted_at = nil

    na!

    AppboyService.new(user)
      .track_event('reject-invite', self)

    InviteUpdatedWorker.perform_async(self.id)
  end

  private

  def adventure_not_canceled
    if adventure.present? && adventure.canceled?
      errors.add :adventure, 'Cannot attend a canceled adventure'
    end
  end

  def adventure_full
    if adventure.present? && adventure.reservation_limit.present? &&
       attending_changed? && ya? &&
       adventure.invites.ya.count >= adventure.reservation_limit

      errors.add :adventure, 'You have reached the reservation limit'
    end
  end
end
