require Rails.root.join('lib', 'redcarpet', 'hashtag_html')

module YazdaTagsAndHtmlDescription
  extend ActiveSupport::Concern

  included do
    acts_as_taggable

    before_validation :set_html_description
    before_validation :set_tags

    def to_markdown(text)
      self.class.markdown.render(text) unless text.blank?
    end

    def set_html_description
      if description.present?
        dup_string = description.dup

        self.html_description = to_markdown dup_string
      else
        self.html_description = nil
      end
    end

    def set_tags
      self.tag_list = description.scan(/#(\w+)/).flatten if description.present?
    end
  end

  class_methods do
    def markdown
      Redcarpet::Markdown.new(HashtagHTML,
                              space_after_headers: true,
                              autolink:            true,
                              link_attributes:     {
                                style: 'text-decoration: none;'
                              })
    end
  end
end
