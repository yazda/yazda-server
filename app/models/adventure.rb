class Adventure < ActiveRecord::Base
  include YazdaTagsAndHtmlDescription

  geocoded_by :address, latitude: :lat, longitude: :lon
  reverse_geocoded_by :lat, :lon
  before_validation :set_location, if: ->(obj) { obj.location_id }
  before_validation :geocode,
                    if: ->(obj) do
                      !obj.location_id && (!obj.lat.present? || !obj.lon.present?)
                    end
  before_validation :reverse_geocode,
                    if: ->(obj) { !obj.location_id && !obj.address.present? }
  before_validation :update_duration
  after_commit :notify_invites, on: :create
  after_commit :appboy_campaigns, on: :create

  STATUS = ActiveSupport::HashWithIndifferentAccess.new(upcoming: :upcoming, past: :past)

  # IMPORTANT, DON'T CHANGE THE ORDER OF THIS OR YOU WILL BE SORRY
  enum privacy: [:public_adventure, :private_adventure, :link]
  enum adventure_type: [:mountain_biking, :hiking, :biking, :running,
                        :trail_running]

  scope :club, ->(user, club_id) do
    if user.present?
      role = ClubRole.find_by(user_id: user,
                              club_id: club_id)

      if role.present? && (role.admin? || role.owner?)
        where(club_id: club_id)
      else
        where(club_id: club_id, privacy: [0, 1])
      end
    else
      where(club_id: club_id, privacy: 0)
    end
  end
  scope :canceled, -> { where(Adventure.arel_table[:canceled].eq(false)) }
  scope :user, ->(user_id) do
    t = Invite.arel_table
    includes(:invites)
      .where(t[:attending]
               .eq(Invite.attendings[:ya])
               .and(t[:user_id].eq(user_id))
               .or(Adventure.arel_table[:user_id].eq(user_id)))
      .references(:invites)
  end

  scope :by_status_events_and_location,
        (proc do |user, status, lat, lon, distance|
          admin_leader_roles = user.club_roles.where(role: [2, 3])
          invites            = Invite.arel_table
          location           = Adventure.location_arel(lat, lon, distance)
          status_arel        = Invite.status_invite_arel(user.id, status)
          arel_query         = arel_table[:user_id].eq(user.id).or(status_arel)
          arel_query         = arel_query.or(location) if location

          adventure_invites = arel_table
                                .join(invites, Arel::Nodes::OuterJoin)
                                .on(arel_table[:id].eq(invites[:adventure_id])
                                      .and(invites[:user_id].eq(user.id)))
                                .join_sources

          adventure_ids = joins(adventure_invites)
                            .where(arel_table[:club_id]
                                     .in(user.clubs.pluck(:id))
                                     .and(arel_table[:privacy].in([0, 1]))
                                     .and(invites[:attending].not_eq(2)
                                            .or(invites[:id].eq(nil)))).pluck(:id)

          if admin_leader_roles.exists?
            adventure_ids.concat(
              joins(adventure_invites)
                .where(arel_table[:club_id]
                         .in(admin_leader_roles.pluck(:club_id))).pluck(:id)
            )
          end

          joins(adventure_invites).where(arel_query.or(arel_table[:id].in(adventure_ids.to_set.to_a)))
        end)
  scope :by_status,
        (proc do |status|
          case status
          when :past, 'past'
            where(arel_table[:end_time].lt(DateTime.now))
          else
            where(arel_table[:end_time].gt(DateTime.now))
          end
        end)
  scope :by_adventure_type,
        (proc do |type, user|
          if type
            where(adventure_type: type)
          else
            where(adventure_type: user.view_preferences)
          end
        end)
  scope :not_notified,
        (proc do
          canceled
            .where(start_time: DateTime.now..1.day.from_now, alert_sent_at: nil)
        end)

  belongs_to :club, counter_cache: true
  belongs_to :location
  belongs_to :owner, class_name: 'User', foreign_key: 'user_id',
             counter_cache:      true, touch: true
  has_many :invites, dependent: :destroy, inverse_of: :adventure
  has_many :users, through: :invites

  validates :owner, presence: true
  validates :adventure_type, presence: true
  validates :lat, presence: true, numericality: true
  validates :lon, presence: true, numericality: true
  validates :name, presence: true, length: { minimum: 5, maximum: 255 }
  validates :description, allow_blank: true, length: { maximum: 50_000 }
  validates :start_time, presence: true, date: { after: Time.now }
  validates :end_time, presence: true, date: { after:        :start_time,
                                               before_or_on: ->() { start_time + 1.month } }
  validates :duration, allow_blank: true, numericality: { only_integer: true, greater_than: 0 }
  validates :reservation_limit,
            allow_blank:  true,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :skill_level, allow_blank: true,
            numericality:              { greater_than_or_equal_to: 1,
                                         less_than_or_equal_to:    5,
                                         only_integer:             true }
  validate :invites_length_validation
  validate :user_club_role

  def is_sponsored?
    return club.is_yazda? if club
    false
  end

  def invited?(user)
    return false unless user
    user_id == user.id || users.exists?(user.id)
  end

  def attending?(user)
    return false unless user

    owner == user || invites.ya.exists?(user_id: user.id)
  end

  def responded?(user)
    return false unless user

    attending?(user) || invites.na.exists?(user_id: user.id)
  end

  def cancel!
    return if canceled

    self.canceled      = true
    self.canceled_date = DateTime.now

    save!
  end

  def private= val
    self.privacy = val ? :private_adventure : :public_adventure
  end

  private

  def self.location_arel(lat, lon, distance = 125)
    distance = 125 if distance.blank?

    if lat && lon
      t = Invite.arel_table

      near([lat.to_f, lon.to_f], distance).canceled
        .where(t[:attending].in([Invite.attendings[:pending], Invite.attendings[:ya]])
                 .or(t[:id].eq(nil))
                 .and(arel_table[:privacy].eq(0))).arel.constraints.reduce(:and)
    end
  end

  def invites_length_validation
    remaining_invites = invites.reject(&:marked_for_destruction?)

    if remaining_invites.empty? && private_adventure? && club_id.blank?
      errors.add :invites, 'There must be at least 1 invite for private adventures'
    end
  end

  def user_club_role
    return unless club

    unless ClubRole.exists?(club_id: club_id, user_id: user_id, role: [1, 2, 3])
      errors.add :club, 'You need leader or above permissions for this club'
    end
  end

  def update_duration
    self.duration = ((end_time.to_f - start_time.to_f) / 60).to_i
  end

  def notify_invites
    NotifyAdventureInviteesWorker.perform_in(5.minutes, id)
  end

  def set_location
    location = Location.find(self.try :location_id)

    if location
      self.lat     = location.lat
      self.lon     = location.lon
      self.address = location.address
    end
  end

  # TODO fix this to be more dynamic and database stored
  def appboy_campaigns
    return unless Rails.env.production? && mountain_biking? && public_adventure?

    api = Appboy::API.new(ENV['APPBOY_GROUP_ID'])

    # Denver inactive campaign
    if distance_to([39.67760, -105.07324]) < 16
      api.triggered(campaign_id: '7783e2d3-7b9f-4b21-8bf2-98c4a872e323')
      # VA/MD/DC
    elsif distance_to([39.09596, -77.22839]) < 30
      api.triggered(campaign_id: '0a02c936-b1c9-4383-a0f9-54b30484067e')
    end
  end
end
