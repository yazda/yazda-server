class ClubRole < ActiveRecord::Base
  enum role: [:member, :leader, :admin, :owner]

  ALL_NOTIF = %w(both email push none).freeze

  # TODO after_commit hook to send email when role has changed

  belongs_to :user
  belongs_to :club

  validates_associated :club

  validates :role, presence: true
  validates :club, presence: true
  validates :user, presence: true, uniqueness: { scope: :club_id }
  validates :adventure_invite,
            presence: true,
            inclusion: { in: ALL_NOTIF, message: 'only allows\'both, email, push, or none\'' }
  validates :chat,
            presence: true,
            inclusion: { in: ALL_NOTIF, message: 'only allows\'both, email, push, or none\'' }

end
