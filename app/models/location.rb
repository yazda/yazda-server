class Location < ActiveRecord::Base
  geocoded_by :address, latitude: :lat, longitude: :lon
  reverse_geocoded_by :lat, :lon
  before_validation :geocode,
                    if: ->(obj) { !obj.lat.present? || !obj.lon.present? }
  before_validation :reverse_geocode,
                    if: ->(obj) { !obj.address.present? }

  has_many :adventures

  validates :lat, presence: true, numericality: true
  validates :lon, presence: true, numericality: true
  validates :name, presence: true, length: { maximum: 255, minimum: 3 }
end
