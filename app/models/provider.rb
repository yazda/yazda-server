class Provider < ActiveRecord::Base
  belongs_to :user

  validates :provider, presence: true, inclusion: { in: ['strava', 'facebook'] }
  validates :uid, presence: true
  validates :user, presence: true
end
