class Friendship < ActiveRecord::Base
  enum status: [:pending, :approved, :rejected, :requested]

  after_commit :notify_user, on: :update, if: :pending?

  belongs_to :user
  belongs_to :friend, class_name: 'User'

  validates :user, presence: true
  validates :friend, presence: true, uniqueness: { scope: :user_id }

  def self.request_friend(user, friend)
    response = nil

    unless user == friend || Friendship.exists?(user: user, friend: friend)
      transaction do
        response = create!(user: user, friend: friend, status: :pending)
        create!(user: friend, friend: user, status: :requested)
      end
    end

    response
  end

  def accept!
    transaction do
      self.class.accept_relationship user, friend
      self.class.accept_relationship friend, user

      NotifyFriendAcceptedWorker.perform_async id
    end
  end

  def reject!
    transaction do
      self.class.reject_relationship user, friend
      self.class.reject_relationship friend, user
    end
  end

  private

  def status_changed_to_pending?
    status_changed? && pending?
  end

  def notify_user
    NotifyFriendRequestWorker.perform_async(id)
  end

  def self.accept_relationship(user, friend)
    request = find_by(user: user, friend: friend)

    request.accepted_at = DateTime.now
    request.rejected_at = nil

    request.approved! if request.pending? || request.requested?
  end

  def self.reject_relationship(user, friend)
    request = find_by(user: user, friend: friend)

    request.rejected_at = DateTime.now
    request.accepted_at = nil

    request.rejected! if request.pending? || request.requested?
  end
end
