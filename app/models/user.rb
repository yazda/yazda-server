class User < ActiveRecord::Base
  include YazdaTagsAndHtmlDescription

  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable and :omniauthable
  # TODO lock the user out
  devise :database_authenticatable, :registerable, :async, #:confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  enum gender: [:no_answer, :male, :female]

  attr_reader :was_a_new_record

  geocoded_by :location, latitude: :lat, longitude: :lon

  scope :ordered, -> { order(:name) }
  scope :search, (lambda do |q|
    select('id, 20 as weight').where('name <-> ? < 0.85', q)
  end)

  delegate :adventure_invite=, :adventure_edit=, :adventure_cancel=,
           :adventure_reminder=, :adventure_joined=, :chat=, :friend=, to:
             :notification_preference

  has_one :notification_preference, dependent: :destroy, autosave: true

  # oauth
  has_many :access_tokens,
           class_name:  'Doorkeeper::AccessToken',
           foreign_key: 'resource_owner_id'
  has_many :authenticated_applications,
           through:    :access_tokens,
           class_name: 'Doorkeeper::Application',
           source:     :application

  # friendships
  has_many :friendships, dependent: :destroy
  has_many :pending_friendships,
           ->() { where(status: Friendship.statuses[:pending]).order(created_at: :desc) },
           class_name: 'Friendship'
  has_many :requested_friendships,
           ->() { where(status: Friendship.statuses[:requested]).order(created_at: :desc) },
           foreign_key: 'friend_id', class_name: 'Friendship'
  has_many :rejected_friendships,
           ->() { where(status: Friendship.statuses[:rejected]).order(created_at: :desc) },
           class_name: 'Friendship'
  has_many :approved_friendships,
           ->() { where(status: Friendship.statuses[:approved]).order(created_at: :desc) },
           class_name: 'Friendship'

  # friends
  has_many :friends, ->() { order :name }, through: :approved_friendships

  # followers friendships
  has_many :incoming_friendships,
           ->() { where(status: Friendship.statuses[:pending]).order(created_at: :desc) },
           class_name:  'Friendship',
           foreign_key: 'friend_id'
  has_many :inverse_friendships,
           class_name:  'Friendship',
           foreign_key: 'friend_id',
           dependent:   :destroy

  # followers
  has_many :followers,
           ->() { order :id },
           through:    :inverse_friendships,
           class_name: 'User',
           source:     :user

  # adventures
  has_many :adventures, dependent: :destroy
  has_many :invited_adventures, through: :invites, source: :adventure

  # invites
  has_many :invites, dependent: :destroy
  has_many :pending_invites, ->() { pending.future }, class_name: 'Invite'
  has_many :attending_invites, ->() { ya.future }, class_name: 'Invite'
  has_many :rejected_invites, ->() { na.future }, class_name: 'Invite'

  # providers
  has_many :providers, dependent: :destroy

  # clubs
  has_many :club_roles, dependent: :destroy
  has_many :clubs, through: :club_roles

  mount_base64_uploader :avatar, AvatarUploader
  process_in_background :avatar

  mount_base64_uploader :banner, BannerUploader
  process_in_background :banner

  validates :banner,
            file_size:         { less_than_or_equal_to: 10.megabytes },
            file_content_type: { allow: %w(image/jpeg image/png image/jpg) },
            if:                :banner_changed?
  validates :avatar,
            file_size:         { less_than_or_equal_to: 10.megabytes },
            file_content_type: { allow: %w(image/jpeg image/png image/jpg) },
            if:                :avatar_changed?
  validates :name, presence: true, length: { maximum: 255, minimum: 2 }
  validates :description, length: { maximum: 140 }

  attr_accessor :views_mountain_biking, :views_hiking, :views_biking,
                :views_running, :views_trail_running

  skip_callback :commit, :after, :remove_previously_stored_avatar
  skip_callback :commit, :after, :remove_previously_stored_banner

  after_validation :geocode,
                   if: ->(obj) { obj.location.present? && obj.location_changed? }
  before_save :update_preferences
  before_save :set_new_record
  after_commit :appboy_updater
  after_commit :follow_up, on: :create
  after_commit :save_subscription, on: :create

  def self.order_by_emails(emails)
    if emails.present?
      order_by = ["case"]
      emails.each_with_index.map do |email, index|
        order_by << "WHEN email=#{ActiveRecord::Base.connection.quote email} THEN #{index}"
      end
      order_by << "end"
      order(order_by.join(" "))
    else
      where('1=1')
    end
  end

  def self.find_or_create_from_omniauth(user_info, access_token, provider_type, user = nil)
    email    = user_info['email']
    provider = Provider.find_or_initialize_by(provider: provider_type,
                                              uid:      user_info['id'])
    user     = user || User.find_by_email(email) || provider.user

    unless user
      password = Devise.friendly_token[0, 20]
      name     = user_info['name']
      location = nil

      if provider_type == 'strava'
        name     = "#{user_info['firstname']} #{user_info['lastname']}"
        location = "#{user_info['city']}, #{user_info['state']} #{user_info['country']}"
      end

      user = User.create(
        email:                   email,
        name:                    name,
        location:                location,
        password:                password,
        password_confirmation:   password,
        notification_preference: NotificationPreference.new
      )

      user.club_roles.create(club:             Club.first,
                             adventure_invite: 'none',
                             chat:             'none')
    end

    provider.user ||= user

    provider.access_token = access_token
    provider.save

    user
  end

  def adventure_leader?
    %w(james.stoup@gmail.com lauren@theinvisiblechecklist.com).include? email
  end

  def facebook?
    providers.exists?(provider: 'facebook')
  end

  def strava?
    providers.exists?(provider: 'strava')
  end

  def facebook_image
    provider = providers.where(provider: 'facebook').first

    "https://graph.facebook.com/#{provider.uid}/picture?type=large"
  end

  def view_preferences
    if adventure_type_preferences.count > 0
      adventure_type_preferences.map { |pref| Adventure.adventure_types[pref] }
    else
      Adventure.adventure_types.values
    end
  end

  def views_mountain_biking?
    adventure_type_preferences.include?('mountain_biking')
  end

  def views_hiking?
    adventure_type_preferences.include?('hiking')
  end

  def views_biking?
    adventure_type_preferences.include?('biking')
  end

  def views_running?
    adventure_type_preferences.include?('running')
  end

  def views_trail_running?
    adventure_type_preferences.include?('trail_running')
  end

  def notifies_invite=(param)
    notification_preference.invite = param
  end

  def notifies_invite
    notification_preference.invite?
  end

  alias notifies_invite? notifies_invite

  def notifies_friend_request=(param)
    notification_preference.friend_request = param
  end

  def notifies_friend_request
    notification_preference.friend_request?
  end

  alias notifies_friend_request? notifies_friend_request

  private

  def update_preferences
    if views_mountain_biking
      adventure_type_preferences << 'mountain_biking' unless views_mountain_biking?
    elsif views_mountain_biking == false
      adventure_type_preferences.delete('mountain_biking')
    end

    if views_biking
      adventure_type_preferences << 'biking' unless views_biking?
    elsif views_biking == false
      adventure_type_preferences.delete('biking')
    end

    if views_hiking
      adventure_type_preferences << 'hiking' unless views_hiking?
    elsif views_hiking == false
      adventure_type_preferences.delete('hiking')
    end

    if views_running
      adventure_type_preferences << 'running' unless views_running?
    elsif views_running == false
      adventure_type_preferences.delete('running')
    end

    if views_trail_running
      adventure_type_preferences << 'trail_running' unless views_trail_running?
    elsif views_trail_running == false
      adventure_type_preferences.delete('trail_running')
    end
  end

  def appboy_updater
    AppboyTrackUserWorker.perform_async(id)
  end

  def follow_up
    NotificationMailer.delay_for(2.weeks).follow_up(id)
  end

  def save_subscription
    if newsletter
      NewsletterSubscriptionWorker.perform_async(:subscribe, id)
    else
      NewsletterSubscriptionWorker.perform_async(:unsubscribe, id)
    end
  end

  def set_new_record
    @was_a_new_record = new_record?
    true
  end
end
