require 'uri'

class Club < ActiveRecord::Base
  include YazdaTagsAndHtmlDescription

  geocoded_by :location, latitude: :lat, longitude: :lon

  skip_callback :commit, :after, :remove_previously_stored_avatar
  skip_callback :commit, :after, :remove_previously_stored_banner

  before_validation :geocode,
                    if: (lambda do |obj|
                      obj.location.present? && obj.location_changed?
                    end)
  before_validation :set_http

  has_many :adventures, dependent: :destroy
  has_many :club_roles, dependent: :destroy
  has_many :users, through: :club_roles

  scope :search,
        (proc do |q|
          if q.present?
            where('name <-> ? < 0.85', q)
          else
            all
          end
        end)

  mount_base64_uploader :avatar, AvatarUploader
  process_in_background :avatar

  mount_base64_uploader :banner, BannerUploader
  process_in_background :banner

  mount_base64_uploader :waiver, WaiverUploader
  process_in_background :waiver

  validates :name, length: { minimum: 4, maximum: 255 }
  validates :description, allow_blank: true, length: { maximum: 50_000 }
  validates :website, allow_blank: true, length: { maximum: 255 }
  validates :location, presence: true, length: { maximum: 255 }
  validates :lat, presence: true, numericality: true
  validates :lon, presence: true, numericality: true
  validates :contact_email,
            allow_blank: true,
            length:      { maximum: 255 },
            format:      { with: Devise.email_regexp }
  validates :banner,
            file_size:         { less_than_or_equal_to: 10.megabytes },
            file_content_type: { allow: %w(image/jpeg image/png image/jpg) },
            if:                :banner_changed?
  validates :avatar,
            file_size:         { less_than_or_equal_to: 10.megabytes },
            file_content_type: { allow: %w(image/jpeg image/png image/jpg) },
            if:                :avatar_changed?

  validate :is_url?

  def adventures_count(user)
    Adventure.club(user, id).canceled.by_status(nil).count
  end

  def is_yazda?
    id == 1
  end

  private

  def is_url?
    URI.parse(website).present?
  rescue URI::InvalidURIError
    false
  end

  def set_http
    website.prepend('http://') if website.present? && !(website =~ /^https?:\/\//)
  end
end
