class NotificationPreference < ActiveRecord::Base
  belongs_to :user

  before_save :set_adventure_invites, if: :invite_changed?
  before_save :set_friend, if: :friend_request_changed?

  ALL_NOTIF = %w(both email push none).freeze

  validates :user, presence: true
  validates :adventure_invite, presence: true, inclusion: { in: ALL_NOTIF }
  validates :adventure_edit, presence: true, inclusion: { in: ALL_NOTIF }
  validates :adventure_cancel, presence: true, inclusion: { in: ALL_NOTIF }
  validates :adventure_reminder, presence: true, inclusion: { in: ALL_NOTIF }
  validates :adventure_joined, presence: true, inclusion: { in: ALL_NOTIF }
  validates :chat, presence: true, inclusion: { in: ALL_NOTIF }
  validates :friend, presence: true, inclusion: { in: ALL_NOTIF }

  private

  def set_adventure_invites
    if self.invite
      self.adventure_invite = 'both'
      self.adventure_edit = 'both'
      self.adventure_cancel = 'both'
      self.adventure_reminder = 'both'
      self.adventure_joined = 'both'
    else
      self.adventure_invite = 'email'
      self.adventure_edit = 'email'
      self.adventure_cancel = 'email'
      self.adventure_reminder = 'none'
      self.adventure_joined = 'none'
    end
  end

  def set_friend
    if self.friend_request
      self.friend = 'both'
    else
      self.friend = 'none'
    end
  end
end
