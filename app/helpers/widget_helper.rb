module WidgetHelper
  def background_style(adventure)
    return unless adventure.club.present?

    "background-image: url(#{adventure.club.banner.url})"
  end

  def skill_level(adventure)
    case adventure.skill_level
    when 1
      'easiest'
    when 2
      'easy'
    when 3
      adventure.mountain_biking? ? 'intermediate' : 'medium'
    when 4
      'hard'
    when 5
      'hardest'
    else
      ''
    end
  end
end
