FROM ruby:2.2
MAINTAINER james.stoup@yazdaapp.com

RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs \
    imagemagick \
    libpq-dev \
    libjpeg62 \
    postgresql-client-9.4

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --jobs 20 --retry 5

COPY . ./
