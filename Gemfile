source 'https://rubygems.org'

gem 'rails', '4.2.7.1'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'jquery-rails'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'devise'
gem 'devise-async'
gem 'gibbon'
gem 'carrierwave'
gem 'carrierwave_backgrounder'
gem 'carrierwave-base64'
gem 'mini_magick'
gem 'fog'
gem 'sidekiq'
gem 'sidekiq-scheduler'
gem 'file_validators'
gem 'doorkeeper'
gem 'jbuilder'
gem 'versionist'
gem 'kaminari'
gem 'multi_json'
gem 'oj'
gem 'yajl-ruby'
gem 'geocoder'
gem 'sinatra'
gem 'rack-throttle'
gem 'redis-rails'
gem 'migration_data'
gem 'aws-sdk'
gem 'rails_admin'
gem 'rack-cors'
gem 'omniauth'
gem 'omniauth-oauth2'
gem 'httparty'
gem 'silencer'
gem 'okcomputer'
gem 'activerecord-import'
gem 'mongo'
gem 'sitemap_generator'
gem 'meetup_client'
gem 'appboy', github: 'jstoup111/appboy'
gem 'strava-api-v3'
gem 'pry'
gem 'bugsnag'
gem 'responders', '~> 2.0'
gem 'foundation-rails'
gem 'browser-timezone-rails'
gem "non-stupid-digest-assets"
gem 'pundit'
gem 'redcarpet'
gem 'acts-as-taggable-on', '~> 4.0'
gem 'timezone'
gem 'blazer'

gem 'sidekiq-status'
gem 'sidekiq-failures'
gem 'sidekiq-unique-jobs'

# New Relic
gem 'newrelic_rpm'

# Use Unicorn as the app server
gem 'unicorn'

group :production do
  gem 'rails_12factor'
end

group :development, :test do
  gem 'rb-readline'
  gem 'growl'
  gem 'byebug'

  gem 'spring'
  gem 'guard-brakeman'
  gem 'guard-rubocop'
  gem 'brakeman'
  gem 'foreman'
  gem 'faker'
  gem 'bullet'
end

group :development do
  gem 'web-console'
  gem 'lol_dba'
  gem 'peek'
  gem 'flay'
  gem 'annotate'
  gem 'quiet_assets'
end

group :test do
  gem 'minitest-rails'
  gem 'minitest-reporters'
  gem 'shoulda', require: false
  gem 'guard-minitest', require: false
  gem 'oauth2', require: false
  gem 'mocha', require: false
  gem 'database_cleaner'
  gem 'simplecov', '~> 0.10.0', require: false
end
