[![Build Status](https://semaphoreci.com/api/v1/projects/cb26bd0b-4b11-4b6b-81a4-b09e668018bd/580823/badge.svg)](https://semaphoreci.com/jstoup111/yazda-server)
# ENV Variables

MAILCHIMP_API_KEY
REDIS_URL


# Sidekiq Queues

- default
- mailer
- carrierwave


For API doc
```
rake swagger:docs
```

# SETUP

install docker https://www.docker.com/products/docker-toolbox

Set up your environment variables and make sure they are setup
```
cp .yazda.env.sample .yazda.env
cp .yazda.env.sample .sidekiq.env
```

#### For Windows 

```
npm install docker-share --global
cd yazda-server
docker-share mount --transient
cd ../yazda-volunteer-tracking
docker-share mount --transient
```

Make sure to put this env variable infront of commands

```
COMPOSE_CONVERT_WINDOWS_PATHS=1
COMPOSE_CONVERT_WINDOWS_PATHS=1 docker-compose up
```

### Create your docker volumes

```
docker-machine start default
docker volume create --name yazda-postgres
docker volume create --name yazda-redis
docker volume create --name yazda-mongo
docker-compose build
docker-compose run yazda rake db:setup
```

Update your /etc/hosts file and add the following line
```
vim /etc/hosts

192.168.99.100 dev.yazdaapp.com yazda_server.dev yazda-server.dev yazda-volunteer-tracking.dev yazda-volunteer-tracking.com
```

# Getting Variables

For other applications you'll need to get the application ID and secret keys
```
http://dev.yazdaapp.com/oauth/applications
```

# Setup any other Projects now

yazda-volunteer-tracking

# Running

Start your docker instances
```
docker-compose up
```

Running rake commands is easy
```
docker-compose run yazda rake db:setup
docker-compose run yazda rake db:migrate
```

# Mail
Emails will be sent to
```
http://dev.yazdaapp.com:81
```

# Testing
```
docker-compose run yazda guard --force-polling
```
