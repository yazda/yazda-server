SimpleCov.minimum_coverage 70
SimpleCov.start 'rails' do
  add_group 'V1/Controllers', 'app/controllers/v1'
  add_group 'Services', 'app/services'
  add_group 'Workers', 'app/workers'
  add_group 'Uploaders', 'app/uploaders'
  add_group 'Validators', 'app/validators'
end
