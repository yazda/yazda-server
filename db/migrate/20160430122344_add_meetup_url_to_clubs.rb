class AddMeetupUrlToClubs < ActiveRecord::Migration
  def change
    add_column :clubs, :meetup_id, :string, index: true
    add_column :clubs, :meetup_type, :string
    add_column :adventures, :meetup_id, :string, index: true
  end

  def data
    james = User.first

    club_hashes = [
      {
        name:        'Jefferson County Mountain Bikers Jeffco MTB',
        url:         'Jefferson-County-Mountain-Bikers-Jeffco-MTB',
        location:    'Denver CO, USA',
        meetup_type: 'mountain_biking',
        description: "We are a casual group of variously skilled mountain bike riders who spend most of their time riding in Colorado's Jefferson County Parks. Most active participants within this group posses a solid intermediate to advanced skill level.  The ride selection, pace of rides, routes and the style we choose are generally more challenging than many of the other groups. The focus of the group is to have fun while riding and improve your skills at the same time. Non-members can still join us on our weekly Tuesday evening rides by examining our calendar for the location of the week's ride. Check out this video clip that shows some of the Jeffco Crew in action http://www.youtube.com/watch?v=eleq1Uvn9Gc"
      },
      {
        name:        'Gears and Beers Mountain Bike Group',
        url:         'mountainbikes-134',
        location:    'Reston VA, USA',
        meetup_type: 'mountain_biking',
        description: "Connect with other trail hounds to ride some sweet singletrack and make some good friends in the process. Rides are scheduled anywhere we can find good trails within driving distance of the NoVa/MD/DC area. All skill levels are welcome as long as you come to RIDE the trails. If you want to cruise on a paved trail, this is not the meetup for you. If you want mud, sweat, and some good times, you are in the right place! You must have a trail ready bike and a helmet!! While having this meetup on your list will make you look cool, this group is really about riding. If you don't make any meetups, you will be deleted. :("
      },
      {
        name:        'MVD Mountain Bikers',
        url:         'MVDMountainBikers',
        location:    'Burtonsville MD, USA',
        meetup_type: 'mountain_biking',
        description: "We are a group of friends that met through mountain biking. We hope we have a chance to meet you too. We mainly do mountain bike rides, but trails tend to get messy and we hit the roads at times. With that being said, the trails are waiting for you!  Let's ride!"
      },
      {
        name:        'A-1 Cycling Events',
        url:         'A-1-Cycling-Events',
        location:    'Herdon VA, USA',
        meetup_type: 'mountain_biking',
        description: "Every Sunday, A-1 Cycling in Herndon hosts a shop mountain bike ride that welcomes riders of all levels. From novice to expert, we provide a fully supported* mountain bike ride each Sunday evening meeting in the Skatequest Reston parking lot and riding the Lake Fairfax trail system (aka. Difficult Run). We plan to hit the trails between 5:45 and 6pm and ride for about an hour to an hour and a half. Post ride festivities to follow. (BYOB and food for the grill) *Fully supported means we have a trained mechanic with each group carrying tools and a finite amount of tubes and other supplies. We are there to help, but remember, it's always good to ride with your own supplies."
      }
    ]

    club_hashes.each do |club_hash|
      email = "#{club_hash[:url]}@#{club_hash[:url]}.com"

      notification_preference = NotificationPreference.new(
        adventure_invite:   'push',
        adventure_edit:     'push',
        adventure_cancel:   'push',
        adventure_reminder: 'none',
        adventure_joined:   'none',
        chat:               'none',
        friend:             'none'
      )

      user = User.new(
        name:                    club_hash[:name],
        email:                   email,
        password:                'thisisasuperhardpasswordtoguessunlessyourehackingme',
        password_confirmation:   'thisisasuperhardpasswordtoguessunlessyourehackingme',
        location:                club_hash[:location],
        notification_preference: notification_preference
      )
      user.save!

      club = Club.new(
        name:        club_hash[:name],
        description: club_hash[:description],
        meetup_id:   club_hash[:url],
        location:    club_hash[:location],
        meetup_type: club_hash[:meetup_type]
      )

      club_role = ClubRole.new(
        role:             'leader',
        club:             club,
        user:             user,
        adventure_invite: 'none',
        chat:             'none'
      )

      club_role.save!

      club.club_roles.create! user:             james,
                              role:             'owner',
                              chat:             'none',
                              adventure_invite: 'none'
    end
  end
end
