class ChangeChatToPushByDefault < ActiveRecord::Migration
  def up
    change_column_default :notification_preferences, :chat, 'push'
  end

  def down
    change_column_default :notification_preferences, :chat, 'both'
  end
end
