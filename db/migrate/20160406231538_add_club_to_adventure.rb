class AddClubToAdventure < ActiveRecord::Migration
  def change
    add_reference :adventures, :club, index: true
  end
end
