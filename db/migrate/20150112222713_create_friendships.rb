class CreateFriendships < ActiveRecord::Migration
  def change
    create_table :friendships do |t|
      t.belongs_to :user, index: true
      t.belongs_to :friend, index: true
      t.integer :status, index: true, default: 0
      t.datetime :accepted_at
      t.datetime :rejected_at

      t.timestamps null: false
    end
    add_foreign_key :friendships, :users
    add_index :friendships, [:user_id, :friend_id], unique: true
  end
end
