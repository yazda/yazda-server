class CreateAppInvites < ActiveRecord::Migration
  def change
    create_table :app_invites do |t|
      t.string :email, null: false

      t.timestamps null: false
    end

    add_index :app_invites, :email, unique: true
  end
end
