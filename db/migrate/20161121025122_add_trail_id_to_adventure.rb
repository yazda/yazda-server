class AddTrailIdToAdventure < ActiveRecord::Migration
  def change
    add_column :adventures, :trail_id, :string
    add_index :adventures, :trail_id
  end
end
