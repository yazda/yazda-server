class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.integer :device_type
      t.string :name
      t.string :identifier
      t.string :aws_arn

      t.timestamps null: false
    end
  end
end
