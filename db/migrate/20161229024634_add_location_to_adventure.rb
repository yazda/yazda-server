class AddLocationToAdventure < ActiveRecord::Migration
  def change
    add_reference :adventures, :location, index: true, foreign_key: true
  end
end
