class AddFieldsToNotificationPreferences < ActiveRecord::Migration
  def change
    add_column :notification_preferences,
               :adventure_invite,
               :string, null: false, default: 'both'
    add_column :notification_preferences,
               :adventure_edit,
               :string, null: false, default: 'both'
    add_column :notification_preferences,
               :adventure_cancel,
               :string, null: false, default: 'both'
    add_column :notification_preferences,
               :adventure_reminder,
               :string, null: false, default: 'none'
    add_column :notification_preferences,
               :adventure_joined,
               :string, null: false, default: 'none'
    add_column :notification_preferences,
               :chat,
               :string, null: false, default: 'both'
    add_column :notification_preferences,
               :friend,
               :string, null: false, default: 'both'
  end

  def data
    NotificationPreference.find_each do |pref|
      pref.adventure_invite = pref.invite ? 'both' : 'email'
      pref.adventure_edit = pref.invite ? 'both' : 'email'
      pref.adventure_cancel = pref.invite ? 'both' : 'email'
      pref.adventure_reminder = pref.invite ? 'both' : 'none'
      pref.adventure_joined = pref.invite ? 'both' : 'none'
      pref.friend = pref.friend_request ? 'both' : 'none'

      pref.save
    end
  end
end
