class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :location, :string, limit: 128
    add_column :users, :adventure_type_preferences, :text, array: true, default: []
  end
end
