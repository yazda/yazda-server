class AddDefaultToNotificationPreferences < ActiveRecord::Migration
  def up
    change_column :users, :adventure_type_preferences, :text, array: true, default: ['hiking',
                                                                                     'mountain_biking']
  end

  def down
    change_column :users, :adventure_type_preferences, :text, array: true, default: []
  end
end
