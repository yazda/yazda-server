class AddExternalSyncToClub < ActiveRecord::Migration
  def change
    add_column :clubs, :external_sync, :boolean, default: false
  end
end
