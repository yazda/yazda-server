class AddLatAndLonToUser < ActiveRecord::Migration
  def change
    add_column :users, :lat, :float
    add_column :users, :lon, :float

    add_index :users, [:lat, :lon]
  end

  def data
    User.where.not(location: nil).each do |user|
      user.geocode
      user.save
    end
  end
end
