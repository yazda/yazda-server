class SetPrivateDefaultFalse < ActiveRecord::Migration
  def up
    change_column_default :adventures, :private, false
  end

  def down
    change_column_default :adventures, :private, true
  end
end
