class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name, null: false
      t.text :description, limit: 50_000
      t.integer :adventures_count
      t.string :avatar
      t.string :banner
      t.string :website
      t.string :contact_email
      t.string :waiver
      t.string :location, null: false
      t.float :lat, null: false
      t.float :lon, null: false

      t.timestamps null: false
    end
  end
end
