class AddPgTrgm < ActiveRecord::Migration
  def up
    execute 'create extension pg_trgm'
    execute 'CREATE INDEX users_name_trgm_idx ON users USING gist (name gist_trgm_ops)'
  end

  def down
    execute 'DROP INDEX users_name_trgm_idx'
    execute 'drop extension pg_trgm'
  end
end
