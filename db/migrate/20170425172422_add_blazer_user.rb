class AddBlazerUser < ActiveRecord::Migration
  def change
    sql = <<SQL
BEGIN;
CREATE ROLE blazer LOGIN PASSWORD '#{ENV["BLAZER_DB_PASSWORD"]}';
GRANT CONNECT ON DATABASE "#{Rails.configuration.database_configuration[Rails.env]['database']}" TO blazer;
GRANT USAGE ON SCHEMA public TO blazer;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO blazer;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO blazer;
COMMIT;
SQL

    execute sql
  end
end
