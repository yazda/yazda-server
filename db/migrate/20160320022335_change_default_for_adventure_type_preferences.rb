class ChangeDefaultForAdventureTypePreferences < ActiveRecord::Migration
  def up
    change_column_default :users, :adventure_type_preferences, []
  end

  def down
    change_column_default :users, :adventure_type_preferences, ['hiking',
                                                                'mountain_biking']
  end
end
