class RemovePrivateFromAdventures < ActiveRecord::Migration
  def change
    remove_column :adventures, :private
  end
end
