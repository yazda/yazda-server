class AddInvitedByToInvites < ActiveRecord::Migration
  def change
    add_reference :invites, :invited_by, references: :user, index: true
    add_foreign_key :invites, :users, column: :invited_by_id
  end

  def data
    Invite.all.each do |invite|
      if(!invite.invited_by)
        invite.invited_by = invite.adventure.owner
        invite.save
      end
    end
  end
end
