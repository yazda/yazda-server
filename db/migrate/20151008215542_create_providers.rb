class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.string :provider
      t.string :uid
      t.string :access_token
      t.references :user, index: true
    end
    add_index :providers, :provider
    add_index :providers, :uid
    add_index :providers, [:uid, :provider]
  end
end
