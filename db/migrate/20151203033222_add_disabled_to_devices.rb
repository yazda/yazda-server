class AddDisabledToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :disabled, :boolean, default: false
  end
end
