class CreateFriendshipsForNewStructure < ActiveRecord::Migration
  def change
  end

  def data
    Friendship.find_each do |f|
      unless Friendship.exists?(user: f.friend, friend: f.user)
        case f.status.to_sym
        when :approved, :rejected
          Friendship.create! user: f.friend, friend: f.user, status: f.status
        when :pending
          Friendship.create! user: f.friend, friend: f.user, status: :requested
        when :requested
          Friendship.create! user: f.friend, friend: f.user, status: :pending
        end
      end
    end
  end
end
