class AddPgTrmToClubs < ActiveRecord::Migration
  def up
    execute 'CREATE INDEX clubs_name_trgm_idx ON clubs USING gist (name gist_trgm_ops)'
  end

  def down
    execute 'DROP INDEX clubs_name_trgm_idx'
  end

  def data
    yazda = Club.first

    unless yazda
      james = User.find_by(email: 'james.stoup@gmail.com')

      club_role = james.club_roles.build(role: :owner)

      club = club_role.build_club(
        name:          'Yazda',
        description:   'Yazda is an app for mountain bikers, road bikers, and
hikers to discover their next adventure.',
        website:       'https://yazdaapp.com',
        contact_email: 'yazda@yazdaapp.com',
        location:      'Lakewood, CO, USA')

      james.save

      User.find_each do |user|
        user
          .club_roles
          .create! club:             club,
                   adventure_invite: 'none',
                   chat:             'none' unless user.email === 'james.stoup@gmail.com'
      end
    end
  end
end
