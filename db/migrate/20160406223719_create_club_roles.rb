class CreateClubRoles < ActiveRecord::Migration
  def change
    create_table :club_roles do |t|
      t.integer :role, default: 0, null: false
      t.boolean :accepted, default: false, null: false
      t.belongs_to :user, index: true, null: false
      t.belongs_to :club, index: true, null: false
      t.string :adventure_invite, default: 'both'
      t.string :chat, default: 'email'

      t.timestamps null: false
    end

    add_index :club_roles, [:user_id, :club_id], unique: true
  end
end
