class AddPrivacyToAdventures < ActiveRecord::Migration
  def change
    add_column :adventures, :privacy, :integer, required: true, default: 0
  end

  def data
    # Default all previously public to public
    Adventure.where(Adventure.arel_table[:private].eq(false))
      .update_all(privacy: 0)

    # Default all previously private to private
    Adventure.where(Adventure.arel_table[:private].eq(true))
      .update_all(privacy: 1)
  end
end
