class CreateNotificationPreferences < ActiveRecord::Migration
  def change
    create_table :notification_preferences do |t|
      t.boolean :invite, default: true
      t.boolean :friend_request, default: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end

  def data
    User.all.each do |user|
      NotificationPreference.create(user: user)
    end
  end
end
