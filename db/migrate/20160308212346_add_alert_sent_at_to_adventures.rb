class AddAlertSentAtToAdventures < ActiveRecord::Migration
  def change
    add_column :adventures, :alert_sent_at, :datetime
  end
end
