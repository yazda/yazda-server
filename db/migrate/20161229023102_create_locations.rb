class CreateLocations < ActiveRecord::Migration
  def up
    create_table :locations do |t|
      t.string :name
      t.float :lat
      t.float :lon
      t.string :address
      t.string :trail_id

      t.timestamps null: false
    end

    execute 'CREATE INDEX locations_name_trgm_idx ON locations USING gist (name gist_trgm_ops)'
  end

  def down
    execute 'DROP INDEX locations_name_trgm_idx'
    drop_table :locations
  end
end
