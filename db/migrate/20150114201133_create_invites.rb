class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.integer :attending, null: false, default: 1
      t.datetime :accepted_at
      t.datetime :rejected_at
      t.belongs_to :user, index: true, null: false
      t.belongs_to :adventure, index: true

      t.timestamps null: false
    end
    add_foreign_key :invites, :users
    add_foreign_key :invites, :adventures

    add_index :invites, [:user_id, :adventure_id], unique: true
  end
end
