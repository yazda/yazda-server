class AddSkillLevelToAdventures < ActiveRecord::Migration
  def change
    add_column :adventures, :skill_level, :integer
  end
end
