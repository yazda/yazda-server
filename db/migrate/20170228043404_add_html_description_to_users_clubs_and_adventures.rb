class AddHtmlDescriptionToUsersClubsAndAdventures < ActiveRecord::Migration
  def change
    add_column :users, :html_description, :text
    add_column :clubs, :html_description, :text
    add_column :adventures, :html_description, :text
  end

  def data
    ActiveRecord::Base.record_timestamps = false

    begin
      User.find_each do |user|
        user.set_html_description
        user.set_tags
        user.save
      end

      Club.find_each do |club|
        club.set_html_description
        club.set_tags
        club.save
      end

      Adventure.find_each do |adventure|
        adventure.set_html_description
        adventure.set_tags
        adventure.save(validate: false)
      end
    ensure
      ActiveRecord::Base.record_timestamps = true # don't forget to enable it again!
    end
  end
end
