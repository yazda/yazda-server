class CreateAdventures < ActiveRecord::Migration
  def change
    create_table :adventures do |t|
      t.belongs_to :user, index: true, null: false
      t.integer :adventure_type
      t.string :name, null: false
      t.text :description, limit: 50_000
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      t.string :address
      t.float :lat
      t.float :lon
      t.boolean :private, default: true
      t.float :duration
      t.integer :reservation_limit

      t.timestamps null: false
    end

    add_foreign_key :adventures, :users

    add_index :adventures, :adventure_type
    add_index :adventures, :private
    add_index :adventures, [:adventure_type, :private]
    add_index :adventures, [:lat, :lon]
  end
end
