class AddCanceledToAdventure < ActiveRecord::Migration
  def change
    add_column :adventures, :canceled, :boolean, default: false, null: false
    add_column :adventures, :canceled_date, :datetime
  end
end
