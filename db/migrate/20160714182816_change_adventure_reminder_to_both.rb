class ChangeAdventureReminderToBoth < ActiveRecord::Migration
  def up
    change_column_default :notification_preferences, :adventure_reminder, 'both'
  end

  def down
    change_column_default :notification_preferences, :adventure_reminder, 'none'
  end
end
