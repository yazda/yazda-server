# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

redirect_uri = ENV['REDIRECT_URL'] ? ENV['REDIRECT_URL'] : 'urn:ietf:wg:oauth:2.0:oob'

Doorkeeper::Application.find_or_create_by(name: 'Yazda iOS') do |a|
  a.official     = true
  a.redirect_uri = redirect_uri
end

Doorkeeper::Application.find_or_create_by(name: 'Yazda Android') do |a|
  a.official     = true
  a.redirect_uri = redirect_uri
end

Doorkeeper::Application.find_or_create_by(name: 'Yazda Volunteer Tracking') do |a|
  a.official     = true
  a.redirect_uri = redirect_uri
end

locations = [
  {
    name:     'Difficult Run - Skatequest',
    lat:      38.950648,
    lon:      -77.3318474,
    address:  '1805 Michael Faraday Drive, Reston, VA 20190, USA',
    trail_id: 'fFxLunDvDqT22ri4y'
  },
  {
    name:     'Fountainhead Regional Park',
    lat:      38.724924,
    lon:      -77.330746,
    address:  '10875 Hampton Rd, Fairfax Station, VA 22039',
    trail_id: 'yh3p4CkdmoCkYtZ6C'
  },
  {
    name:     'North Table - 93 Lot',
    lat:      39.782367,
    lon:      -105.229779,
    address:  'North Table Mountain Park, Colorado 93, Golden, CO',
    trail_id: 'SiMp6vtM5XGxPCx4f'
  },
  {
    name:     'Apex - Lower lot',
    lat:      39.7161421,
    lon:      -105.2100167,
    address:  'Apex Park, Golden CO',
    trail_id: 'PiGNnfcdpvhnXS3gB'
  },
  {
    name:     'White Ranch - Lower Lot',
    lat:      39.798791,
    lon:      -105.24837,
    address:  'White Ranch Open Space Park lower parking lot',
    trail_id: '37uAyzXo4CTKu3XHi'
  },
  {
    name:     'Lair o\' the Bear',
    lat:      39.6674091,
    lon:      -105.2588397,
    address:  '226600 Colorado 74, Idledale, CO 80453',
    trail_id: 'Ndj5hLna4j2zpnxPx'
  },
  {
    name:     'Chimney Gultch - Lot off 6th',
    lat:      39.7504481,
    lon:      -105.2301643,
    address:  'Chimney Gulch Trailhead, Golden CO',
    trail_id: 'YcEQoRjrkHYNBJCr9'
  },
  {
    name:     'Matthews Winters - 93 Lot',
    lat:      39.6959373,
    lon:      -105.205894,
    address:  '1135-1227 County Rd 93, Golden, CO 80401',
    trail_id: 'mhd8LP6ybM9HqScQJ'
  },
  {
    name:     'Dakota Ridge - Dino Lot',
    lat:      39.6980874,
    lon:      -105.2051657,
    address:  '1106-1114 County Rd 93, Golden, CO 80401',
    trail_id: 'mhd8LP6ybM9HqScQJ'
  },
  {
    name:     'Mt Falcon - West Parking Lot',
    lat:      39.6362001,
    lon:      -105.2416907,
    address:  '21004 Mt Falcon Rd, Indian Hills, CO 80454',
    trail_id: 'KQtSERokvf5xGWGZS'
  },
  {
    name:     'Wakefield - Park Rec Center Lot',
    lat:      38.818142,
    lon:      -77.226019,
    address:  '8100 Bradock Road Annandale, VA 22003 United States',
    trail_id: 'DNqrxDRRHeoWTxCKz'
  },
  {
    name:     'Meadowood - Main Lot',
    lat:      38.683158,
    lon:      -77.209137,
    address:  '10100 Gunston Rd, Lorton, VA 22079',
    trail_id: 'H66HxGZfHcKftEHGr'
  },
  {
    name:     'Whie Clay - Nine Foot Road Parking Lot',
    lat:      39.724866,
    lon:      -75.743438,
    address:  'Fairhill School Dr, Newark, DE 19711',
    trail_id: 'yoQjnJBGZM6N5jaaA'
  }
]

locations.each do |location|
  Location.find_or_create_by(location)
end

Club.first_or_create(name:          'Yazda',
                     description:   'Yazda is an app for mountain bikers, road bikers, and hikers to discover their next adventure.',
                     website:       'https://yazdaapp.com',
                     contact_email: 'yazda@yazdaapp.com',
                     location:      'Lakewood, CO, USA')
