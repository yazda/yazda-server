# lib/omniauth/facebook.rb

require 'httparty'
require "#{Rails.root}/lib/omniauth/permission_error"
require "#{Rails.root}/lib/omniauth/response_error"

module Omniauth
  class Facebook
    include HTTParty

    debug_output Rails.logger

    # The base uri for facebook graph API
    base_uri 'https://graph.facebook.com/v2.5'

    # Used to authenticate app with facebook user
    # Usage
    #   Omniauth::Facebook.authenticate('authorization_code')
    # Flow
    #   Retrieve access_token from authorization_code
    #   Retrieve User_Info hash from access_token
    def self.authenticate(params, redirect_uri)
      provider     = self.new
      access_token = if params[:access_token].blank?
                       provider.get_access_token(params[:auth_code], redirect_uri)
                     else
                       provider.get_long_access_token(params[:access_token])
                     end
      user_info    = provider.get_user_profile(access_token)
      return user_info, access_token
    end

    # Used to revoke the application permissions and login if a user
    # revoked some of the mandatory permissions required by the application
    # like the email
    # Usage
    #    Omniauth::Facebook.deauthorize('user_id')
    # Flow
    #   Send DELETE /me/permissions?access_token=XXX
    def self.deauthorize(access_token)
      options  = { query: { access_token: access_token } }
      response = self.delete('/me/permissions', options)


      # Something went wrong most propably beacuse of the connection.
      unless response.success?
        Rails.logger.error 'Omniauth::Facebook.deauthorize Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response
    end

    def get_access_token(code, redirect_uri)
      response = self.class.get('/oauth/access_token', query(code, redirect_uri))

      # Something went wrong either wrong configuration or connection
      unless response.success?
        Rails.logger.error 'Omniauth::Facebook.get_access_token Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response['access_token']
    end

    def get_long_access_token(token)
      response = self.class.get('/oauth/access_token', long_query(token))

      # Something went wrong either wrong configuration or connection
      unless response.success?
        Rails.logger.error 'Omniauth::Facebook.get_access_token Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response['access_token']
    end

    def get_user_profile(access_token)
      options  = { query: { access_token: access_token, fields: 'email,name,picture.type(large)' } }
      response = self.class.get('/me', options)

      # Something went wrong most propably beacuse of the connection.
      unless response.success?
        Rails.logger.error 'Omniauth::Facebook.get_user_profile Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response
    end

    def get_user_friends(provider)
      options = { query: { access_token: provider.access_token,
                           fields:       'installed' } }
      begin
        response = self.class.get("/me/friends", options)

        response.parsed_response['data'] || []
      rescue
        []
      end
    end

    private

    # access_token required params
    # https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow/v2.3#confirm
    def query(code, redirect_uri)
      {
        query: {
          code:         code,
          redirect_uri:  redirect_uri || ENV['REDIRECT_URL'],
          client_id:     ENV['FACEBOOK_KEY'],
          client_secret: ENV['FACEBOOK_SECRET']
        }
      }
    end

    def long_query(code)
      {
        query: {
          fb_exchange_token: code,
          grant_type:        'fb_exchange_token',
          client_id:         ENV['FACEBOOK_KEY'],
          client_secret:     ENV['FACEBOOK_SECRET']
        }
      }
    end
  end
end
