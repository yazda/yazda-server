# lib/omniauth/strava.rb

require 'httparty'
require "#{Rails.root}/lib/omniauth/permission_error"
require "#{Rails.root}/lib/omniauth/response_error"

module Omniauth
  class Strava
    include HTTParty

    # The base uri for facebook graph API
    base_uri 'https://www.strava.com/'

    # Used to authenticate app with facebook user
    # Usage
    #   Omniauth::Facebook.authenticate('authorization_code')
    # Flow
    #   Retrieve access_token from authorization_code
    #   Retrieve User_Info hash from access_token
    def self.authenticate(params)
      provider     = self.new
      access_token = if params[:access_token].blank?
                       provider.get_access_token(params[:auth_code])
                     else
                       params[:access_token]
                     end
      user_info    = provider.get_user_profile(access_token)
      return user_info, access_token
    end

    def get_access_token(code)
      response = self.class.post('/oauth/token', query(code))

      # Something went wrong either wrong configuration or connection
      unless response.success?
        Rails.logger.error 'Omniauth::Strava.get_access_token Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response['access_token']
    end

    # Used to revoke the application permissions and login if a user
    # revoked some of the mandatory permissions required by the application
    # like the email
    # Usage
    #    Omniauth::Strava.deauthorize('user_id')
    # Flow
    #   Send DELETE /me/permissions?access_token=XXX
    def self.deauthorize(access_token)
      options  = { query: { access_token: access_token } }
      response = self.post('/oauth/deauthorize', options)

      # Something went wrong most propably beacuse of the connection.
      unless response.success?
        Rails.logger.error 'Omniauth::Strava.deauthorize Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response
    end

    def get_user_profile(access_token)
      options  = { query: { access_token: access_token } }
      response = self.class.get('/api/v3/athlete', options)

      # Something went wrong most propably beacuse of the connection.
      unless response.success?
        Rails.logger.error 'Omniauth::Strava.get_user_profile Failed'
        fail Omniauth::ResponseError, response.body
      end
      response.parsed_response
    end

    def get_user_friends(provider)
      options = { query: { access_token: provider.access_token } }
      begin
        response = self.class.get("/api/v3/athletes/#{provider.uid}/friends",
                                  options)

        response.parsed_response
      rescue
        []
      end
    end

    private
    def query(code)
      {
        query: {
          code:         code,
          client_id:     ENV['STRAVA_KEY'],
          client_secret: ENV['STRAVA_SECRET']
        }
      }
    end
  end
end
