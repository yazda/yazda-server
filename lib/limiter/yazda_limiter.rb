require 'rack/throttle'

class YazdaLimiter < Rack::Throttle::Minute
  def allowed?(request)
    if request.env.has_key?('HTTP_AUTHORIZATION') || Rails.env.test?
      super
    else
      true
    end
  end

  def client_identifier(request)
    request.env['HTTP_AUTHORIZATION'].split(' ')[-1]
  end
end
