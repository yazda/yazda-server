class HashtagHTML < Redcarpet::Render::HTML
  def preprocess(text)
    wrap_hashtags(text)
  end

  def wrap_hashtags(text)
    text.gsub!(/#(\w+)/) do
      "<a href='https://yazdaapp.com/tags/#{$1}'>##{$1}</a>"
    end
    text
  end
end
