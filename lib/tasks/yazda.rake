namespace :yazda do
  desc 'Generates Fake data'
  task fake_data: [:fake_users, :fake_clubs, :fake_adventures] do

  end

  desc "Generates Fake Users"
  task fake_users: :environment do
    club = Club.first

    (1..30).each do |i|
      u = User.new(name:       Faker::Name.name, email: Faker::Internet.email,
                   password:   'password', password_confirmation: 'password',
                   newsletter: false)

      u.build_notification_preference
      u.club_roles.build(club: club, role: :member)
      u.save
    end
  end

  desc "Generates Fake Clubs"
  task fake_clubs: :environment do
    primary_user = User.first

    (1..10).each do |i|
      role = primary_user.club_roles.build(role: :owner)
      role.build_club(website:  Faker::Internet.url, name: Faker::Company.name,
                      location: Faker::Address.city)

      role.save
    end
  end

  desc "TODO"
  task fake_adventures: :environment do
    adventure_types = Adventure.adventure_types.to_a.flatten
    privacy         = Adventure.privacies.to_a.flatten
    attending       = Invite.attendings.to_a.flatten
    skill_level     = [nil, 1, 2, 3, 4, 5]

    (1..10).each do |i|
      user_ids   = User.order("RANDOM()").limit(5).pluck(:id)
      club       = Club.order("RANDOM()").first
      user       = club.club_roles.where(role: [1, 2, 3]).sample.user
      location   = Location.order("RANDOM()").first
      start_time = Faker::Date.forward(2)
      end_time   = start_time + rand(2..5).hours

      adventure = user.adventures.build(user_ids:       user_ids,
                                        club:           club,
                                        location:       location,
                                        privacy:        privacy.sample,
                                        name:           Faker::Book.title,
                                        description:    Faker::Lorem.paragraph,
                                        adventure_type: adventure_types.sample,
                                        start_time:     start_time,
                                        end_time:       end_time,
                                        skill_level:    skill_level.sample)

      adventure.save
      adventure.invites.each do |invite|
        invite.attending = attending.sample
        invite.save
      end
    end
  end

  desc 'Sets all adventures to be in the past'
  task adventures_past: :environment do
    Adventure.by_status.update_all(start_time: 3.days.ago, end_time: 2.days.ago)
  end
end
