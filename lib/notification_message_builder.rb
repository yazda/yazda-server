class NotificationMessageBuilder
  def adventure_invite(adventure, opts = {})
    owner_name = opts[:invited_by] ? opts[:invited_by] : adventure.owner.name

    build_message "#{owner_name} has invited you to a new adventure.",
                  'adventure',
                  adventure.name,
                  opts
  end

  def update_adventure(adventure, opts = {})
    build_message "#{adventure.name} details have been updated.",
                  'adventure_update',
                  adventure.name,
                  opts
  end

  def cancel_adventure(adventure, opts = {})
    build_message "#{adventure.name} has been canceled.",
                  'adventure_update',
                  adventure.name,
                  opts
  end

  def friend_request(friendship, opts = {})
    build_message "#{friendship.user.name} sent you a friend request.",
                  'friend_request',
                  'Yazda',
                  opts
  end

  def invite_responded_to(invite, opts = {})
    action = if invite.ya?
               'said YA'
             elsif invite.na?
               'said NA'
             else
               'has not responded'
             end

    build_message "#{invite.user.name} #{action} to #{invite.adventure.name}.",
                  'invite',
                  'Yazda',
                  opts
  end

  def friend_accepted(friendship, opts = {})
    build_message "#{friendship.friend.name} accepted your friend request.",
                  'friend_accept',
                  'Yazda',
                  opts
  end

  def adventure_reminder(adventure, opts = {})
    build_message "Time to start packing your bags. #{adventure.name} is \
happening soon.",
                  'adventure_update',
                  adventure.name,
                  opts
  end

  def chat(message, title, opts= {})
    build_message message, 'chat', title, opts
  end

  private

  def build_message(message, type, title='Yazda', opts)
    {
      apple_push:   {
        alert:           {
          body:  message,
          title: title
        },
        sound:           'yazda_alert.caf',
        custom_uri:      opts[:custom_uri],
        asset_url:       opts[:appboy_image_url],
        asset_file_type: 'png',
        extra:           {
                           notification_type: type
                         }.merge(opts)
      },
      android_push: {
        alert:        message,
        title:        title,
        custom_uri:   opts[:custom_uri],
        collapse_key: type,
        extra:        {
                        sound: 'yazda_alert'
                      }.merge(opts)
      }
    }
  end
end
